//
//  Singleton.swift
//  Auditix
//
//  Created by TPL Corp Development Team on 1/3/18.
//  Copyright © 2019 TPL Corp. All rights reserved.
//

import Foundation
class Singleton {
    
    
    static let sharedInstance = Singleton()
    
    private init() {
        
    }
}
