



import Foundation
import UIKit
import AVFoundation
import Toast_Swift
import NVActivityIndicatorView
import UserNotifications
import Firebase

//// MARK: AppHelperUtility setup
@objc class Utility: NSObject
{
    static let main = Utility()
    fileprivate override init() {}
}


extension Utility {
    
    func roundAndFormatFloat(floatToReturn : Float, numDecimalPlaces: Int) -> String{
        
        let formattedNumber = String(format: "%.\(numDecimalPlaces)f", floatToReturn)
        return formattedNumber
    }
    func printFonts() {
        for familyName in UIFont.familyNames {
            print("\n-- \(familyName) \n")
            for fontName in UIFont.fontNames(forFamilyName: familyName) {
                print(fontName)
            }
        }
    }
    
    func topViewController(base: UIViewController? = (Constants.APP_DELEGATE).window?.rootViewController) -> UIViewController? {
        
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
    
    
    //     func showAlert(title:String?, message:String?) {
    //        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    //        alert.addAction(UIAlertAction(title: "OK", style: .default) { _ in })
    //        Utility().topViewController()!.present(alert, animated: true){}
    //    }
    
    func resizeImage(image: UIImage,  targetSize: CGFloat) -> UIImage {
        
        guard (image.size.width > 1024 || image.size.height > 1024) else {
            return image;
        }
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newRect: CGRect = CGRect.zero;
        
        if(image.size.width > image.size.height) {
            newRect.size = CGSize(width: targetSize, height: targetSize * (image.size.height / image.size.width))
        } else {
            newRect.size = CGSize(width: targetSize * (image.size.width / image.size.height), height: targetSize)
        }
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newRect.size, false, 1.0)
        image.draw(in: newRect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    
    func thumbnailForVideoAtURL(url: URL) -> UIImage? {
        
        let asset = AVAsset(url: url)
        let assetImageGenerator = AVAssetImageGenerator(asset: asset)
        assetImageGenerator.appliesPreferredTrackTransform=true
        
        var time = asset.duration
        time.value = min(time.value, 2)
        
        do {
            let imageRef = try assetImageGenerator.copyCGImage(at: time, actualTime: nil)
            return UIImage(cgImage: imageRef)
        } catch {
            print("error")
            return nil
        }
    }
    
    
    func delay(delay:Double, closure:@escaping ()->()) {
        DispatchQueue.main.asyncAfter(
            deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
    }
    
    func showLabelIfNoData(withtext text: String, controller: UIViewController, collectionView: UICollectionView)
    {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: controller.view.bounds.size.width, height: controller.view.bounds.size.height))
        messageLabel.text = text
        messageLabel.textColor = UIColor.black
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont(name: "Palatino-Italic", size: 20) ?? UIFont()
        messageLabel.sizeToFit()
        
        collectionView.backgroundView = messageLabel;
    }
    
    func showLabelIfNoData(withtext text: String, controller: UIViewController, tableview: UITableView) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: controller.view.bounds.size.width, height: controller.view.bounds.size.height))
        messageLabel.text = text
        messageLabel.textColor = UIColor.black
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont(name: "Palatino-Italic", size: 20) ?? UIFont()
        messageLabel.sizeToFit()
        tableview.backgroundView = messageLabel;
    }
//
//    func openSettings() {
//        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
//            return
//        }
//
//        if UIApplication.shared.canOpenURL(settingsUrl) {
//            if #available(iOS 10.0, *) {
//                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
//                    print("Settings opened: \(success)") // Prints true
//                })
//            } else {
//                // Fallback on earlier versions
//                UIApplication.shared.openURL(settingsUrl)
//            }
//        }
//    }
    
    func getDevice() -> Devices {
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                //                print("iPhone 5 or 5S or 5C")
                return .iPhone5
            case 1334:
                //                print("iPhone 6/6S/7/8")
                return .iPhone6
            case 1920, 2208:
                //                print("iPhone 6+/6S+/7+/8+")
                return .iPhone6Plus
            case 2436:
                //                print("iPhone X, XS")
                return .iPhoneX
            case 2688:
                //                print("iPhone XS Max")
                return .iPhoneXSMax
            case 1792:
                //                print("iPhone XR")
                return .iPhoneXR
            default:
                return .iPhone6
            }
        }
        return .iPhone6
    }
    
    
    func getFacebookProfilePictureURL(with id: String) -> String {
        return "http://graph.facebook.com/\(id)/picture?type=large"
    }
    
    func getDateString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        return dateFormatter.string(from: Date())
    }
    
    func getDateInLocalFormat(date: String) -> String{
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        return dateFormatter.string(from: dt!)
    }
    
    
    func showShareMenu(with text: String, on Controller: UIViewController) {
        let activity = UIActivityViewController(
            activityItems: [text],
            applicationActivities: nil
        )
        Controller.present(activity, animated: true, completion: nil)
    }
    
    
    func getThreadID(with myUid: String, with recipientUid: String) -> String {
        if myUid > recipientUid {
            return "\(myUid)|$|\(recipientUid)"
        } else {
            return "\(recipientUid)|$|\(myUid)"
        }
    }
    
    func getInstantFCMToken(fcmToken: @escaping (String) -> Void) {
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                fcmToken("")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                fcmToken(result.token)
            }
        }
    }
    
    func openWebURL(webURL: String) {
        let url = URL(string: webURL)
        if UIApplication.shared.canOpenURL(url!) {
            UIApplication.shared.open(url!, options: [:], completionHandler: nil)
        }
    }
}

// MARK: Alert related functions
extension Utility
{
    func showAlert(message:String,title:String,controller:UIViewController){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
//        alertController.view.tintColor = Constants.THEME_COLOR_GOLDEN
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        controller.present(alertController, animated: true, completion: nil)
    }
    
    func showAlertWithOptionalController(message:String,title:String,controller:UIViewController?) -> UIViewController?{
        if(controller == nil)
        {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            return alertController
        }
        else
        {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            controller!.present(alertController, animated: true, completion: nil)
            
            return nil
        }
    }
    

    
    func showAlert(message: String, title: String, controller: UIViewController, usingCompletionHandler handler:@escaping (() -> Swift.Void))
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: {
            
            (action) in
            
            handler()
        }
        ))
        controller.present(alertController, animated: true, completion: nil)
    }
    
    func showAlert(message:String,title:String,controller:UIViewController, completionHandler: @escaping (UIAlertAction?, UIAlertAction?) -> Void){
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
//       alertController.view.tintColor = Constants.THEME_ORANGE_COLOR
        alertController.addAction(UIAlertAction(title: "YES", style: .default){ (alertActionYES) in
            completionHandler(alertActionYES, nil)
        })
        
        alertController.addAction(UIAlertAction(title: "NO", style: .cancel){ (alertActionNO) in
            completionHandler(nil, alertActionNO)
        })
        
        controller.present(alertController, animated: true, completion: nil)
        
    }
    
    func showAlert(message:String,title:String,controller:UIViewController, firstButtonText: String, secondButtonText: String, completionHandler: @escaping (UIAlertAction?, UIAlertAction?) -> Void){
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        //       alertController.view.tintColor = Constants.THEME_ORANGE_COLOR
        alertController.addAction(UIAlertAction(title: firstButtonText, style: .default){ (alertActionYES) in
            completionHandler(alertActionYES, nil)
        })
        
        alertController.addAction(UIAlertAction(title: secondButtonText, style: .cancel){ (alertActionNO) in
            completionHandler(nil, alertActionNO)
        })
        
        controller.present(alertController, animated: true, completion: nil)
        
    }
    
    func showAlert(message:String,title:String){
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            topController.present(alertController, animated: true, completion: nil)
            
        }
    }
    
    func convertStringToUrl(urlToConvert: String)-> URL {
        let url = URL(string: urlToConvert)!
        return url
  }
    
    func convertToArray(text: String) -> Array<Any>? {
        if let data = text.data(using: String.Encoding.utf8) {
            NSLog("Enter here")
            do {
                return try JSONSerialization.jsonObject(with: data, options: [.allowFragments, .mutableContainers, .mutableLeaves]) as? Array
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
        
    }
    
    func showActivityViewController(with sharingText: String)
    {
        let activityViewController = UIActivityViewController(
            activityItems: [sharingText],
            applicationActivities: nil)
        let topController = UIApplication.shared.keyWindow?.rootViewController
        topController?.present(activityViewController, animated: true, completion: nil)
    }
    
    // MARK: - LOCAL NOTIFICATION
    func setLocalNotification(with title: String, bodyText: String, identifier: String, timeIntervel: Double) {
        
        let content = UNMutableNotificationContent()
        content.title = title
        content.body = bodyText
        content.sound = UNNotificationSound.default
        content.categoryIdentifier = identifier
        print("time Interval: \(timeIntervel)")
        
        let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: timeIntervel <= 0.0 ? 5.0 : timeIntervel, repeats: false) // if time intervel is zero, set default value of 5
        let request = UNNotificationRequest.init(identifier: identifier, content: content, trigger: trigger)
        
        let center = UNUserNotificationCenter.current()
        center.add(request)
    }
}


// MARK:- INDICATOR
extension Utility
{
    func showLoader() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let size = CGSize(width: 50, height: 50)
        let bgColor = UIColor.clear//UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        let activityData = ActivityData(size: size, message: "", messageFont: UIFont.systemFont(ofSize: 12), type: .ballClipRotate, color: ColorSheet.LOADER_COLOR , padding: 0, displayTimeThreshold: 0, minimumDisplayTime: 1, backgroundColor: bgColor, textColor: UIColor.black)
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
    }
    
    func hideLoader() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
    }
}
// MARK:- TOAST HELPER UTILITY
extension Utility
{
    func showToast(message: String, controller: UIViewController)
    {
        // Show the message.
        controller.view.hideToastActivity()
        controller.view.makeToast(message, duration: 3.0, position: .center)
    }

    func showToast(message: String, controller: UIViewController, position: ToastPosition)
    {
        controller.view.hideToastActivity()
        controller.view.makeToast(message, duration: 3.0, position: position)
    }
    func showToast(message: String, controller: UIViewController, duration: TimeInterval)
    {
        controller.view.hideToastActivity()
        controller.view.makeToast(message, duration: duration, position: .center)

    }

    func showToast(message: String)
    {
        let topController = UIApplication.shared.keyWindow?.rootViewController
        topController?.view.hideToastActivity()
        topController?.view.makeToast(message, duration: 3.0, position: .center)
    }
    
    
}
 

