//
//  Constants.swift
//  TPL Trakker Affiliate
//
//  Created by IMac on 12/01/2019.
//  Copyright © 2019 TPL Corp. All rights reserved.
//

import Foundation
import UIKit
import Firebase

struct Global {
    
    static let db = Firestore.firestore()
//    static var APP_MANAGER                   = AppStateManager.sharedInstance
//    static var APP_REALM                     = APP_MANAGER.realm
    //    static var USER                          = APP_MANAGER.loggedInUser
}

struct Constants {
    
    static let APP_DELEGATE                  = UIApplication.shared.delegate as! AppDelegate
    static let UIWINDOW                      = UIApplication.shared.delegate!.window!
    static let fcmServerKey                  = "AAAAJ8mHfOM:APA91bERrvetSLXTGxfsotgKzbnLvvyuC8O8i38iR2k5f427oTCiI5jLK2aEZuW4sfqjigVXpAX1Uz2T0jvd1aHwUnQz0PSqLsdOy6UZuYI9IoQI1h8GMVLVEvr1jnv8oPR6cEOnUXn2"
    static let USER_DEFAULTS                 = UserDefaults.standard
    
    static let SINGLETON                     = Singleton.sharedInstance
    
    static let filePath                      = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
    
//    static let BASEURL                       = "http://hktechs.dx.am/ikon/"
    static let BASEURL                       = "https://tadexx.com/tadexx.com/icon_app/ikon/"

    static let Google_Client_ID              = "757598030614-1fdpfkmmk77tusfojh0sg7msjo7dd0cd.apps.googleusercontent.com"
    
    static let ADMOB_APP_ID                  = "ca-app-pub-1759075323275374~6698619928" //TEST
    static let ADMOB_INTERSTITIAL_AD_ID      = "ca-app-pub-3940256099942544/4411468910" // TEST
    
    static let GOOGLE_SIGNIN_NOTIFICATION    = "GOOGLE_SIGNIN_NOTIFICATION"
    
    static let FIREBASE_IMAGE_BASEURL        = "gs://p-messenger-91b0a.appspot.com/"
    static let FIREBASE_IMAGE_STORAGE_PATH   = "gs://p-messenger-91b0a.appspot.com/images/"
    static let FCM_BASE_URL                  = "https://fcm.googleapis.com/fcm/send"
    
    static let PREMIER_WEB_URL               = "https://premierhospicemi.com/"
    static let ADVANCE_DIRECT_NOW_WEB_URL    = "https://adirnow.com/"
    static let MHP_WEB_URL_WEB_URL           = "https://www.mhppharmacy.com/"
    static let MED_SUPPLY_WEB_URL            = "https://apps.intellidosetxm.com/platform/authentication/login?returnUrl=https%3a%2f%2fapps.intellidosetxm.com%2fhome"
    static let AMAZON_WEB_URL                = "https://www.amazon.com/"
    static let GOOGLE_WEB_URL                = "https://www.google.com/"
    
    
}
