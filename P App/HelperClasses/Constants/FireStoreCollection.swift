//
//  FireStoreCollection.swift
//  P App
//
//  Created by Ahmed Shahid on 24/05/2019.
//  Copyright © 2019 Ahmed Shahid. All rights reserved.
//

import Foundation


struct FireStoreCollection {
    static let UserCollection = "users"
    static let MessagesCollection = "messages"
    static let AllMessagesCollection = "allMessages"
    static let AllThreads = "Allthreads"
    static let ThreadsCollection = "threads"
    static let Organizations = "organizations"
    
    static let AllGroups = "AllGroups"
}
