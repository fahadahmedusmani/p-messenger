//
//  ColorSheet.swift
//  TPL Trakker Affiliate
//
//  Created by IMac on 14/01/2019.
//  Copyright © 2019 TPL Corp. All rights reserved.
//

import Foundation
import UIKit

struct ColorSheet {
    static let NAVIGATION_BAR_COLOR = UIColor(red:0.05, green:0.42, blue:0.20, alpha:1.0) // #0c6b34
    static let LOADER_COLOR = UIColor(red:0.60, green:0.60, blue:0.60, alpha:1.0) //9A9A9A grey color
    static let NEW_MSG_COLOR = UIColor(red:0.00, green:0.48, blue:1.00, alpha:1.0)//007AFF // blue color
}
