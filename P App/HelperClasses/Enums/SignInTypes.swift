//
//  SignInTypers.swift
//  iKon
//
//  Created by MBP on 24/02/2019.
//  Copyright © 2019 iKon. All rights reserved.
//

import Foundation

enum SignInTypes: String {
    case phoneNumber = "Phone Number"
    case gmail = "Gmail"
    case facebook = "Facebook"
}
