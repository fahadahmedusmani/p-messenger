//
//  Devices.swift
//  TPL Trakker Affiliate
//
//  Created by IMac on 12/02/2019.
//  Copyright © 2019 TPL Corp. All rights reserved.
//

import Foundation

enum Devices {
    case iPhone5
    case iPhone6
    case iPhone6Plus
    case iPhoneX
    case iPhoneXSMax
    case iPhoneXR
}

