//
//  Strings+Extension.swift
//  TPL Trakker Affiliate
//
//  Created by IMac on 30/01/2019.
//  Copyright © 2019 TPL Corp. All rights reserved.
//

import Foundation

extension String
{
    func trim() -> String {
        return self.trimmingCharacters(in: .whitespaces)
    }
}
