////
////  UIButton+Extension.swift
////  Auditix
////
////  Created by TPL Corp Development Team on 1/3/18.
////  Copyright © 2019 TPL Corp. All rights reserved.
////
//
//import UIKit
//
//extension UIButton {
//    private func actionHandleBlock(action:(() -> Void)? = nil) {
//        struct __ {
//            static var action :(() -> Void)?
//        }
//        if action != nil {
//            __.action = action
//        } else {
//            __.action?()
//        }
//    }
//    
//    @objc private func triggerActionHandleBlock() {
//        self.actionHandleBlock()
//    }
//    
//    func actionHandle(controlEvents control :UIControl.Event, ForAction action:@escaping () -> Void) {
//        self.actionHandleBlock(action: action)
//        self.addTarget(self, action: #selector(UIButton.triggerActionHandleBlock), for: control)
//    }
//}
//
