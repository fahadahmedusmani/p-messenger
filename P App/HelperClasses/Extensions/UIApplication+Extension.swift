//
//  UIApplication+Extension.swift
//  Auditix
//
//  Created by TPL Corp Development Team on 1/3/18.
//  Copyright © 2019 TPL Corp. All rights reserved.
//

import Foundation
import UIKit

extension UIApplication {
    class func tryURL(urls: [String]) {
        let application = UIApplication.shared
        for url in urls {
            if application.canOpenURL(NSURL(string: url)! as URL) {
                if #available(iOS 10.0, *) {
                    application.open(NSURL(string: url)! as URL, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                    application.openURL(URL(string: url)!)
                }
                return
            }
        }
    }
}
