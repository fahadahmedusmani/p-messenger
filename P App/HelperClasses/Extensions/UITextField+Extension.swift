////
////  UITextField+Extension.swift
////  Auditix
////
////  Created by TPL Corp Development Team on 1/3/18.
////  Copyright © 2019 TPL Corp. All rights reserved.
////
//
//import Foundation
//import UIKit
//
//extension UITextField{
//    @IBInspectable var placeHolderColor: UIColor? {
//        get {
//            return self.placeHolderColor
//        }
//        set {
//            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue!])
//        }
//    }
//}
