////
////  UIFont+Extension.swift
////  Auditix
////
////  Created by TPL Corp Development Team on 1/3/18.
////  Copyright © 2019 TPL Corp. All rights reserved.
////
//
//import Foundation
//import UIKit
//
//extension UIFont {
//    func sizeOfString(text: NSString, constrainedToWidth width: Double) -> CGSize {
//        return text.boundingRect(with: CGSize(width: width, height: .greatestFiniteMagnitude),
//                                 options: .usesLineFragmentOrigin,
//                                 attributes: [NSAttributedString.Key.font: self],
//                                 context: nil).size
//    }
//}
//
//extension String {
//    func removingWhitespaces() -> String {
//        return components(separatedBy: .whitespaces).joined()
//    }
//}
