//
//  AppDelegate.swift
//  P App
//
//  Created by IMac on 13/05/2019.
//  Copyright © 2019 Ahmed Shahid. All rights reserved.
//

import UIKit
import Firebase
import Sinch


// on branch development
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, SINClientDelegate, SINCallClientDelegate, SINManagedPushDelegate  {
    
    var window: UIWindow?
    var client: SINClient!
    var player: AVAudioPlayer?
    var threads: [Thread]?
    var push: SINManagedPush?
    
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        func onUserDidLogin(_ userId: String)
        {
            
            print("calling initSinch")
            self.initSinchClient(withUserId: userId)
        }
        
        NotificationCenter.default.addObserver(forName: Notification.Name("UserDidLoginNotification"), object: nil, queue: nil, using: {(_ note: Notification) -> Void in
            print("Got notification")
            let userId = note.userInfo!["userId"] as! String
            UserDefaults.standard.set(userId, forKey: "userId")
            UserDefaults.standard.synchronize()
            onUserDidLogin(userId)
        })
        
        FirebaseApp.configure()
        if AppStateManager.shared.isUserLoggedIn() {
            self.setRootViewControllerToHome()
        } else {
            self.setRootViewControllerToLogin()
        }
        
        UNUserNotificationCenter.current().delegate = self
        
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: {_, _ in })
       // DispatchQueue.main.async {
            application.registerForRemoteNotifications()
       // }
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        return true
    }
    
    func initSinchClient(withUserId userId: String) {
        
        if client == nil {
            
            self.push = Sinch.managedPush(with: .development)
            self.push?.delegate = self
            self.push?.setDesiredPushType(SINPushTypeRemote)
           
            self.push?.setDisplayName(AppStateManager.shared.loggedInUser?.name)
            
            print("initializing client 2")
            client = Sinch.client(withApplicationKey: "1deac554-efe9-4b9a-95af-e76b3c1cca59",
                                  applicationSecret: "0HO+2A4Df0ukpqnKhAytmw==",
                                  environmentHost: "clientapi.sinch.com",
                                  userId: userId)
            
            client.delegate = self
            client.call()?.delegate = self
            client.setSupportCalling(true)
            client.start()
            client.startListeningOnActiveConnection()
            client.setSupportPushNotifications(true)
            client.enableManagedPushNotifications()
            self.client.setPushNotificationDisplayName(AppStateManager.shared.loggedInUser?.name)
        }
    }
    
    //SINCallClient delegates
    func clientDidStart(_ client: SINClient!) {
        print("Sinch client started successfully (version: \(Sinch.version()) with userid \(client.userId!)")
    }
    
    func clientDidFail(_ client: SINClient!, error: Error!) {
        print("Sinch client error: \(String(describing: error?.localizedDescription))")
    }
    
    func client(_ client: SINClient, logMessage message: String, area: String, severity: SINLogSeverity, timestamp: Date) {
        
        print("\(message)")
        
    }
    
    func managedPush(_ managedPush: SINManagedPush!, didReceiveIncomingPushWithPayload payload: [AnyHashable : Any]!, forType pushType: String!) {
        
        let result = SINPushHelper.queryPushNotificationPayload(payload)
        if (result?.isCall())!{
            print(result?.call()?.callId as Any)
        }
        client.relayRemotePushNotification(payload)
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        //client.stopListeningOnActiveConnection()
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        //client.startListeningOnActiveConnection()
        print("foreground")
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func setRootViewControllerToHome() {
        let tabBarController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "BaseTabBarController")
        self.window?.rootViewController = nil;
        self.window?.rootViewController = tabBarController
    }
    
    func setRootViewControllerToLogin() {
        let navigation = AppStoryboard.Login.instance.instantiateViewController(withIdentifier: "BaseNavigationController")
        self.window?.rootViewController = nil;
        self.window?.rootViewController = navigation
    }
    
    func client(_ client: SINCallClient!, didReceiveIncomingCall call: SINCall!) {
        
        if call.state == .ended{
            return
        }
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CallerViewController") as! CallerViewController
        vc.call = call
        if let sender = self.threads?.first(where: {$0.senderUid == call.remoteUserId})?.snederObject{
            vc.sender = sender
        }
        else{
            let user = User.init()
            user.name = call.headers["callerName"] as? String ?? "Incoming Call"
            user.phoneNumber = call.headers["callerPhone"] as? String ?? ""
            user.image = call.headers["callerPic"] as? String ?? ""
            user.uid = call.remoteUserId
            vc.sender = user
        }
        if let rootViewController = UIApplication.topViewController() {
            rootViewController.present(vc, animated: true, completion: nil)
        }
    }
    
    func client(_ client: SINCallClient!, localNotificationForIncomingCall call: SINCall!) -> UILocalNotification {
        
        let notification = UILocalNotification.init()
        notification.alertAction = "Answer"
        notification.alertBody = "Incoming call from \(call.headers["callerName"] ?? "")"
        notification.soundName = "iphone_x_ringtone"
        return notification
    }
}

// MARK: - PUSH NOTIFICATION
extension AppDelegate: UNUserNotificationCenterDelegate {
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print("deviceTokenString: \(deviceTokenString)")
        //        self.apnsToken = deviceTokenString
        
        //set apns token in messaging
        
        Messaging.messaging().apnsToken = deviceToken
       // DispatchQueue.main.async {
            self.push?.application(application, didRegisterForRemoteNotificationsWithDeviceToken: deviceToken)
       // }
        //get FCM tokenfSINCallClientDelegate
        if let token = Messaging.messaging().fcmToken {
            //            self.fcmToken = token
            print("FCM token: \(token)")
        }
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        
        
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("I am willPresent")
        completionHandler( [.alert, .badge, .sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("I am didReceive")
        
    }
    
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        
        self.push?.application(application, didReceiveRemoteNotification: userInfo)
        
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        // self.push?.application(application,     didReceiveRemoteNotification: userInfo)
    }
    
    
    
}

extension UIApplication {
    
    class func topViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        
        if let tab = base as? UITabBarController {
            let moreNavigationController = tab.moreNavigationController
            
            if let top = moreNavigationController.topViewController, top.view.window != nil {
                return topViewController(base: top)
            } else if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        
        return base
    }
}
