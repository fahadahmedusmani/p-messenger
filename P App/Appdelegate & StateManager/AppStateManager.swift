//
//  AppStateManager.swift
//  P App
//
//  Created by Ahmed Shahid on 18/05/2019.
//  Copyright © 2019 Ahmed Shahid. All rights reserved.
//

import Foundation


class AppStateManager: NSObject {
    
    static let shared = AppStateManager()
    var loggedInUser: User?
    
    private override init() {
        super.init()
        if Constants.USER_DEFAULTS.rm_customObject(forKey: UserDefaultKeys.User) != nil {
            if let User = Constants.USER_DEFAULTS.rm_customObject(forKey: UserDefaultKeys.User) as? User {
                loggedInUser = User
            }
        }
    }
    
    func isUserLoggedIn() -> Bool {
        if (self.loggedInUser) != nil {
            return true
        }
        return false
    }
    
    func createUser(with User: User) {
        Constants.USER_DEFAULTS.rm_setCustomObject(User, forKey: UserDefaultKeys.User)
        self.loggedInUser = User
    }
    
    func syncLocalUserData() {
        Constants.USER_DEFAULTS.rm_setCustomObject(self.loggedInUser, forKey: UserDefaultKeys.User)
    }
    
    func logoutUser() {
        
        let batch = Global.db.batch()
        
        let ref = Global.db.collection(FireStoreCollection.UserCollection).document(AppStateManager.shared.loggedInUser?.uid ?? "")
        batch.updateData([User.CodingKeys.isUserActive.stringValue(): "0"], forDocument: ref)
        Utility.main.showLoader()
        batch.commit { (error) in
            if let err = error {
                print("Error writing batch \(err)")
                Utility.main.showToast(message: AlertErrorMessages.unableToEditProfile.message())
                return
            }
            Utility.main.hideLoader()
            Constants.USER_DEFAULTS.removeObject(forKey: UserDefaultKeys.User)
            self.loggedInUser = nil
            Constants.APP_DELEGATE.setRootViewControllerToLogin()
        }
        
    }
    
}
