//
//  CreateGroupController.swift
//  P App
//
//  Created by Ahmed Shahid on 07/07/2019.
//  Copyright © 2019 Ahmed Shahid. All rights reserved.
//

import UIKit

class CreateGroupController: UIViewController {

    @IBOutlet weak var imageviewGroupIcon: UIImageView!
    @IBOutlet weak var barBtnCreate: UIBarButtonItem!
    @IBOutlet weak var labelTextCount: UILabel!
    @IBOutlet weak var textfieldGroupSubject: UITextField!
    @IBOutlet weak var btnAddPhoto: UIButton!
    
    var groupMembersArray: [User]? = [User]()
    var imagePicker: ImagePicker? = nil
    var imageGroup: UIImage? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.textfieldGroupSubject.delegate = self
        self.textfieldGroupSubject.becomeFirstResponder()
        
         self.imagePicker = ImagePicker.init(presentationController: self, delegate: self)
        // Do any additional setup after loading the view.
    }
    

    func getMembersDictionary() -> [NSDictionary] {
        var returnDictionary = [NSDictionary]()
        
        for userMember in groupMembersArray! {
            returnDictionary.append(userMember.dictionaryRepresentation())
            if let loggedInUser = AppStateManager.shared.loggedInUser?.dictionaryRepresentation(){
                returnDictionary.append(loggedInUser)
            }
        }
        
        return returnDictionary
    }
    
    func pushChatController(with group: Group, groupID: String) {
        if let controller = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "ChatController") as? ChatController {
            controller.recipientMemebers = self.groupMembersArray
            controller.group = group
            controller.groupID = groupID
            controller.controllerType = .groupChat
            controller.createGroupBecauseImNew = true
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    @IBAction func actionAddPhoto(_ sender: Any) {
        self.imagePicker?.present(from: self.view)
    }
    
    @IBAction func actionBarBtnCreate(_ sender: Any) {
        if (self.groupMembersArray?.count ?? 0) == 0 {
            return
        } else if self.textfieldGroupSubject.text?.isEmpty ?? true {
            return
        }
        
        if self.imageGroup != nil {
            UserFireStoreManager.shared.uploadGroupImage(with: AppStateManager.shared.loggedInUser?.uid ?? "", image: self.imageGroup ?? UIImage()) { (imageURL) in
                self.createGroup(with: imageURL)
            }
        } else {
            self.createGroup(with: "")
        }
       
    }
    
    @IBAction func btnPremier(_ sender: Any) {
        Utility.main.openWebURL(webURL: Constants.PREMIER_WEB_URL)
    }
    
    @IBAction func btnAdvanceDirectiveNow(_ sender: Any) {
        Utility.main.openWebURL(webURL: Constants.ADVANCE_DIRECT_NOW_WEB_URL)
    }
    
    @IBAction func btnMedSupply(_ sender: Any) {
        Utility.main.openWebURL(webURL: Constants.MED_SUPPLY_WEB_URL)
    }
    
    @IBAction func btnMHP(_ sender: Any) {
        Utility.main.openWebURL(webURL: Constants.MHP_WEB_URL_WEB_URL)
    }
    
    @IBAction func btnGoogle(_ sender: Any) {
        Utility.main.openWebURL(webURL: Constants.GOOGLE_WEB_URL)
    }
    
    @IBAction func btnAmazon(_ sender: Any) {
        Utility.main.openWebURL(webURL: Constants.AMAZON_WEB_URL)
    }
    
}

// MARK: - API CALLINGS
extension CreateGroupController {
    func createGroup(with groupImage: String) {
        
        Utility.main.showLoader()
        GroupFireStoreManager.shared.createGroup(groupName: self.textfieldGroupSubject.text!, groupImage: groupImage, groupMembers: self.getMembersDictionary(), groupCreated: { (groupCreatedSuccess, group, groupID) in
            if groupCreatedSuccess {
                print("group created please check")
                self.pushChatController(with: group, groupID: groupID)
            }
            Utility.main.hideLoader()
        }) { (err) in
            Utility.main.hideLoader()
            Utility.main.showToast(message: err.localizedDescription, controller: self)
        }
    }
    
    //uploadGroupImage
}
// MARK: - ImagePicker Delegate
extension CreateGroupController: ImagePickerDelegateInternal {
    func didSelect(image: UIImage?) {
        if (image != nil) {
            self.btnAddPhoto.setTitle("", for: .normal)
            self.imageviewGroupIcon.image = image
            self.imageGroup = image
        }
    }
}

// MARK: - UITextField Delegate
extension CreateGroupController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        // verify max length has not been exceeded
        let nsString = textField.text as NSString?
        let text = nsString?.replacingCharacters(in: range, with: string)
        if (text?.count ?? 0 > 25) // 4 was chosen for SSN verification
        {
            // suppress the max length message only when the user is typing
            // easy: pasted data has a length greater than 1; who copy/pastes one character?
            if (text?.count ?? 0 > 1)
            {
                
                // BasicAlert(@"", @"This field accepts a maximum of 4 characters.");
            }
            
            return false;
        }
        self.labelTextCount.text = "\(25 - text!.count)"
        (text?.count ?? 0) > 0 ? (self.barBtnCreate.isEnabled = true) : (self.barBtnCreate.isEnabled = false)
        return true
    }
}
