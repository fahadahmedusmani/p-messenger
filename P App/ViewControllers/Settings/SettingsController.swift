//
//  SettingsController.swift
//  P App
//
//  Created by Ahmed Shahid on 25/05/2019.
//  Copyright © 2019 Ahmed Shahid. All rights reserved.
//

import UIKit
import MessageUI


class SettingsController: UITableViewController {
    
    @IBOutlet weak var labelVersion: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var imagviewProfile: UIImageView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        //CFBundleVersion
        let buildNum = Bundle.main.infoDictionary?["CFBundleVersion"] as? String
        labelVersion.text = "-- Version \(appVersion ?? "")(\(buildNum ?? "")) --"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.labelName.text = AppStateManager.shared.loggedInUser?.name
        
        //AppStateManager.shared.loggedInUser?.image
        if let urlImage = URL(string: UserDefaults.standard.value(forKey: "profileImage") as? String ?? "") {
            self.imagviewProfile.sd_setImage(with: urlImage, placeholderImage: UIImage(named: "profilePlaceholder"), options: .fromLoaderOnly, context: nil)
        } else {
            self.imagviewProfile.image = UIImage(named: "profilePlaceholder")
        }
    }
    
    
    func launchEmail() {
        let email = "sales@ansytech.com"
        if let url = URL(string: "mailto:\(email)") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        /*if MFMailComposeViewController.canSendMail() {
            let toRecipents = ["sales@ansytech.com"]
            let mc: MFMailComposeViewController = MFMailComposeViewController()
            mc.mailComposeDelegate = self
            mc.setToRecipients(toRecipents)
            self.present(mc, animated: true, completion: nil)
        }*/
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if section == 0 {
            return 1
        } else if section == 1 {
            return 3
        }
        
        return 0
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.section == 0 {
            if let controller = AppStoryboard.Settings.instance.instantiateViewController(withIdentifier: "EditProfileController") as? EditProfileController {
                self.navigationController?.pushViewController(controller, animated: true)
            }
        } else if indexPath.section == 1 {
            switch indexPath.row {
            case 0:
                Utility.main.showShareMenu(with: AlertErrorMessages.tellAFriendText.message(), on: self)
                break
            case 1:
                self.launchEmail()
                break
            case 2:
                print("delete account")
                Utility.main.showAlert(message: AlertErrorMessages.logoutWarningMsg.message(), title: AlertErrorMessages.alertTitle.message(), controller: self) { (yesTap, _) in
                    if yesTap != nil {
                        AppStateManager.shared.logoutUser()
                    }
                }
            default:
                print("")
            }
        }
    }
    
    @IBAction func btnPremier(_ sender: Any) {
        Utility.main.openWebURL(webURL: Constants.PREMIER_WEB_URL)
    }
    
    @IBAction func btnAdvanceDirectiveNow(_ sender: Any) {
        Utility.main.openWebURL(webURL: Constants.ADVANCE_DIRECT_NOW_WEB_URL)
    }
    
    @IBAction func btnMedSupply(_ sender: Any) {
        Utility.main.openWebURL(webURL: Constants.MED_SUPPLY_WEB_URL)
    }
    
    @IBAction func btnMHP(_ sender: Any) {
        Utility.main.openWebURL(webURL: Constants.MHP_WEB_URL_WEB_URL)
    }
    
    @IBAction func btnGoogle(_ sender: Any) {
        Utility.main.openWebURL(webURL: Constants.GOOGLE_WEB_URL)
    }
    
    @IBAction func btnAmazon(_ sender: Any) {
        Utility.main.openWebURL(webURL: Constants.AMAZON_WEB_URL)
    }
    
}


// MARK: - MFMailComposeViewController Delegate
extension SettingsController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result {
        case .cancelled:
            print("Mail cancelled")
        case .saved:
            print("Mail saved")
        case .sent:
            print("Mail sent")
        case .failed:
            print("Mail sent failure: \(error?.localizedDescription)")
        default:
            break
        }
        self.dismiss(animated: true, completion: nil)
    }
}
