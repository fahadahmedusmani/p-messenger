//
//  EditProfileController.swift
//  P App
//
//  Created by Ahmed Shahid on 26/05/2019.
//  Copyright © 2019 Ahmed Shahid. All rights reserved.
//

import UIKit
import FirebaseStorage

class EditProfileController: UITableViewController {
    
    @IBOutlet weak var switchPatient: UISwitch!
    @IBOutlet weak var switchPatientFamily: UISwitch!
    //    @IBOutlet weak var textfieldPatientNum: UITextField!
//    @IBOutlet weak var textfieldPatientID: UITextField!
    @IBOutlet weak var labelNumbe: UILabel!
    @IBOutlet weak var labelNameCharCount: UILabel!
    @IBOutlet weak var btnAddPhoto: UIButton!
    @IBOutlet weak var textfieldName: UITextField!
    @IBOutlet weak var imageviewProfile: UIImageView!
    @IBOutlet weak var switchMedProf: UISwitch!
    
    var imagePicker: ImagePicker? = nil
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.labelNumbe.text = AppStateManager.shared.loggedInUser?.phoneNumber
        self.textfieldName.text = AppStateManager.shared.loggedInUser?.name
        self.switchPatient.isOn = AppStateManager.shared.loggedInUser?.isPatient ?? false
        self.switchPatientFamily.isOn = AppStateManager.shared.loggedInUser?.isPatientFamilyMemeber ?? false
        
        self.textfieldName.delegate = self
//        self.textfieldPatientID.delegate = self
//        self.textfieldPatientNum.delegate = self
        self.labelNameCharCount.text = "\(25 - (AppStateManager.shared.loggedInUser?.name?.count ?? 0))"
        
        self.switchMedProf.isOn = AppStateManager.shared.loggedInUser?.isMedicalProfessional ?? false
        
        //AppStateManager.shared.loggedInUser?.image ?? ""
        if let urlImage = URL(string: (UserDefaults.standard.value(forKey: "profileImage") as? String) ?? "") {
            self.btnAddPhoto.setTitle("", for: .normal)
            self.imageviewProfile.sd_setImage(with: urlImage, placeholderImage: UIImage(named: "profilePlaceholder"))
        } else {
            //            self.imageviewProfile.image = UIImage(named: "profilePlaceholder")
        }
        
        self.imagePicker = ImagePicker.init(presentationController: self, delegate: self)
    }
    
    func editData() {
        Utility.main.showLoader()
        UserFireStoreManager.shared.editData(key: User.CodingKeys.name.stringValue(), value: self.textfieldName.text?.trim() ?? "", sucess: { (resultSuccess) in
            if resultSuccess ?? false {
                AppStateManager.shared.loggedInUser?.name = self.textfieldName.text
                AppStateManager.shared.syncLocalUserData()
            }
            Utility.main.hideLoader()
        }) { (error) in
            print("Error writing batch \(error)")
            Utility.main.showToast(message: AlertErrorMessages.unableToEditProfile.message(), controller: self)
            Utility.main.hideLoader()
        }
    }
    
    func editDataPatientID() {
        Utility.main.showLoader()
        UserFireStoreManager.shared.editData(key: User.CodingKeys.isPatient.stringValue(), value: self.switchPatient.isOn, sucess: { (resultSuccess) in
            if resultSuccess ?? false {
                AppStateManager.shared.loggedInUser?.isPatient = self.switchPatient.isOn//self.textfieldPatientID.text
                AppStateManager.shared.syncLocalUserData()
            }
            Utility.main.hideLoader()
        }) { (error) in
            print("Error writing batch \(error)")
            Utility.main.showToast(message: AlertErrorMessages.unableToEditProfile.message(), controller: self)
            Utility.main.hideLoader()
        }
    }
    
    func editDataPatientNum() {
        Utility.main.showLoader()
        UserFireStoreManager.shared.editData(key: User.CodingKeys.isPatientFamilyMemeber.stringValue(), value: self.switchPatient.isOn, sucess: { (resultSuccess) in
            if resultSuccess ?? false {
                AppStateManager.shared.loggedInUser?.isPatientFamilyMemeber = self.switchPatientFamily.isOn
                AppStateManager.shared.syncLocalUserData()
            }
            Utility.main.hideLoader()
        }) { (error) in
            print("Error writing batch \(error)")
            Utility.main.showToast(message: AlertErrorMessages.unableToEditProfile.message(), controller: self)
            Utility.main.hideLoader()
        }
    }
    
    func editDataIsMedProf() {
        Utility.main.showLoader()
        UserFireStoreManager.shared.editData(key: User.CodingKeys.isMedicalProfessional.stringValue(), value: self.switchMedProf.isOn, sucess: { (resultSuccess) in
            if resultSuccess ?? false {
                AppStateManager.shared.loggedInUser?.isMedicalProfessional = self.switchMedProf.isOn
                AppStateManager.shared.syncLocalUserData()
            }
            Utility.main.hideLoader()
        }) { (error) in
            print("Error writing batch \(error)")
            Utility.main.showToast(message: AlertErrorMessages.unableToEditProfile.message(), controller: self)
            Utility.main.hideLoader()
        }
    }
    
    @IBAction func actionAddPhoto(_ sender: Any) {
        self.imagePicker?.present(from: self.view)
    }
    
    @IBAction func actionSwitchPatient(_ sender: Any) {
        self.editDataPatientID()
        self.tableView.reloadSections(IndexSet(integer: 2), with: .none)
    }
    @IBAction func actionSwitchPatientFamily(_ sender: Any) {
        editDataPatientNum()
        self.tableView.reloadSections(IndexSet(integer: 2), with: .none)
    }
    @IBAction func actionSwitchMedProf(_ sender: Any) {
//        self.tableView.reloadData()
        self.editDataIsMedProf()
        self.tableView.reloadSections(IndexSet(integer: 2), with: .none)
    }
    
    @IBAction func btnPremier(_ sender: Any) {
        Utility.main.openWebURL(webURL: Constants.PREMIER_WEB_URL)
    }
    
    @IBAction func btnAdvanceDirectiveNow(_ sender: Any) {
        Utility.main.openWebURL(webURL: Constants.ADVANCE_DIRECT_NOW_WEB_URL)
    }
    
    @IBAction func btnMedSupply(_ sender: Any) {
        Utility.main.openWebURL(webURL: Constants.MED_SUPPLY_WEB_URL)
    }
    
    @IBAction func btnMHP(_ sender: Any) {
        Utility.main.openWebURL(webURL: Constants.MHP_WEB_URL_WEB_URL)
    }
    
    @IBAction func btnGoogle(_ sender: Any) {
        Utility.main.openWebURL(webURL: Constants.GOOGLE_WEB_URL)
    }
    
    @IBAction func btnAmazon(_ sender: Any) {
        Utility.main.openWebURL(webURL: Constants.AMAZON_WEB_URL)
    }
}

// MARK: - UITableView DataSource
extension EditProfileController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 3
        
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if section == 0 || section == 1 {
            return 1
        } else if section == 2 {
//            if self.switchMedProf.isOn {
//                return 3
//            } else {
//                return 1
//            }
            return 3
        }
        
        return 0
    }
    
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        } else {
            return 18
        }
    }
    
    
    
}

// MARK: - UITextField Delegate
extension EditProfileController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if self.textfieldName == textField {
            if (textField.text?.count ?? 0) > 0 {
                // save data
                self.editData()
            }
        }
//        else if self.textfieldPatientID == textField {
//            if (textField.text?.count ?? 0) > 0 {
//                // save data
//                self.editDataPatientID()
//            }
//        } else if self.textfieldPatientNum == textField {
//            if (textField.text?.count ?? 0) > 0 {
//                // save data
//                self.editDataPatientNum()
//            }
//        }
       
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        // verify max length has not been exceeded
        if self.textfieldName == textField {
            let nsString = textField.text as NSString?
            let text = nsString?.replacingCharacters(in: range, with: string)
            if (text?.count ?? 0 > 25) // 4 was chosen for SSN verification
            {
                // suppress the max length message only when the user is typing
                // easy: pasted data has a length greater than 1; who copy/pastes one character?
                if (text?.count ?? 0 > 1)
                {
                    
                    // BasicAlert(@"", @"This field accepts a maximum of 4 characters.");
                }
                
                return false;
            }
            self.labelNameCharCount.text = "\(25 - text!.count)"
            //        (text?.count ?? 0) > 0 ? (self.barBtnDone.isEnabled = true) : (self.barBtnDone.isEnabled = false)
        }
        return true
    }
}
// MARK: - ImagePicker Delegate
extension EditProfileController: ImagePickerDelegateInternal {
    func didSelect(image: UIImage?) {
        
        if (image != nil) {
            self.btnAddPhoto.setTitle("", for: .normal)
            self.imageviewProfile.image = image
            if let uid = AppStateManager.shared.loggedInUser?.uid {
                UserFireStoreManager.shared.uploadImage(with: uid, image: image!) { (imageURL) in
                    print(imageURL)
                     AppStateManager.shared.loggedInUser?.image = imageURL
                     UserDefaults.standard.set(imageURL, forKey: "profileImage")
                    UserDefaults.standard.synchronize()
                }
            }
           
        }
    }
}
