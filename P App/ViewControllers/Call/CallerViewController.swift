//
//  CallerViewController.swift
//  P App
//
//  Created by Avanza on 10/05/2020.
//  Copyright © 2020 Ahmed Shahid. All rights reserved.
//

import UIKit
import Sinch
import AVFoundation
import CallKit

class CallerViewController: UIViewController {
    
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var hangeUpButton: UIButton!
    @IBOutlet weak var remoteUserView: UIView!
    @IBOutlet weak var localUserView: UIView!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var rejectButton: UIButton!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var speakerButton: UIButton!
    @IBOutlet weak var micButton: UIButton!
    
    var durationTimer: Timer?
    var call: SINCall!
    var sender: User?
    var player : AVAudioPlayer?
    var audioController:SINAudioController!
    let session = AVAudioSession.sharedInstance()
    var error: NSError?
    
    var videoController:SINVideoController? {
        let appDelegate = UIApplication.shared.delegate! as! AppDelegate
        return (appDelegate.client?.videoController())
    }
    
    var client: SINClient? {
        let appDelegate = UIApplication.shared.delegate! as! AppDelegate
        return (appDelegate.client)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        audioController = client?.audioController()
        audioController.configureAudioSessionForCallKitCall()
        call.delegate = self
        
        do{
            try session.setCategory(.playAndRecord)
            try session.overrideOutputAudioPort(AVAudioSession.PortOverride.speaker)
            try session.setActive(true)
        } catch {
            print ("\(#file) - \(#function) error: \(error.localizedDescription)")
        }
        
        player = try! AVAudioPlayer.init(contentsOf: Bundle.main.url(forResource: "iphone_x_ringtone", withExtension: "mp3")!)
        player?.numberOfLoops = -1
        
        if call.details.isVideoOffered{
            imgView.isHidden = true
            localUserView.isHidden = false
            remoteUserView.isHidden = false
            localUserView.addSubview(self.videoController!.localView())
        }
        else{
            imgView.isHidden = false
            localUserView.isHidden = true
            remoteUserView.isHidden = true
        }
        
        if call.direction == .incoming
        {
            setCallStatus("INCOMING CALL")
            showButtons(.decline)
            //let path = Bundle.main.path(forResource: "incoming", ofType: "wav")
            //self.audioController.startPlayingSoundFile(path, loop: true)
            player?.play()
        }
        else
        {
            setCallStatus("CALLING.....")
            showButtons(.hangup)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let user = sender{
            numberLabel.text = user.name
            imgView.sd_setImage(with: URL(string: user.image!), placeholderImage: #imageLiteral(resourceName: "profilePlaceholder1"), options: [], context: nil)
        }
        else{
            numberLabel.text = call?.remoteUserId!
        }
    }
    
    
    @IBAction func didTappedHangUp(_ sender: Any) {
        self.audioController.disableSpeaker()
        call.hangup()
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func acceptButton(_ sender: Any) {
        self.audioController.disableSpeaker()
        self.audioController.stopPlayingSoundFile()
        player?.stop()
        call.answer()
    }
    
    @IBAction func didTappedReject(_ sender: Any) {
        player?.stop()
        call.hangup()
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func didTappedSpeaker(_ sender: Any) {
        if (sender as! UIButton).isSelected{
            self.audioController.disableSpeaker()
        }
        else{
            self.audioController.enableSpeaker()
        }
        (sender as! UIButton).isSelected.toggle()
    }
    
    @IBAction func didTappedMic(_ sender: Any) {
        if (sender as! UIButton).isSelected{
            self.audioController.unmute()
        }
        else{
           self.audioController.mute()
        }
         (sender as! UIButton).isSelected.toggle()
    }
    
}

extension CallerViewController{
    
    func setCallStatus(_ text: String) {
        self.statusLabel.text = text
    }
    
    func setCallStatusText(_ text: String) {
        statusLabel.text = text
    }
    
    func showButtons(_ buttons: EButtonsBar) {
        if buttons == .decline {
            rejectButton.isHidden = false
            acceptButton.isHidden = false
            hangeUpButton.isHidden = true
        }
        else if buttons == .hangup {
            rejectButton.isHidden = true
            acceptButton.isHidden = true
            hangeUpButton.isHidden = false
        }
    }
    
    // MARK: - Duration
    func setDuration(_ seconds: Int)
    {
        setCallStatusText(String(format: "%02d:%02d", arguments: [Int(seconds / 60), Int(seconds % 60)]))
    }
    
    func pathForSound(_ soundName: String) -> String {
        let resourcePath = Bundle.main.resourcePath! as NSString
        return resourcePath.appendingPathComponent(soundName)
    }
}

extension CallerViewController: SINCallDelegate{
    
    @objc func onDurationTimer(_ unused: Timer) {
        let duration = Int(Date().timeIntervalSince(call.details.establishedTime))
        DispatchQueue.main.async {
            self.setDuration(duration)
        }
    }
    
    // MARK: - SINCallDelegate
    
    func callDidProgress(_ call: SINCall)
    {
        speakerButton.isHidden = false
        micButton.isHidden = false
        statusLabel.text = "RINGING..."
        do{
            try session.setCategory(.playAndRecord)
            try session.overrideOutputAudioPort(AVAudioSession.PortOverride.none)
            try session.setActive(true)
        } catch {
            print ("\(#file) - \(#function) error: \(error.localizedDescription)")
        }
       
        player = try! AVAudioPlayer.init(contentsOf: Bundle.main.url(forResource: "ringback", withExtension: "wav")!)
        player?.numberOfLoops = -1
        player?.play()
        //audioController.startPlayingSoundFile(pathForSound("ringback.wav"), loop: true)
        
    }
    
    func callDidEstablish(_ call: SINCall)
    {
        player?.pause()
        startCallDurationTimerWithSelector(#selector(CallViewController.onDurationTimer(_:)))
        showButtons(.hangup)
        audioController.stopPlayingSoundFile()
        if call.details.isVideoOffered{
            topView.backgroundColor = UIColor.clear
        }
    }
    
    func callDidEnd(_ call: SINCall)
    {
        audioController.stopPlayingSoundFile()
        stopCallDurationTimer()
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    func callDidAddVideoTrack(_ call: SINCall!) {
        let remoteView = self.videoController!.remoteView()
        //remoteView?.contentMode = .scaleAspectFill
        remoteUserView.addSubview(remoteView!)
        remoteUserView.bringSubviewToFront(localUserView)
    }
    
    func startCallDurationTimerWithSelector(_ selector: Selector) {
        let selectorString  = NSStringFromSelector(selector)
        durationTimer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(CallerViewController.internal_updateDurartion(_:)), userInfo: selectorString, repeats: true)
    }
    
    func stopCallDurationTimer() {
        durationTimer?.invalidate()
        durationTimer = nil
    }
    
    @objc func internal_updateDurartion(_ timer: Timer)
    {
        let selector:Selector = NSSelectorFromString(timer.userInfo as! String)
        
        if self.responds(to: selector)
        {
            self.performSelector(inBackground: selector, with: timer)
        }
    }
}
