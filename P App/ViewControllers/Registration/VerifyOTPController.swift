//
//  VerifyOTPController.swift
//  P App
//
//  Created by Ahmed Shahid on 15/05/2019.
//  Copyright © 2019 Ahmed Shahid. All rights reserved.
//

import UIKit
import FirebaseAuth

class VerifyOTPController: UIViewController {

    @IBOutlet weak var labelTimer: UILabel!
    @IBOutlet weak var textfieldOTPCode: UITextField!
    
    var timer: Timer? = nil
    
    var timerCount: Int = Int(1.5 * 60) // 1.5 mins, 90 seconds
    var verificationID: String? = nil
    var phoneNumberEntered: String? = nil
    var timerCompletedForFirstTime: Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.startTimer()
        // Do any additional setup after loading the view.
        
        textfieldOTPCode.attributedPlaceholder = NSAttributedString(string: "6-digit activation code",
        attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.textfieldOTPCode.becomeFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.timer?.invalidate()
        self.timer = nil
    }
    
    // MARK: - HELPER METHODS
    func startTimer() {
        self.timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { (timerBlock) in
            if self.timerCompletedForFirstTime {
                self.labelTimer.text = "Session will expire in \(String(format: "%02d", self.secondsToHoursMinutesSeconds(seconds: self.timerCount).1)):\(String(format: "%02d", self.secondsToHoursMinutesSeconds(seconds: self.timerCount).2))"
            } else {
                 self.labelTimer.text = "Resend code in \(String(format: "%02d", self.secondsToHoursMinutesSeconds(seconds: self.timerCount).1)):\(String(format: "%02d", self.secondsToHoursMinutesSeconds(seconds: self.timerCount).2))"
                
            }
           
            self.timerCount = self.timerCount - 1
            
            if self.timerCount == -1 {
                timerBlock.invalidate()
                self.timerCount = Int(1.5 * 60)
                if self.timerCompletedForFirstTime {
                    Utility.main.showToast(message: AlertErrorMessages.otpSessionExpire.message())
                    self.navigationController?.popViewController(animated: true)
                } else {
                    self.timerCompletedForFirstTime = true
                    self.labelTimer.text = "Resending code..."
                    self.textfieldOTPCode.isEnabled = false
                    // call firebase send code
                    self.sendOPTToVerify(this: self.phoneNumberEntered ?? "")
                    
                }
                
            }
        })
    }
    
    
    func sendOPTToVerify(this phoneNumber: String) {
        Utility.main.showLoader()
        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { (verificationID, error) in
            if let error = error {
                print(error.localizedDescription)
                Utility.main.hideLoader()
                Utility.main.showToast(message: error.localizedDescription, controller: self)
                return
            }
            // Sign in using the verificationID and the code sent to the user
            // ...
            self.verificationID = verificationID
            self.textfieldOTPCode.isEnabled = true
            Utility.main.hideLoader()
            self.startTimer()
        }
    }
    
    func signInUser(with activationCode: String) {
        
        let credential = PhoneAuthProvider.provider().credential(
            withVerificationID: self.verificationID ?? "",
            verificationCode: activationCode)
        Utility.main.showLoader()
        
        Auth.auth().signIn(with: credential) { (authResult, error) in
            if let error = error {
                self.textfieldOTPCode.text = ""
                self.textfieldOTPCode.isEnabled = true
                self.startTimer()
                Utility.main.showToast(message: error.localizedDescription, controller: self)
                return
            }
            Utility.main.hideLoader()
            self.navigateToAddProfile(with: authResult!, phoneNumber: self.phoneNumberEntered ?? "")
        }
    }
    
    func navigateToAddProfile(with authResult: AuthDataResult, phoneNumber: String) {
        if let controller = AppStoryboard.Login.instance.instantiateViewController(withIdentifier: "AddProfileController") as? AddProfileController {
            controller.authData = authResult
            controller.phoneNumberEntered = phoneNumber
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
    // MARK: - IB ACTIONS
    @IBAction func textfieldActivationCodeTextChange(_ sender: Any) {
        if self.textfieldOTPCode.text?.count == 6 {
            self.textfieldOTPCode.isEnabled = false
            self.signInUser(with: self.textfieldOTPCode.text ?? "")
            
            // in case fail to sign in, we will start timer again from the start
//            self.timerCount = Int(1.5 * 60)
//            self.timerCompletedForFirstTime = false
            self.timer?.invalidate()
            self.timer = nil
        }
    }
    
    
}
