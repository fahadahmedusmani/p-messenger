//
//  EnterOrganizationCodeViewController.swift
//  P App
//
//  Created by Avanza on 26/03/2020.
//  Copyright © 2020 Ahmed Shahid. All rights reserved.
//

import UIKit
import IQKeyboardManager
import CountryPickerView
import FirebaseAuth

class EnterOrganizationCodeViewController: UIViewController {
    
    @IBOutlet weak var btnCountryName: UIButton!
    @IBOutlet weak var barButtonDone: UIBarButtonItem!
    @IBOutlet weak var textfieldOrgCode: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.barButtonDone.isEnabled = false
        //  self.textfieldPhoneNumber.becomeFirstResponder()
        textfieldOrgCode.keyboardType = .default
        btnCountryName.isUserInteractionEnabled = false
        textfieldOrgCode.attributedPlaceholder = NSAttributedString(string: "Enter organization code",
        attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        
        let vc = TermsAndConditionViewController.instantiate(fromAppStoryboard: .Login)
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: false, completion: nil)
        //self.view.endEditing(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.textfieldPhoneNumber.becomeFirstResponder()
        IQKeyboardManager.shared().isEnableAutoToolbar = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        IQKeyboardManager.shared().isEnableAutoToolbar = true
    }
    
    // MARK: HELPER METHOD
    func verifyOrgCode(_ code: String) {
        Utility.main.showLoader()
        UserFireStoreManager.shared.getOrganizationCollection(with: "", sucess: { (responseUser) in
        
            Utility.main.hideLoader()
            if responseUser?.pcode == code{
                
                if let controller = AppStoryboard.Login.instance.instantiateViewController(withIdentifier: "EnterPhoneNumberController") as? EnterPhoneNumberController {
                self.navigationController?.pushViewController(controller, animated: true)
                }
                
            }
            else{
                  Utility.main.showToast(message: AlertErrorMessages.invalidOrganizationCode.message(), controller: self, position: .center)
            }
          //  self.userAlreayExistObj = responseUser
          //  self.populateDataIfUserExist()
        }) { (failure) in
            print(failure.localizedDescription)
        }
    }
    
    func navigateToOTPController(with verificationID: String, phoneNumber: String) {
        if let controller = AppStoryboard.Login.instance.instantiateViewController(withIdentifier: "VerifyOTPController") as? VerifyOTPController {
            controller.title = "\(self.textfieldOrgCode.text ?? "")"
            controller.verificationID = verificationID
            controller.phoneNumberEntered = phoneNumber
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    // MARK: IB ACTIONS
    @IBAction func actionNumber(_ sender: Any) {
        
    }
    @IBAction func actionPhoneNumberTextChange(_ sender: Any) {
        if (self.textfieldOrgCode.text?.count ?? 0) >= 1 {
            self.barButtonDone.isEnabled = true
        } else {
            self.barButtonDone.isEnabled = false
        }
    }
    
    @IBAction func actionDoneBtn(_ sender: Any) {
        if let text = self.textfieldOrgCode.text {
            self.verifyOrgCode("\(text)")
        } else {
            Utility.main.showToast(message: AlertErrorMessages.invalidPhoneNumber.message(), controller: self, position: .center)
        }
        
    }
}
