//
//  EnterPhoneNumberController.swift
//  P App
//
//  Created by Ahmed Shahid on 14/05/2019.
//  Copyright © 2019 Ahmed Shahid. All rights reserved.
//

import UIKit
import IQKeyboardManager
import CountryPickerView
import FirebaseAuth

class EnterPhoneNumberController: UIViewController {

    @IBOutlet weak var btnCountryNum: UIButton!
    @IBOutlet weak var btnCountryName: UIButton!
    @IBOutlet weak var barButtonDone: UIBarButtonItem!
    @IBOutlet weak var textfieldPhoneNumber: UITextField!
    
    var selectedCountry: Country? = nil
    let countryPickerView = CountryPickerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.barButtonDone.isEnabled = false
      //  self.textfieldPhoneNumber.becomeFirstResponder()
        self.countryPickerView.delegate = self
        
        textfieldPhoneNumber.attributedPlaceholder = NSAttributedString(string: "your phone number",
        attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        
        // get the country code by Locale, but if any case of nil, use United States as default.
        if let regionCode = NSLocale.current.regionCode {
            if let country: Country = self.countryPickerView.getCountryByCode(regionCode) {
                self.btnCountryName.setTitle(country.name, for: .normal)
                self.btnCountryNum.setTitle(country.phoneCode, for: .normal)
                self.selectedCountry = country
            } else {
                let country = self.countryPickerView.getCountryByCode("US")
                
                self.btnCountryName.setTitle(country?.name ?? "United States", for: .normal)
                self.btnCountryNum.setTitle(country?.phoneCode ?? "+1", for: .normal)
                self.selectedCountry = country
            }
        } else {
            let country = self.countryPickerView.getCountryByCode("US")
            self.btnCountryName.setTitle(country?.name ?? "United States", for: .normal)
            self.btnCountryNum.setTitle(country?.phoneCode ?? "+1", for: .normal)
            self.selectedCountry = country
        }
     //   let vc = TermsAndConditionViewController.instantiate(fromAppStoryboard: .Login)
      //   vc.modalPresentationStyle = .overCurrentContext
     //    self.present(vc, animated: false, completion: nil)
        //self.view.endEditing(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.textfieldPhoneNumber.becomeFirstResponder()
         IQKeyboardManager.shared().isEnableAutoToolbar = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
         IQKeyboardManager.shared().isEnableAutoToolbar = true
    }
    // MARK: HELPER METHOD
    func sendOPTToVerify(this phoneNumber: String) {
        Utility.main.showLoader()
        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { (verificationID, error) in
            if let error = error {
                print(error.localizedDescription)
                Utility.main.hideLoader()
                Utility.main.showToast(message: error.localizedDescription, controller: self)
                return
            }
            // Sign in using the verificationID and the code sent to the user
            // ...
            Utility.main.hideLoader()
            self.navigateToOTPController(with: verificationID ?? "", phoneNumber: phoneNumber)
        }
    }
    
    func navigateToOTPController(with verificationID: String, phoneNumber: String) {
        if let controller = AppStoryboard.Login.instance.instantiateViewController(withIdentifier: "VerifyOTPController") as? VerifyOTPController {
            controller.title = "\(self.btnCountryNum.titleLabel?.text ?? "") \(self.textfieldPhoneNumber.text ?? "")"
            controller.verificationID = verificationID
            controller.phoneNumberEntered = phoneNumber
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    // MARK: IB ACTIONS
    @IBAction func actionNumber(_ sender: Any) {
         self.countryPickerView.showCountriesList(from: self)
    }
    @IBAction func actionPhoneNumberTextChange(_ sender: Any) {
        if (self.textfieldPhoneNumber.text?.count ?? 0) >= 7 {
             self.barButtonDone.isEnabled = true
        } else {
             self.barButtonDone.isEnabled = false
        }
    }
    
    @IBAction func actionDoneBtn(_ sender: Any) {
        if let text = self.textfieldPhoneNumber.text {
            self.sendOPTToVerify(this: "\(self.btnCountryNum.titleLabel?.text ?? "")\(text)")
        } else {
            Utility.main.showToast(message: AlertErrorMessages.invalidPhoneNumber.message(), controller: self, position: .center)
        }
        
    }
}

// MARK: - CountryPickerView Delegate
extension EnterPhoneNumberController: CountryPickerViewDelegate {
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        self.selectedCountry = country
        self.btnCountryName.setTitle(country.name, for: .normal)
        self.btnCountryNum.setTitle(country.phoneCode, for: .normal)
    }
}
