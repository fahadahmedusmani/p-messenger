//
//  AddProfileController.swift
//  P App
//
//  Created by Ahmed Shahid on 16/05/2019.
//  Copyright © 2019 Ahmed Shahid. All rights reserved.
//

import UIKit
import Firebase
import FirebaseStorage
import SDWebImage

class AddProfileController: UIViewController {

    @IBOutlet weak var btnAddPhoto: UIButton!
    @IBOutlet weak var barBtnDone: UIBarButtonItem!
    @IBOutlet weak var labelNameCharCount: UILabel!
    @IBOutlet weak var textfieldName: UITextField!
    @IBOutlet weak var imageviewProfilePicture: UIImageView!
    var phoneNumberEntered: String? = nil
    public var authData: AuthDataResult? = nil
    var imagePicker: ImagePicker? = nil
    var userAlreayExistObj: User? = nil
    
    
    var isMedicalProfessional: Bool? = false
    var isPatient: Bool? = false
    var isPatientFamilyMemeber: Bool? = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.getUserCollection(with: self.authData?.user.uid ?? "")
        self.textfieldName.delegate = self
        self.textfieldName.becomeFirstResponder()
        self.navigationItem.setHidesBackButton(true, animated:true);
        
        self.imagePicker = ImagePicker.init(presentationController: self, delegate: self)
        
        // Do any additional setup after loading the view.
    }
    
    func getUserCollection(with uid: String) {
        Utility.main.showLoader()
        UserFireStoreManager.shared.getUserCollection(with: uid, sucess: { (responseUser) in
            Utility.main.hideLoader()
            self.userAlreayExistObj = responseUser
            self.populateDataIfUserExist()
        }) { (failure) in
            print(failure.localizedDescription)
        }
    }
    
    func getLast10Digit(with string: String) -> String {
        return String(string.filter("0123456789+".contains).suffix(10))
    }
    
    func populateDataIfUserExist() {
        self.textfieldName.text = self.userAlreayExistObj?.name
        self.labelNameCharCount.text = "\(25 - (self.userAlreayExistObj?.name?.count ?? 0))"
        self.btnAddPhoto.setTitle("", for: .normal)
        self.imageviewProfilePicture.sd_setImage(with: URL(string: self.userAlreayExistObj?.image ?? ""), placeholderImage: UIImage(named: "profilePlaceholder"))
        self.barBtnDone.isEnabled = true
        
        self.isMedicalProfessional = self.userAlreayExistObj?.isMedicalProfessional
        self.isPatient = self.userAlreayExistObj?.isPatient
        self.isPatientFamilyMemeber = self.userAlreayExistObj?.isPatientFamilyMemeber
//        self.imageviewProfilePicture.image = image
    }
    
    @IBAction func actionAddPhoto(_ sender: Any) {
        self.imagePicker?.present(from: self.view)
        
    }
    
    @IBAction func actionDoneBtn(_ sender: Any) {
        
        // Add a new document with a generated ID
        var fcmToken = ""
        Utility.main.getInstantFCMToken { (token) in
            fcmToken = token
            if ((self.imageviewProfilePicture?.image) != nil) {
                UserFireStoreManager.shared.uploadImage(with: self.authData?.user.uid ?? "", image: self.imageviewProfilePicture.image!) { (imageURL) in
                    let numberLast10Digits = self.getLast10Digit(with: self.phoneNumberEntered ?? "")//String(self.phoneNumberEntered?.suffix(10) ?? "")
                    let param: [String : Any] = [
                        "name": self.textfieldName.text?.trim() ?? "",
                        "phoneNumber": self.phoneNumberEntered ?? "",
                        "uid": self.authData?.user.uid ?? "",
                        "dateTime" : Utility.main.getDateString(),
                        "image" : imageURL,
                        "isUserActive" : "1",
                        "isMedicalProfessional" : self.isMedicalProfessional ?? false,
                        "isPatient" : self.isPatient ?? "",
                        "isPatientFamilyMemeber" : self.isPatientFamilyMemeber ?? "",
                        "fcmDeviceToken" : fcmToken,
                        "lastTenDigit" : numberLast10Digits
                    ]
                    
                    Utility.main.showLoader()
                    UserFireStoreManager.shared.registerdUser(with: self.authData?.user.uid ?? "", param: param, isUserCreated: { (_) in
                        let user = User()
                        user.uid = self.authData?.user.uid ?? ""
                        user.name = self.textfieldName.text?.trim() ?? ""
                        user.phoneNumber = self.phoneNumberEntered ?? ""
                        user.dateTime = Utility.main.getDateString()
                        
                        user.isMedicalProfessional = self.isMedicalProfessional ?? false
                        user.isPatient = self.isPatient
                        user.isPatientFamilyMemeber = self.isPatientFamilyMemeber
                        
                        AppStateManager.shared.createUser(with: user)
                        Utility.main.hideLoader()
                        Constants.APP_DELEGATE.setRootViewControllerToHome()
                    }) { (err) in
                        print("Error adding document: \(err)")
                        Utility.main.showToast(message: err.localizedDescription, controller: self)
                        Utility.main.hideLoader()
                    }
                }
            } else {
                
                let numberLast10Digits = self.getLast10Digit(with: self.phoneNumberEntered ?? "")
                let param: [String : Any] = [
                    "name": self.textfieldName.text?.trim() ?? "",
                    "phoneNumber": self.phoneNumberEntered ?? "",
                    "uid": self.authData?.user.uid ?? "",
                    "dateTime" : Utility.main.getDateString(),
                    "image" : "",
                    "isUserActive" : "1",
                    "isMedicalProfessional" : self.isMedicalProfessional ?? false,
                    "isPatient" : self.isPatient ?? "",
                    "isPatientFamilyMemeber" : self.isPatientFamilyMemeber ?? "",
                    "fcmDeviceToken" : fcmToken,
                    "lastTenDigit" : numberLast10Digits
                ]
                
                Utility.main.showLoader()
                UserFireStoreManager.shared.registerdUser(with: self.authData?.user.uid ?? "", param: param, isUserCreated: { (_) in
                    let user = User()
                    user.uid = self.authData?.user.uid ?? ""
                    user.name = self.textfieldName.text?.trim() ?? ""
                    user.phoneNumber = self.phoneNumberEntered ?? ""
                    user.dateTime = Utility.main.getDateString()
                    
                    user.isMedicalProfessional = self.isMedicalProfessional ?? false
                    user.isPatient = self.isPatient
                    user.isPatientFamilyMemeber = self.isPatientFamilyMemeber
                    
                    AppStateManager.shared.createUser(with: user)
                    Utility.main.hideLoader()
                    Constants.APP_DELEGATE.setRootViewControllerToHome()
                }) { (err) in
                    print("Error adding document: \(err)")
                    Utility.main.showToast(message: err.localizedDescription, controller: self)
                    Utility.main.hideLoader()
                }
            }
        }
    }
    

    
}

extension AddProfileController: ImagePickerDelegateInternal {
    func didSelect(image: UIImage?) {
//        print("image selected: \(image)")
        if (image != nil) {
            self.btnAddPhoto.setTitle("", for: .normal)
            self.imageviewProfilePicture.image = image
        }
    }
}

extension AddProfileController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        // verify max length has not been exceeded
        let nsString = textField.text as NSString?
        let text = nsString?.replacingCharacters(in: range, with: string)
        if (text?.count ?? 0 > 25) // 4 was chosen for SSN verification
        {
            // suppress the max length message only when the user is typing
            // easy: pasted data has a length greater than 1; who copy/pastes one character?
            if (text?.count ?? 0 > 1)
            {
                
                // BasicAlert(@"", @"This field accepts a maximum of 4 characters.");
            }
            
            return false;
        }
        self.labelNameCharCount.text = "\(25 - text!.count)"
        (text?.count ?? 0) > 0 ? (self.barBtnDone.isEnabled = true) : (self.barBtnDone.isEnabled = false)
        return true
    }
}
