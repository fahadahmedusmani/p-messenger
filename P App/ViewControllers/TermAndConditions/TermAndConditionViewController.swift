//
//  TermAndConditionViewController.swift
//  P App
//
//  Created by Avanza on 21/02/2020.
//  Copyright © 2020 Ahmed Shahid. All rights reserved.
//

import UIKit
import WebKit


class TermsAndConditionViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var agreeButtom: BottomButton!
    var viewModel : TermsAndConditionViewModel?
    var htmlString:String?
    
    @IBOutlet weak var textView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareView()
        prepareData()
        self.view.endEditing(true)
    }
    
    @IBAction func didTappedAgree(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

//MARK:- UI Methods
extension TermsAndConditionViewController{
    
    func prepareView(){
        
       // self.titleLabel.font = UIFont(name: C.Fonts.font_Bold, size: 18.0)
        self.titleLabel.textColor = UIColor.black
        // self.titleLabel.text = "Security Instructions"
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        //agreeButtom.layer.cornerRadius = 10.0
        textView.layer.cornerRadius = 10.0
        
    }
}

//MARK:- Helper Functions
extension TermsAndConditionViewController{
    
    func prepareData(){
        //viewModel = TermsAndConditionViewModel.init(controller: self)
        self.titleLabel.text = "End User License Agreement (EULA)"
        self.htmlString = "1. Preamble: This Agreement, signed on October 19, 2019 (hereinafter: Effective Date) governs the relationship between you (You, Your) (either an individual or a single entity), (hereinafter: Licensee) and AA, llc., whose principal place of business is Sterling Heights, Michigan USA (hereinafter: Licensor). This Agreement sets the terms, rights, restrictions, and obligations on using all products created by AA, llc., (hereinafter: The Software) created and owned by Licensor, as detailed herein\n\nPLEASE READ THIS EULA CAREFULLY BEFORE DOWNLOADING, INSTALLING OR USING LICENSOR'S SOFTWARE. BY DOWNLOADING, INSTALLING, AND USING THE SOFTWARE, YOU AGREE TO BE BOUND BY THE TERMS OF THIS EULA. IF YOU DO NOT AGREE TO THE TERMS OF THIS EULA, DO NOT DOWNLOAD, INSTALL AND/OR USE THE SOFTWARE AND, IF PRESENTED WITH THE OPTION TO AGREE OR DISAGREE TO THE TERMS, CLICK DISAGREE.\n\nTHIS HIPAA COMPLIANT MESSAGING SERVICE PROVIDE END-TO-END MESSAGE ENCRYPTION. AA, LLC MONITORS, AND STORE MESSAGES. THERE IS ZERO TOLERANCE FOR ABUSIVE USERS  OR OBJECTIONABLE AND INAPPROPRIATE MESSAGES. AA, LLC RESERVE THE RIGHT TO DECLARE MESSAGES IN THAT CATEGORY, FLAGGING THEM OR DELETING THEM AND TERMINATING SUCH USERS’ SERVICES.\n\nIN THIS DOCUMENT AND OTHERS SOFTWARE AND APP TERMS WILL BE USE FOR COMPUTER SOFTWARE AND DEVICE APP RESPECTIVELY. THE TWO TERMS, SOFTWARE AND APP, WILL ALSO BE USED INTERACHANGEBALY.\n\n2. License Grant: Licensor hereby grants Licensee a Personal, Non-assignable & non-transferable, Perpetual, Non-commercial, Without the rights to create derivative works, Non-exclusive license, all with accordance with the terms set forth and other legal restrictions set forth in 3rd party software used while running Software. 1.10 and up. The Trial Version of The Software may be installed and used by Licensee for the sole purpose of communicating, using, trying and evaluating the Software. The App is used by our sister organizations, (call company hereafter). Licensor will provide each company with a unique company code that their users will need to install the App. The Licensee should obtain their company’s unique code through their company’s office administrators .\n\n2.2. The Trial Version of Software/APP may be installed and used by Licensee on any number of systems.\n\n2.3. One Single Use license allows Licensee to activate two copies of the Software on two qualified computers or two tablets or two phones or any two combinations of devices for which it is designated and that are in the sole possession of the Licensee.\n\n2.4. The Single-Use License does not allow the Software to exist on more than two devices at a time.\n\n2.5. Unauthorized copying of the Software is expressly forbidden.\n\n2.6. The licensee's distribution of a Trial Version of the Software to a third party will not entitle Licensee to any compensation from Licensor.\n\n2.7. Licensee may not rent, lease, or lend the Software to anyone.\n\n2.8. Licensee may not use the Software for commercial purposes.\n\n2.9. Licensee may not permanently transfer all of your rights under this EULA, unless We are notified of and consent to the assignment and the assignee agrees to the terms of this EULA.\n\n2.10. Except as and only to the extent permitted in this EULA and by applicable law, the Licensee may not copy, adapt, translate, decompile, reverse engineer, disassemble, modify, or create derivative works of the Software or advertise the Software in any form.\n\n2.11. Without prejudice to any other rights, the Licensor may terminate this EULA if Licensee fails to comply with the terms and conditions of this EULA. In such an event, Licensee must destroy all copies of the Software.\n\n3. Privacy & Consent to Use of Data: The Licensee acknowledges receipt of Licensor's Privacy Policy and that he has read it, understood it and Licensee agrees to it. Licensee also agrees that Licensor and its subsidiaries may collect and use technical and related information, as long as it is in a form that does not personally identify Licensee, to improve Licensors products or to provide services or technologies to Licensee. Licensor monitor messages and filter objectionable messages or abuser users. Upon request from Licensee’s company, the Licensor can terminate any existing Licensee’s privilege to use the App and software. All such users and objectionable messages are filtered within 24 hours of posting.\n\n4. Term & Termination: The Term of this license shall be until terminated. Licensor may terminate this Agreement, including Licensee’s license in the case where Licensee:\n\n4.1. Company request a termination for the licensee. Or company terminate the use of the App/ Software\n\n4.2. post inappropriate and or objectionable messages\n\n4.3. became insolvent or otherwise entered into any liquidation process; or\n\n4.4. exported The Software to any jurisdiction where licensor may not enforce his rights under this agreement in; or\n\n4.5. Licensee was in breach of any of this license's terms and conditions, and such breach was not cured, immediately upon notification; or\n\n4.6. Licensee in breach of any of the terms of clause 2 to this license; or\n\n4.7. The licensee otherwise entered into any arrangement, which caused Licensor to be unable to enforce his rights under this License.\n\n5. Payment: In consideration of the License granted under clause 2, Licensee shall not pay Licensor.\n\n6. Upgrades, Updates, and Fixes: Licensor may provide Licensee, from time to time, with Upgrades, Updates or Fixes, as detailed herein and according to their sole discretion. Licensee hereby warrants to keep The Software up-to-date and install all relevant updates and fixes, and may, at their sole discretion, purchase upgrades, according to the rates set by Licensor, if applicable. Licensor shall provide any update or Fix free of charge; however, nothing in this Agreement shall require Licensor to provide Updates or Fixes.\n\n6.1. Upgrades: for the purpose of this license, an Upgrade shall be a material amendment in The Software, which contains new features and or major performance improvements and shall be marked as a new version number. For example, should the Licensee purchase The Software under version 1.X.X, an upgrade shall commence under number 2.0.0.\n\n6.2. Updates:  for the purpose of this license, an update shall be a minor amendment in The Software, which may contain new features or minor improvements and shall be marked as a new sub-version number. For example, should Licensee purchase The Software under version 1.1.X, an upgrade shall commence under number 1.2.0.\n\n6.3. Fix: for the purpose of this license, a fix shall be a minor amendment in The Software, intended to remove bugs or alter minor features which impair the The Software's functionality. A fix shall be marked as a new sub-sub-version number. For example, should Licensee purchase Software under version 1.1.1, an upgrade shall commence under number 1.1.2.\n\n7. Support: Software is provided under an AS-IS basis and without any support, updates or maintenance. Nothing in this Agreement shall require Licensor to provide Licensee with support or fixes to any bug, failure, mis-performance or other defect in The Software.\n\n7.1. Bug Notification:  Licensee may provide Licensor of details regarding any bug, defect or failure in The Software promptly and with no delay from such event; Licensee shall comply with Licensor's request for information regarding bugs, defects or failures and furnish him with information, screenshots and try to reproduce such bugs, defects or failures.\n\n7.2. Feature Request:  Licensee may request additional features in Software, provided, however, that (i) Licensee shall waive any claim or right in such feature should feature be developed by Licensor; (ii) Licensee shall be prohibited from developing the feature, or disclose such feature request, or feature, to any 3rd party directly competing with Licensor or any 3rd party which may be, following the development of such feature, in direct competition with Licensor; (iii) Licensee warrants that feature does not infringe any 3rd party patent, trademark, trade-secret or any other intellectual property right; and (iv) Licensee developed, envisioned or created the feature solely by himself.\n\n8. Limitation of Liability:  TO THE EXTENT NOT PROHIBITED BY APPLICABLE LAW, IN NO EVENT SHALL LICENSOR BE LIABLE TO LICENSEE OR A THIRD PARTY FOR PERSONAL INJURY, OR ANY INCIDENTAL, SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES WHATSOEVER. THIS INCLUDES, WITHOUT LIMITATION, DAMAGES FOR LOSS OF PROFITS, LOSS OF DATA OR INFORMATION, BUSINESS INTERRUPTION OR ANY OTHER COMMERCIAL DAMAGES OR LOSSES, ARISING OUT OF OR RELATED TO LICENSEE'S USE OR INABILITY TO USE THE SOFTWARE OR SERVICES OR ANY THIRD PARTY SOFTWARE OR APPLICATIONS IN CONJUNCTION WITH THE SOFTWARE HOWEVER CAUSED, REGARDLESS OF THE THEORY OF LIABILITY (CONTRACT, TORT OR OTHERWISE) AND EVEN IF LICENSOR HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE LIMITATION OF LIABILITY FOR PERSONAL INJURY, OR OF INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THIS LIMITATION MAY NOT APPLY TO LICENSEE. REGARDLESS, IN NO EVENT SHALL LICENSOR'S TOTAL LIABILITY TO LICENSEE FOR ALL DAMAGES EXCEED THE AMOUNT OF FIFTY DOLLARS ($50.00). THE FOREGOING LIMITATIONS WILL APPLY EVEN IF THE ABOVE STATED REMEDY FAILS OF ITS ESSENTIAL PURPOSE.\n\n9. Warranty:\n\n9.1. No-Warranty: LICENSEE ASSUMES THE ENTIRE RISK AS TO THE USE, QUALITY AND PERFORMANCE OF THE SOFTWARE. THE SOFTWARE AND ACCOMPANYING WRITTEN MATERIALS ARE PROVIDED ON AN AS IS AND AS AVAILABLE BASIS WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED OF ANY KIND AND LICENSOR SPECIFICALLY DISCLAIMS WARRANTIES OF FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABILITY AND NON-INFRINGMENT OF ANY THIRD PARTY RIGHTS. NO ORAL OR WRITTEN ADVICE GIVEN BY LICENSOR, ITS DEALERS, DISTRIBUTORS, AGENTS OR EMPLOYEES SHALL CREATE A WARRANTY OR IN ANY WAY INCREASE THE SCOPE OF THIS WARRANTY AND LICENSEE MAY NOT RELY UPON SUCH INFORMATION OR ADVICE.\n\n9.2. Prior Inspection:  Licensee hereby states that he inspected The Software thoroughly and found it satisfactory and adequate to his needs, that it does not interfere with his regular operation and that it does meet the standards and scope of his computer systems and architecture. Licensee found that The Software interacts with his development, website and server environment and that it does not infringe any of End User License Agreement of any software Licensee may use in performing his services. Licensee hereby waives any claims regarding The Software's incompatibility, performance, results and features, and warrants that he inspected the The Software.\n\n10. No Refunds: Licensee warrants that he inspected The Software according to clause 8(2) and that it is adequate to his needs. Accordingly, as The Software is intangible goods, Licensee shall not be, ever, entitled to any refund, rebate, compensation or restitution for any reason whatsoever, even if The Software contains material flaws. Licensor can however choose to refund Licensee after a written inquiry by Licensee on a voluntary basis.\n\n11. Indemnification: Licensee hereby warrants to hold Licensor harmless and indemnify Licensor for any lawsuit brought against it in regards to Licensee’s use of The Software in means that violate, breach or otherwise circumvent this license, Licensor's intellectual property rights or Licensor's title in The Software. Licensor shall promptly notify Licensee in case of such legal action and request Licensee’s consent prior to any settlement in relation to such lawsuit or claim.\n\n12. Governing Law, Jurisdiction: Licensee hereby agrees not to initiate class-action lawsuits against Licensor in relation to this license and to compensate Licensor for any legal fees, cost or attorney fees should any claim brought by Licensee against Licensor be denied, in part or in full."
        
        TermsAndConditionFetched()
    }
    
    
}

//MARK:- View Model Delegates
extension TermsAndConditionViewController : TermsAndConditionViewModelDelegate{
    func showAlert(title: String, message: String) {
      
    }
    
    func TermsAndConditionFetched() {
     //   let font = UIFont.init(name: C.Fonts.font_Regular, size: 14.0)
        let color = UIColor.black
        self.textView.text = htmlString
       // self.webView.loadHTMLString("<span style=\"; line-height: 20px; color: \(color)\"> \(self.htmlString ?? "")", baseURL: nil)
    }
    
}

