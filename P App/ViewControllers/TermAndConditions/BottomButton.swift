//
//  BottomButton.swift
//  P App
//
//  Created by Avanza on 21/02/2020.
//  Copyright © 2020 Ahmed Shahid. All rights reserved.
//

import UIKit

public class BottomButton: UIButton {
    
    public static var bgColor : UIColor = UIColor.red
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupButton()
    }
    
    public func setTitle(color:UIColor, size:CGFloat,familyName:String){
        self.setTitleColor(color, for: .normal)
        self.titleLabel?.font = UIFont(name: familyName, size: size)
    }
    
    public func setupButton(){
        self.backgroundColor = BottomButton.bgColor
        self.setTitleColor(UIColor.white, for: .normal)
        //self.titleLabel?.font = UIFont(name: C.Fonts.font_Bold, size: 20.0)
    }
    public func setBorderOnBtn(radius:CGFloat){
        self.layer.cornerRadius = radius
    }
    public func setBackGroundColor(color:UIColor){
        self.backgroundColor = color
        
    }
    func setBackGroundImage(bgImage:UIImage ){
        self.setBackgroundImage(bgImage, for: .normal)
        self.backgroundColor = UIColor.clear
    }
}
