//
//  ContactsViewController.swift
//  P App
//
//  Created by Ahmed Shahid on 01/06/2019.
//  Copyright © 2019 Ahmed Shahid. All rights reserved.
//

import UIKit
import Contacts
import CountryPickerView

enum ContactControllerType {
    case OnlyCreateGroup
    case ShowContactsAndCreateGroup
}

class ContactsViewController: UIViewController {
    
    
    @IBOutlet weak var constraintBtnCreateGroupHeight: NSLayoutConstraint!
    @IBOutlet weak var btnCreateGroup: UIButton!
    @IBOutlet weak var tableview: UITableView!
    let store = CNContactStore()
    var contacts = [CNContact]()
    var searchArray = [CNContact]()
    var selectedForGroup = [CNContact]()
    var searchEnable: Bool = false
    var controllerType: ContactControllerType?
    var selectedContactsForArray: [User]? = [User]()
    
    @IBOutlet weak var textfieldSearch: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let keys = [CNContactFormatter.descriptorForRequiredKeys(for: .fullName),CNContactPhoneNumbersKey] as [Any]
        let request = CNContactFetchRequest(keysToFetch: keys as! [CNKeyDescriptor])
        
        do {
            try self.store.enumerateContacts(with: request) {
                (contact, stop) in
                // Array containing all unified contacts from everywhere
                self.contacts.append(contact)
            }
        }
        catch {
            print("unable to fetch contacts")
        }
        
        self.contacts.sort { (contact1, contact2) -> Bool in
            return contact1.givenName < contact2.givenName
        }
        
        if self.controllerType == .OnlyCreateGroup {
            navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.actionCreateGroupDoneButton(_:)))
            self.btnCreateGroup.isHidden = true
            self.constraintBtnCreateGroupHeight.constant = 0
        }
        print(self.contacts)
        // Do any additional setup after loading the view.
    }
    
    func adjustPhoneNumberWithCountryCode(with phoneNumber: String) -> String {
        
        let countryPicker = CountryPickerView()
        var returnPhoneNumber = phoneNumber
        if let regionCode = NSLocale.current.regionCode {
            if let country: Country = countryPicker.getCountryByCode(regionCode) {
                if phoneNumber.hasPrefix(country.phoneCode) {
                    return returnPhoneNumber
                } else if phoneNumber.hasPrefix("+") {
                    return returnPhoneNumber
                }
                else {
                    returnPhoneNumber.removeFirst()
                    returnPhoneNumber = country.phoneCode + returnPhoneNumber
                }
            }
        }
       return returnPhoneNumber
    }
    
    func handleTheNoUserFoundResponse() {
        Utility.main.showAlert(message: AlertErrorMessages.NoContactFound.message(), title: AlertErrorMessages.alertTitle.message(), controller: self) { (yesAction, noAction) in
            if yesAction != nil {
                Utility.main.showShareMenu(with: AlertErrorMessages.tellAFriendText.message(), on: self)
            }
        }
    }
    
    func pushChatController(with user: User?) {
        
        if user?.phoneNumber == AppStateManager.shared.loggedInUser?.phoneNumber{
            Utility.main.showAlert(message: "Chat with own account is not allowed", title: "Alert")
            return
        }
        
        if let controller = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "ChatController") as? ChatController {
            controller.recipientUser = user
            controller.controllerType = .oneToOneChat
            
            let uidCombination = Utility.main.getThreadID(with: AppStateManager.shared.loggedInUser?.uid ?? "", with: user?.uid ?? "")
            
            let thread = (UIApplication.shared.delegate as! AppDelegate).threads?.first(where: {$0.combineUid == uidCombination})
            controller.threadID = thread?.threadID ?? ""
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func getPhoneNumArrayFromSelectedContacts() -> [String] {
        var returnArray = [String]()
        for phoneNumber in self.selectedForGroup {
            returnArray.append( (phoneNumber.phoneNumbers.first?.value)?.value(forKey: "digits") as? String ?? "" )
        }
        
        return returnArray
    }
    @IBAction func searchTextEditingEnd(_ sender: Any) {
        self.searchEnable = false
        self.textfieldSearch.text = ""
        self.tableview.reloadData()
    }
    
    @IBAction func searchTextEditing(_ sender: Any) {
        if textfieldSearch.text?.isEmpty ?? false {
            self.searchEnable = false
        } else {
            self.searchEnable = true
            searchArray = self.contacts.filter{
                ($0.givenName + " " + $0.familyName).lowercased().contains(self.textfieldSearch.text?.lowercased() ?? "")
            }
        }
        self.tableview.reloadData()
    }

    @objc func actionCreateGroupDoneButton(_ sender: UIButton) {
        if (self.selectedContactsForArray?.count ?? 0) > 0 {
//            Utility.main.showLoader()
//            ContactsFireStoreManager.shared.getMultipleUsers(by: self.getPhoneNumArrayFromSelectedContacts(), sucess: { (usersArray) in
//                print("Users \(usersArray)")
//                Utility.main.hideLoader()
                if let controller = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "CreateGroupController") as? CreateGroupController {
                    controller.groupMembersArray = self.selectedContactsForArray
                    self.navigationController?.pushViewController(controller, animated: true)
                }
//            }) { (err) in
//                print(err.localizedDescription)
//            }

        }
    }
    
    @IBAction func btnPremier(_ sender: Any) {
        Utility.main.openWebURL(webURL: Constants.PREMIER_WEB_URL)
    }
    
    @IBAction func btnAdvanceDirectiveNow(_ sender: Any) {
        Utility.main.openWebURL(webURL: Constants.ADVANCE_DIRECT_NOW_WEB_URL)
    }
    
    @IBAction func btnMedSupply(_ sender: Any) {
        Utility.main.openWebURL(webURL: Constants.MED_SUPPLY_WEB_URL)
    }
    
    @IBAction func btnMHP(_ sender: Any) {
        Utility.main.openWebURL(webURL: Constants.MHP_WEB_URL_WEB_URL)
    }
    
    @IBAction func btnGoogle(_ sender: Any) {
        Utility.main.openWebURL(webURL: Constants.GOOGLE_WEB_URL)
    }
    
    @IBAction func btnAmazon(_ sender: Any) {
        Utility.main.openWebURL(webURL: Constants.AMAZON_WEB_URL)
    }
    
    @IBAction func actionCreateGroup(_ sender: UIButton) {
        if let controller = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "ContactsViewController") as? ContactsViewController {
            controller.controllerType = .OnlyCreateGroup
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}


// MARK: - UITableView DataSource & Delegate
extension ContactsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.searchEnable {
            return self.searchArray.count
        } else {
            return self.contacts.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //        cell.textLabel?.text = (self.contacts[indexPath.row].phoneNumbers.first?.value)?.value(forKey: "digits") as? String
        
        let cell = UITableViewCell()
        var arrayToCheck = [CNContact]()
        if self.searchEnable {
            arrayToCheck = self.searchArray
        } else {
            arrayToCheck = self.contacts
        }
        
        cell.textLabel?.text = arrayToCheck[indexPath.row].givenName + " " + arrayToCheck[indexPath.row].familyName
        
        if let index = self.selectedForGroup.firstIndex(where: {
            $0.identifier == arrayToCheck[indexPath.row].identifier
        }) {
            // user exist remove it
           
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
            
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if self.controllerType == .OnlyCreateGroup {
            
             var phoneNumber = ""
            if self.searchEnable {
                phoneNumber = self.adjustPhoneNumberWithCountryCode(with: (self.searchArray[indexPath.row].phoneNumbers.first?.value)?.value(forKey: "digits") as? String ?? "")
            } else {
                phoneNumber = self.adjustPhoneNumberWithCountryCode(with: (self.contacts[indexPath.row].phoneNumbers.first?.value)?.value(forKey: "digits") as? String ?? "")
            }
            if phoneNumber != "" {
                Utility.main.showLoader()
                let numberLast10Digits = String(phoneNumber.suffix(10))
                ContactsFireStoreManager.shared.getUser(from: numberLast10Digits, sucess: { (responseUser) in
                    if let cell = tableView.cellForRow(at: indexPath) {
                        if self.searchEnable {
                            if let index = self.selectedForGroup.firstIndex(where: {
                                $0.identifier == self.searchArray[indexPath.row].identifier
                            }) {
                                // user exist remove it
                                self.selectedContactsForArray?.remove(at: index)
                                self.selectedForGroup.remove(at: index)
                                cell.accessoryType = .none
                            } else {
                                self.selectedContactsForArray?.append(responseUser!)
                                self.selectedForGroup.append(self.searchArray[indexPath.row])
                                cell.accessoryType = .checkmark
                            }
                        } else {
                            if let index = self.selectedForGroup.firstIndex(where: {
                                $0.identifier == self.contacts[indexPath.row].identifier
                            }) {
                                // user exist remove it
                                cell.accessoryType = .none
                                self.selectedForGroup.remove(at: index)
                                self.selectedContactsForArray?.remove(at: index)
                            } else {
                                cell.accessoryType = .checkmark
                                self.selectedForGroup.append(self.contacts[indexPath.row])
                                self.selectedContactsForArray?.append(responseUser!)
                            }
                        }
                    }
                    Utility.main.hideLoader()
                }) { (failure) in
                    self.handleTheNoUserFoundResponse()
                    Utility.main.hideLoader()
                }
            }
            

        } else {
            print("\((self.contacts[indexPath.row].phoneNumbers.first?.value)?.value(forKey: "digits") as? String ?? "")")
            
            var phoneNumber = ""
            if self.searchEnable {
                phoneNumber = self.adjustPhoneNumberWithCountryCode(with: (self.searchArray[indexPath.row].phoneNumbers.first?.value)?.value(forKey: "digits") as? String ?? "")
            } else {
                phoneNumber = self.adjustPhoneNumberWithCountryCode(with: (self.contacts[indexPath.row].phoneNumbers.first?.value)?.value(forKey: "digits") as? String ?? "") 
            }
            if phoneNumber != "" {
                Utility.main.showLoader()
                let numberLast10Digits = String(phoneNumber.suffix(10))
                ContactsFireStoreManager.shared.getUser(from: numberLast10Digits, sucess: { (responseUser) in
                    self.pushChatController(with: responseUser)
                    Utility.main.hideLoader()
                }) { (failure) in
                    self.handleTheNoUserFoundResponse()
                    Utility.main.hideLoader()
                }
            }
            
        }
    }
}
