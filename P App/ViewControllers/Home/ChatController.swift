//
//  ChatController.swift
//  P App
//
//  Created by Ahmed Shahid on 24/05/2019.
//  Copyright © 2019 Ahmed Shahid. All rights reserved.
//

import UIKit
import IQKeyboardManager
import ALTextInputBar
import FirebaseStorage
import MessageUI
import ImagePicker
import Sinch

enum ChatControllerType {
    case groupChat
    case oneToOneChat
}

class ChatController: UIViewController, ALTextInputBarDelegate {
    
    @IBOutlet weak var constraintInputBarBottom: NSLayoutConstraint!
    @IBOutlet weak var textInputBar: ALTextInputBar!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var btnImageAdd: UIButton!
    @IBOutlet weak var replyView: UIView!
    @IBOutlet weak var replyTitleLabel: UILabel!
    @IBOutlet weak var replyMessageLabel: UILabel!
    @IBOutlet weak var replyImageView: UIView!
    @IBOutlet weak var replyImageTitle: UILabel!
    @IBOutlet weak var replyImage: UIImageView!
    
    var recipientUser: User? = nil
    var recipientMemebers: [User]? = nil
    var threadArray: [Thread] = [Thread]()
    
    var client: SINClient {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.client!
    }
    
    //var sinch : SINClient?
    //var call : SINCallClient?
    //var call1 : SINCall?
    var call : SINCallClient?
    
    var group: Group? = nil
    var groupID: String? = nil
    var tap: UITapGestureRecognizer? = nil
    
    var uidCombination: String? = nil
    
    var chatMessagesArray: [ChatMessage]? = [ChatMessage]()
    var grpMembersUidsForGroup: [String]? = [String]()
    var membersUidsForGroup: [String]? = [String]()
    var controllerType: ChatControllerType?
    
    var threadID: String? = nil
    var senderUID: String?
    
    var createGroupBecauseImNew: Bool? = nil
    //    var listener
    //    var listener: ListenerRegistration? = nil
    
    override func awakeFromNib() {
        //self.client.call().delegate = self
        //call = self.client.call()
        //call?.delegate = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let rightItem1 = UIBarButtonItem(image: #imageLiteral(resourceName: "rsz_communications"), style: .plain, target: self, action: #selector(self.makeCall))
        
        let rightItem2 = UIBarButtonItem(image: #imageLiteral(resourceName: "rsz_technology"), style: .plain, target: self, action: #selector(self.makeVideoCall))
        
        navigationItem.rightBarButtonItems = [rightItem1, rightItem2]
        /*sinch = Sinch.client(withApplicationKey: "1deac554-efe9-4b9a-95af-e76b3c1cca59", applicationSecret: "0HO+2A4Df0ukpqnKhAytmw==", environmentHost: "clientapi.sinch.com", userId: "+923313622195")
        
        sinch?.setSupportCalling(true)
        sinch?.enableManagedPushNotifications()
        sinch?.delegate = self
        sinch?.start()
        sinch?.startListeningOnActiveConnection()
        */
        self.btnImageAdd.layer.cornerRadius = 5
        
        self.uiPreSettings()
        
        self.uidCombination = Utility.main.getThreadID(with: AppStateManager.shared.loggedInUser?.uid ?? "", with: self.recipientUser?.uid ?? "")
        
        //        self.getAllMessagesOfThisThread()
        if self.controllerType == .groupChat {
            self.getRealTimeMessages(with: self.groupID ?? "")
        } else {
            self.getRealTimeMessages(with: self.uidCombination ?? "")
        }
        
        // Do any additional setup after loading the view.
        //Long Press
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        longPressGesture.minimumPressDuration = 0.3
        self.tableview.addGestureRecognizer(longPressGesture)
    }
    
    @objc func handleLongPress(longPressGesture: UILongPressGestureRecognizer) {
        let p = longPressGesture.location(in: self.tableview)
        let indexPath = self.tableview.indexPathForRow(at: p)
        if indexPath == nil {
            print("Long press on table view, not row.")
        } else if longPressGesture.state == UIGestureRecognizer.State.began {
            print("Long press on row, at \(indexPath!.row)")
            performActions(index: indexPath!.row)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        IQKeyboardManager.shared().isEnableAutoToolbar = false
        
        
        //        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        //        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardFrameChanged(notification:)), name: NSNotification.Name(rawValue: ALKeyboardFrameDidChangeNotification), object: nil)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        IQKeyboardManager.shared().isEnableAutoToolbar = true
        //        self.listener.sto
        
        //        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        //        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardDidHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: ALKeyboardFrameDidChangeNotification), object: nil)
    }
    
    //    func fetchChatFromLocalDB() {
    //        self.messagesArray = MessageRealmHelper.sharedInstance.getThreadMessages(with: self.doctorRecipientEmail ?? "")
    //
    //        self.tableview.reloadData()
    //        if self.messagesArray.count ?? 0 > 0 {
    //            self.tableview.scrollToRow(at: IndexPath(row: (self.messagesArray.count) - 1, section: 0), at: UITableViewScrollPosition.bottom, animated: false)
    //        }
    //    }
    
    func uiPreSettings() {
        
        if self.controllerType == .groupChat {
            self.title = self.group?.groupName
        } else {
            self.title = self.recipientUser?.name
        }
        
        self.navigationController?.navigationBar.backItem?.title = ""
        
        self.navigationController?.navigationBar.tintColor = UIColor.black
        self.textInputBar.textView.placeholder = "Type here"
        textInputBar.showTextViewBorder = true
        //        self.textInputBar.textView.delegate = self
        self.textInputBar.delegate = self
        self.configureTextInputBar()
        self.tableview.keyboardDismissMode = .interactive
        
        self.tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        
        self.view.addGestureRecognizer(self.tap!)
    }
    func configureTextInputBar() {
        let rightButton = UIButton(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
        
        rightButton.setImage(UIImage(named: "sendMessageArrow"), for: .normal)
        rightButton.addTarget(self, action: #selector(self.actionSendBtnPress(_:)), for: .touchUpInside)
        textInputBar.showTextViewBorder = true
        textInputBar.rightView = rightButton
        //        textInputBar.frame = CGRect(x: 0, y: view.frame.size.height - textInputBar.defaultHeight, width: view.frame.size.width, height: textInputBar.defaultHeight)
        textInputBar.backgroundColor = UIColor(white: 0.95, alpha: 1)
        
    }
    
    func scrollToLast() {
        if (self.chatMessagesArray?.count ?? 0) > 0 {
            self.tableview.scrollToRow(at: IndexPath(row: (self.chatMessagesArray?.count ?? 1) - 1, section: 0), at: .bottom, animated: false)
        }
    }
    
    func sendViaMessage(with text: String) {
        if (MFMessageComposeViewController.canSendText()) {
            let controller = MFMessageComposeViewController()
            controller.body = text
            controller.recipients = [self.recipientUser?.phoneNumber ?? ""]
            controller.messageComposeDelegate = self
            self.present(controller, animated: true, completion: nil)
        } else {
            Utility.main.showToast(message: "This device does not support messages", controller: self)
        }
    }
    
    func getmembersUidsForGroup() -> [String] {
        if (self.grpMembersUidsForGroup?.count ?? 0) > 0 {
            return self.grpMembersUidsForGroup ?? [String]()
        } else {
            for user in self.group?.groupMembers ?? [User]() {
                if user.uid != AppStateManager.shared.loggedInUser?.uid {
                    self.grpMembersUidsForGroup?.append(user.uid ?? "")
                }
            }
        }
        return self.grpMembersUidsForGroup ?? [String]()
    }
    
    func getGroupMembersUids() -> [String] {
        if (self.membersUidsForGroup?.count ?? 0) > 0 {
            return self.membersUidsForGroup ?? [String]()
        } else {
            for members in self.group?.groupMembers ?? [User]() {
                self.membersUidsForGroup?.append(members.uid ?? "")
            }
        }
        
        return self.membersUidsForGroup ?? [String]()
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) { // Change `2.0` to the desired number of seconds.
                if self.constraintInputBarBottom.constant == 0 {
                    //                    self.constraintInputBarBottom.constant = keyboardHeight * -1
                }
            }
            self.view.addGestureRecognizer(self.tap!)
        }
    }
    
    @objc func keyboardDidHide(_ notification: Notification) {
        self.constraintInputBarBottom.constant = 0
        self.view.removeGestureRecognizer(self.tap!)
    }
    
    @objc func keyboardFrameChanged(notification: NSNotification) {
        //        self.constraintInputBarBottom.constant = 0
    }
    
    @objc func handleTap(_ recognizer: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    @objc func actionSendBtnPress(_ sender: UIButton) {
        if self.textInputBar.textView.text != "" {
            if self.controllerType == .groupChat {
                self.messageSend(with: self.textInputBar.text, messageType: MessageType.textMessage.rawValue, replyName: replyTitleLabel.text ?? "", replyType: "0", replyText: replyMessageLabel.text ?? "")
            } else {
                if Reachability.isConnectedToNetwork() {
                    if self.chatMessagesArray?.count == 0 { // if no messages between these, first create there thread.
                        self.createThread(threadCreatedSuccessfully: {(threadCreated) in
                            if self.replyView.isHidden && self.replyImageView.isHidden{
                                self.messageSend(with: self.textInputBar.text, messageType:  MessageType.textMessage.rawValue, replyName: "", replyType: "", replyText: "")
                            }
                            else if self.replyView.isHidden{
                                self.messageSend(with: self.textInputBar.text, messageType: MessageType.textMessage.rawValue, replyName: self.replyImageTitle.text ?? "", replyType: "1", replyText: self.chatMessagesArray?[sender.tag].message ?? "")
                            }
                            else{
                                self.messageSend(with: self.textInputBar.text, messageType:  MessageType.imageMessage.rawValue, replyName: self.replyTitleLabel.text ?? "", replyType: "0", replyText: self.replyMessageLabel.text ?? "")
                            }
                        })
                    } else {
                        if self.replyView.isHidden && self.replyImageView.isHidden{
                            self.messageSend(with: self.textInputBar.text, messageType:  MessageType.textMessage.rawValue, replyName: "", replyType: "", replyText: "")
                        }
                        else if self.replyView.isHidden{
                            self.messageSend(with: self.textInputBar.text, messageType: MessageType.textMessage.rawValue, replyName: self.replyImageTitle.text ?? "", replyType: "1", replyText: self.chatMessagesArray?[self.replyImageView.tag].message ?? "")
                        }
                        else{
                            self.messageSend(with: self.textInputBar.text, messageType:  MessageType.textMessage.rawValue, replyName: replyTitleLabel.text ?? "", replyType: "0", replyText: replyMessageLabel.text ?? "")
                        }
                    }
                } else {
                    // no internet
                    Utility.main.showAlert(message: AlertErrorMessages.NoInternetSendViaSMS.message(), title: AlertErrorMessages.alertTitle.message(), controller: self) { (yesAction, noAction) in
                        if yesAction != nil {
                            self.sendViaMessage(with: self.textInputBar.text)
                        }
                    }
                }
            }
        } else {
            // Utility.main.showToast(message: "can not send message, please try again or later")
        }
        self.replyView.isHidden = true
        self.replyImageView.isHidden = true
    }
    
    @IBAction func actionBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnPremier(_ sender: Any) {
        Utility.main.openWebURL(webURL: Constants.PREMIER_WEB_URL)
    }
    
    @IBAction func btnAdvanceDirectiveNow(_ sender: Any) {
        Utility.main.openWebURL(webURL: Constants.ADVANCE_DIRECT_NOW_WEB_URL)
    }
    
    @IBAction func btnMedSupply(_ sender: Any) {
        Utility.main.openWebURL(webURL: Constants.MED_SUPPLY_WEB_URL)
    }
    
    @IBAction func btnMHP(_ sender: Any) {
        Utility.main.openWebURL(webURL: Constants.MHP_WEB_URL_WEB_URL)
    }
    
    @IBAction func btnGoogle(_ sender: Any) {
        Utility.main.openWebURL(webURL: Constants.GOOGLE_WEB_URL)
    }
    
    @IBAction func btnAmazon(_ sender: Any) {
        Utility.main.openWebURL(webURL: Constants.AMAZON_WEB_URL)
    }
    
    @IBAction func actionAddImage(_ sender: Any) {
        let imagePickerController = ImagePickerController()
        imagePickerController.imageLimit = 1
        imagePickerController.delegate = self
        present(imagePickerController, animated: true, completion: nil)
    }
    
    @objc func actionImageView(_ sender: UIButton) {
        if let controller = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "FullSizeImageController") as? FullSizeImageController {
            controller.imageURLString = self.chatMessagesArray?[sender.tag].message ?? ""
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    
    
    @objc func actionLongPressGesture(_ sender: CustomizeLongPressGestureRecognizer) {
        
        let alert = UIAlertController.init(title: "", message: "", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Copy", style: .default, handler: { Copy in
            
            UIPasteboard.general.string = self.chatMessagesArray?[sender.index!].message ?? ""
            Utility.main.showToast(message: "Text Copied", controller: self)
        }))
        
        alert.addAction(UIAlertAction(title: "Reply", style: .default, handler: {reply in
            if self.chatMessagesArray?[sender.index!].messageType == MessageType.textMessage.rawValue || self.chatMessagesArray?[sender.index!].replyType == "0"{
                self.replyView.isHidden = false
                self.replyTitleLabel.text = self.chatMessagesArray?[sender.index!].senderName
                self.replyMessageLabel.text = self.chatMessagesArray?[sender.index!].message
            }
            else{
                self.replyImageView.tag = sender.index ?? 0
                self.replyImageView.isHidden = false
                self.replyImageTitle.text = self.chatMessagesArray?[sender.index!].senderName
                if let url = URL(string: self.chatMessagesArray?[sender.index!].message ?? "") {
                    self.replyImage.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder-image"))
                }
                else if let url = URL(string: self.chatMessagesArray?[sender.index!].replyText ?? "") {
                    self.replyImage.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder-image"))
                }
                else {
                    self.replyImage.image = UIImage(named: "placeholder-image")
                }
            }
        }))
        
        if chatMessagesArray?[sender.index!].messageType == MessageType.textMessage.rawValue{
            alert.addAction(UIAlertAction(title: "Priority", style: .default, handler: { priority in
                if let index = sender.index{
                    self.markPriority(index: index)
                }
            }))
        }
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true)
    }
    
    @objc func makeCall(){
        if self.client.isStarted(){
            let user = AppStateManager.shared.loggedInUser
            let headers = ["callerName":    user?.name ?? "",
            "callerPhone": user?.phoneNumber ?? "",
            "callerPic": user?.image ?? ""]
            weak var call: SINCall? = client.call()?.callUser(withId: senderUID, headers: headers)
            
                    performSegue(withIdentifier: "callView", sender: call)
         return
        }
    }
    
    @objc func makeVideoCall(){
        if self.client.isStarted(){
            let user = AppStateManager.shared.loggedInUser
            let headers = ["callerName": user?.name ?? "",
            "callerPhone": user?.phoneNumber ?? "",
            "callerPic": user?.image ?? ""]
            
            weak var call: SINCall? = client.call()?.callUserVideo(withId: senderUID, headers: headers)
                    performSegue(withIdentifier: "callView", sender: call)
         return
        }
    }
    
    func performActions(index: Int){
        
        let alert = UIAlertController.init(title: "", message: "", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Copy", style: .default, handler: { Copy in
            
            UIPasteboard.general.string = self.chatMessagesArray?[index].message ?? ""
            Utility.main.showToast(message: "Text Copied", controller: self)
        }))
        
        alert.addAction(UIAlertAction(title: "Reply", style: .default, handler: {reply in
            if self.chatMessagesArray?[index].messageType == MessageType.textMessage.rawValue || self.chatMessagesArray?[index].replyType == "0"{
                self.replyView.isHidden = false
                self.replyTitleLabel.text = self.chatMessagesArray?[index].senderName
                self.replyMessageLabel.text = self.chatMessagesArray?[index].message
            }
            else{
                self.replyImageView.tag = index ?? 0
                self.replyImageView.isHidden = false
                self.replyImageTitle.text = self.chatMessagesArray?[index].senderName
                if let url = URL(string: self.chatMessagesArray?[index].message ?? "") {
                    self.replyImage.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder-image"))
                }
                else if let url = URL(string: self.chatMessagesArray?[index].replyText ?? "") {
                    self.replyImage.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder-image"))
                }
                else {
                    self.replyImage.image = UIImage(named: "placeholder-image")
                }
            }
        }))
        
        if chatMessagesArray?[index].messageType == MessageType.textMessage.rawValue{
            alert.addAction(UIAlertAction(title: "Priority", style: .default, handler: { priority in
                //if let index = index{
                    self.markPriority(index: index)
                //}
            }))
        }
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true)
    }
    
    func markPriority(index: Int){
        
        let message = self.chatMessagesArray?[index]
        if message?.priority == "1"{
            message?.priority = "0"
        }
        else{
            message?.priority = "1"
        }
        
        let id = self.controllerType == .groupChat ? self.groupID : self.uidCombination
        ChatFireStoreManager.shared.updatePriorityMessage(with: message?.priority ?? "", threadID: message?.documentId ?? "", uidCombination: id ?? "", sucess: { (success) in
            if success!{
                print("HURRAY")
                //self.getRealTimeMessages(with: id ?? "")
                  self.chatMessagesArray?[index] = message!
                DispatchQueue.main.async {
                    self.tableview.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
                }

              
                
                //self.tableview.reloadData()
                //self.scrollToLast()
                if self.controllerType == .groupChat{
                    FCMPushServiceModel.shared.sendPushNotificationInGroup(fcmTokens: self.getmembersUidsForGroup(), senderName: AppStateManager.shared.loggedInUser?.name ?? "", message: "This message has been set to priority \(message?.message ?? "")", groupName: self.title ?? "")
                }
                else{
                    FCMPushServiceModel.sendPushNotificationTo(uid: self.recipientUser?.uid ?? "", senderName: AppStateManager.shared.loggedInUser?.name ?? "", message: "This message has been set to priority \(message?.message ?? "")")
                }
            }
        }, failure: { (err) in
            Utility.main.showToast(message: "Unable to update priority\nReason: \(err.localizedDescription)")
        })
        
    }
 
    
    @IBAction func didTappedReplyCancel(_ sender: Any) {
        replyView.isHidden = true
    }
    
    @IBAction func didTappedReplyImageCancel(_ sender: Any) {
        replyImageView.isHidden = true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        let callViewController = segue.destination as? CallerViewController
        callViewController?.call = (sender as! SINCall)
        callViewController?.sender = self.recipientUser
    }
    
}

// MARK: - REAL TIME LISTENER & API CALLING
extension ChatController {
    
    func messageSend(with text: String, messageType: String, replyName: String, replyType: String, replyText: String) {
        guard let messageTemp = self.textInputBar.text else { return }
        let dateTime: String = Utility.main.getDateString()
        if self.controllerType == .groupChat {
            ChatFireStoreManager.shared.sendGroupMessage(with: text, senderUid: AppStateManager.shared.loggedInUser?.uid ?? "", groupID: self.groupID ?? "", senderName: AppStateManager.shared.loggedInUser?.name ?? "", priority: "0", replyName: replyName, replyType: replyType, replyText: replyText, messageType: messageType, messengSent: { (messageSent) in
                Utility.main.showToast(message: "Group Message Sent")
                if self.threadID != nil{
                    self.updateThreadLastMessage(with: messageType == MessageType.imageMessage.rawValue ? "🖼️ Image" : text, dateTime: dateTime)
                }
                FCMPushServiceModel.shared.sendPushNotificationInGroup(fcmTokens: self.getmembersUidsForGroup(), senderName: AppStateManager.shared.loggedInUser?.name ?? "", message: messageType == MessageType.imageMessage.rawValue ? "Image" : messageTemp, groupName: self.title ?? "")
            }) { (err) in
                Utility.main.showToast(message: "Message failed\nReason: \(err.localizedDescription)")
            }
        } else {
            
            ChatFireStoreManager.shared.sendMessage(with: text, senderUid: AppStateManager.shared.loggedInUser?.uid ?? "", uidCombination: Utility.main.getThreadID(with: AppStateManager.shared.loggedInUser?.uid ?? "", with: self.recipientUser?.uid ?? ""), senderName: AppStateManager.shared.loggedInUser?.name ?? "", dateTime: dateTime, messageType: messageType, priority: "0", replyName: replyName, replyType: replyType, replyText: replyText, messengSent: { (messageSent) in
                
                // update his thread with last message
                self.updateThreadLastMessage(with: messageType == MessageType.imageMessage.rawValue ? "🖼️ Image" : text, dateTime: dateTime)
                
                // send push of message
                FCMPushServiceModel.sendPushNotificationTo(uid: self.recipientUser?.uid ?? "", senderName: AppStateManager.shared.loggedInUser?.name ?? "", message: messageType == MessageType.imageMessage.rawValue ? "Image" : messageTemp)
            }) { (err) in
                Utility.main.showToast(message: "Message failed\nReason: \(err.localizedDescription)")
            }
        }
        
        self.textInputBar.textView.text = ""
    }
    
    func getRealTimeMessages(with chatDocID: String) {
        /* self.listener =*/ Global.db.collection(FireStoreCollection.AllMessagesCollection).document(chatDocID).collection(FireStoreCollection.MessagesCollection).order(by: ChatMessage.CodingKeys.dateTime.stringValue(), descending: false).addSnapshotListener { documentSnapshot, error in
            guard let documents = documentSnapshot?.documents else {
                print("Error fetching document: \(error!)")
                return
            }
            
            if self.threadID != nil && !(self.threadID?.isEmpty ?? false) {
                if self.controllerType == .oneToOneChat {
                    ChatFireStoreManager.shared.editThreadDataIHaveSeenLstMsg(threadID: self.threadID ?? "", sucess: nil, failure: nil)
                } else {
                    ChatFireStoreManager.shared.editThreadDataIHaveSeenLstMsg(threadID: self.threadID ?? "", sucess: nil, failure: nil)
                }
            }
            
            self.chatMessagesArray?.removeAll()
            for document in documents {
                self.chatMessagesArray?.append(ChatMessage(with: document.data() as NSDictionary, documentId: document.documentID))
            }
            
            // create group If you are new
            if self.controllerType == .groupChat {
                if self.createGroupBecauseImNew ?? false {
                    self.createThreadForGroup(threadCreatedSuccessfully: { (threadCreate) in
                        // yes I m using this
                    })
                }
            }
            
            
            self.tableview.reloadData()
            self.scrollToLast()
        }
    }
    
    //    func getAllMessagesOfThisThread() {
    //        Utility.main.showLoader()
    //        ChatFireStoreManager.shared.getMessages(with: self.uidCombination ?? "", success: { (chatMessagesArray) in
    //            self.chatMessagesArray = chatMessagesArray
    //            self.tableview.reloadData()
    //            self.scrollToLast()
    //            self.getRealTimeMessages()
    //            Utility.main.hideLoader()
    //        }) { (failure) in
    //            Utility.main.hideLoader()
    //            Utility.main.showToast(message: "Unable to fetch chat at this moment", controller: self)
    //        }
    //    }
    
    // if no message in thread and this message is first message, its means, there thread have'nt created...
    func createThread(threadCreatedSuccessfully: @escaping (Bool) -> Void) {
        Utility.main.showLoader()
        ChatFireStoreManager.shared.createThread(senderUid: self.recipientUser?.uid ?? "", uidCombination: self.uidCombination ?? "", senderObject: self.recipientUser?.dictionaryRepresentation() ?? NSDictionary(), threadIDHasBeenCreated: {(threadID) in
            self.threadID = threadID
        }, threadCreated: { (threadCreated) in
            if threadCreated {
                threadCreatedSuccessfully(true)
                Utility.main.hideLoader()
                //                Utility.main.showToast(message: "Thread Created Successfully", controller: self)
            }
        }) { (err) in
            Utility.main.hideLoader()
            Utility.main.showToast(message: "Unable to create thread at this moment", controller: self)
        }
    }
    
    func createThreadForGroup(threadCreatedSuccessfully: @escaping (Bool) -> Void) {
        Utility.main.showLoader()
        ChatFireStoreManager.shared.createGroupThread(senderUid: AppStateManager.shared.loggedInUser?.uid ?? "", groupID: self.groupID ?? "", groupObject: self.group?.dictionaryRepresentation() ?? NSDictionary(), otherMembers: self.recipientMemebers!, threadCreated: { (threadCreated) in
            
            if threadCreated {
                threadCreatedSuccessfully(true)
                Utility.main.hideLoader()
                //                Utility.main.showToast(message: "Thread Created Successfully", controller: self)
                self.createGroupBecauseImNew = false
            }
        }) { (err) in
            Utility.main.hideLoader()
            Utility.main.showToast(message: "Unable to create thread at this moment", controller: self)
        }
        //        ChatFireStoreManager.shared.createThread(senderUid: self.recipientUser?.uid ?? "", uidCombination: self.uidCombination ?? "", senderObject: self.recipientUser?.dictionaryRepresentation() ?? NSDictionary(), threadCreated: { (threadCreated) in
        //            if threadCreated {
        //                threadCreatedSuccessfully(true)
        //                Utility.main.hideLoader()
        //                Utility.main.showToast(message: "Thread Created Successfully", controller: self)
        //            }
        //        }) { (err) in
        //            Utility.main.hideLoader()
        //            Utility.main.showToast(message: "Unable to create thread at this moment", controller: self)
        //        }
    }
    
    func  updateThreadLastMessage(with message: String, dateTime: String) {
        if self.controllerType == .groupChat {
            ChatFireStoreManager.shared.editThreadDataForGroup(lastMessage: message, dateTime: dateTime, threadID: self.threadID ?? "", groupID: self.groupID ?? "", senderUids: self.getGroupMembersUids(), sucess: { (threadUpdated) in
                //                Utility.main.showToast(message: "My own Thread group updated, yahooooooo", controller: self)
            }) { (err) in
                Utility.main.showToast(message: "Unable to update thread at this moment", controller: self)
            }
        } else {
            ChatFireStoreManager.shared.editThreadData(lastMessage: message, dateTime: dateTime, senderUid: self.recipientUser?.uid ?? "", threadID: self.threadID ?? "", combineID: self.uidCombination ?? "", sucess: { (threadUpdated) in
                //                Utility.main.showToast(message: "Thread updated, yahooooooo", controller: self)
            }) { (err) in
                Utility.main.showToast(message: "Unable to update thread at this moment", controller: self)
            }
        }
    }
    
    func uploadImage(with image: UIImage, index: Int) {
        if Reachability.isConnectedToNetwork() {
            if self.chatMessagesArray?.count == 0 { // if no messages between these, first create there thread.
                self.createThread(threadCreatedSuccessfully: {(threadCreated) in
                    UserFireStoreManager.shared.uploadImage(with: AppStateManager.shared.loggedInUser?.uid ?? "", chatID: self.uidCombination ?? "", image: image) { (imageURL) in
                        
                        if self.replyView.isHidden && self.replyImage.isHidden{
                            self.messageSend(with: imageURL, messageType:  MessageType.imageMessage.rawValue, replyName: "", replyType: "", replyText: "")
                        }
                        else if self.replyView.isHidden{
                            self.messageSend(with: imageURL, messageType:  MessageType.imageMessage.rawValue, replyName: self.replyImageTitle.text ?? "", replyType: "1", replyText: self.chatMessagesArray?[index].message ?? "")
                        }
                        else{
                            self.messageSend(with: imageURL, messageType:  MessageType.imageMessage.rawValue, replyName: self.replyTitleLabel.text ?? "", replyType: "1", replyText: self.replyMessageLabel.text ?? "")
                        }
                    }
                })
            } else {
                
                UserFireStoreManager.shared.uploadImage(with: AppStateManager.shared.loggedInUser?.uid ?? "", chatID: self.uidCombination ?? "", image: image) { (imageURL) in
                    // self.messageSend(with: imageURL, messageType:  MessageType.imageMessage.rawValue, replyName: "", replyType: "", replyText: "")
                    //}
                    if self.replyView.isHidden && self.replyImageView.isHidden{
                        self.messageSend(with: imageURL, messageType:  MessageType.imageMessage.rawValue, replyName: "", replyType: "", replyText: "")
                    }
                    else if self.replyView.isHidden{
                        self.messageSend(with: imageURL, messageType:  MessageType.imageMessage.rawValue, replyName: self.replyImageTitle.text ?? "", replyType: "1", replyText: self.chatMessagesArray?[index].message ?? "")
                    }
                    else{
                        self.messageSend(with: imageURL, messageType:  MessageType.imageMessage.rawValue, replyName: self.replyTitleLabel.text ?? "", replyType: "0", replyText: self.replyMessageLabel.text ?? "")
                    }
                    
                    self.replyView.isHidden = true
                    self.replyImageView.isHidden = true
                }
                /* }
                 else{
                 
                 UserFireStoreManager.shared.uploadImage(with: AppStateManager.shared.loggedInUser?.uid ?? "", chatID: self.uidCombination ?? "", image: image) { (imageURL) in
                 self.messageSend(with: imageURL, messageType:  MessageType.imageMessage.rawValue, replyName: self.replyImageTitle.text ?? "", replyType: "1", replyText: self.chatMessagesArray?[index].message ?? "")
                 }
                 }*/
            }
        }else {
            Utility.main.showToast(message: "Internet is not connected to this device.", controller: self)
            self.replyView.isHidden = true
            self.replyImageView.isHidden = true
        }
        
        
    }
}

// MARK: - ImagePickerDelegate
extension ChatController: ImagePickerDelegate {
    
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        self.uploadImage(with: images.first ?? UIImage(), index: replyImageView.tag)
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
}
// MARK: - UITABLEVIEW DATASOURCE & DELEGATE
extension ChatController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.chatMessagesArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (self.chatMessagesArray?[indexPath.row].messageType ?? "0") == MessageType.textMessage.rawValue {
            
            if !(self.chatMessagesArray?[indexPath.row].replyText ?? "").isEmpty{
                
                if self.chatMessagesArray?[indexPath.row].sendUid == AppStateManager.shared.loggedInUser?.uid{
                    
                    
                    if self.chatMessagesArray?[indexPath.row].replyType == "1"{
                        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MessageImageReply", for: indexPath) as? MessageImageReplyCell else {
                            return UITableViewCell()
                        }
                        
                        if let url = URL(string: self.chatMessagesArray?[indexPath.row].replyText ?? "") {
                            cell.replyImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder-image"))
                        } else {
                            cell.replyImageView.image = UIImage(named: "placeholder-image")
                        }
                        
                        // cell.labelMessageText.text = self.chatMessagesArray?[indexPath.row].message ?? ""
                        cell.labelReplyTitle.text = self.chatMessagesArray?[indexPath.row].replyName ?? ""
                        cell.labelMessageText.text = self.chatMessagesArray?[indexPath.row].message ?? ""
                        
                       /* let longGesture = CustomizeLongPressGestureRecognizer(target: self, action: #selector(self.actionLongPressGesture(_:)))
                        let index = self.chatMessagesArray?.firstIndex{$0 === self.chatMessagesArray?[indexPath.row]}
                        longGesture.index = index//indexPath.row
                        cell.addGestureRecognizer(longGesture)*/
                        
                        cell.labelMessageDate.text = Utility.main.getDateInLocalFormat(date: self.chatMessagesArray?[indexPath.row].dateTime ?? "")// date?.offset()
                        
                        return cell
                    }
                        
                    else{
                        
                        guard let cell = tableView.dequeueReusableCell(withIdentifier: "TextToTextReplyCell", for: indexPath) as? ReplyMessageCell else {
                            return UITableViewCell()
                        }
                        
                        cell.labelMessageText.text = self.chatMessagesArray?[indexPath.row].message ?? ""
                        cell.labelReplyTitle.text = self.chatMessagesArray?[indexPath.row].replyName ?? ""
                        cell.labelReplyMessage.text = self.chatMessagesArray?[indexPath.row].replyText ?? ""
                        
                        /*let longGesture = CustomizeLongPressGestureRecognizer(target: self, action: #selector(self.actionLongPressGesture(_:)))
                         let index = self.chatMessagesArray?.firstIndex{$0 === self.chatMessagesArray?[indexPath.row]}
                        longGesture.index = index//indexPath.row
                        cell.addGestureRecognizer(longGesture)*/
                        
                        cell.labelMessageDate.text = Utility.main.getDateInLocalFormat(date: self.chatMessagesArray?[indexPath.row].dateTime ?? "")// date?.offset()
                        
                        return cell
                        
                        
                    }
                }
                else{
                    
                    if self.chatMessagesArray?[indexPath.row].replyType == "1"{
                        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MessageImageReplyReceived", for: indexPath) as? ReplyImageMessageReceivedCell else {
                            return UITableViewCell()
                        }
                        
                        if let url = URL(string: self.chatMessagesArray?[indexPath.row].replyText ?? "") {
                            cell.replyImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder-image"))
                        } else {
                            cell.replyImageView.image = UIImage(named: "placeholder-image")
                        }
                        
                        // cell.labelMessageText.text = self.chatMessagesArray?[indexPath.row].message ?? ""
                        cell.labelReplyTitle.text = self.chatMessagesArray?[indexPath.row].replyName ?? ""
                        cell.labelMessageText.text = self.chatMessagesArray?[indexPath.row].message ?? ""
                        
                        /*let longGesture = CustomizeLongPressGestureRecognizer(target: self, action: #selector(self.actionLongPressGesture(_:)))
                        let index = self.chatMessagesArray?.firstIndex{$0 === self.chatMessagesArray?[indexPath.row]}
                        longGesture.index = index//indexPath.row
                        cell.addGestureRecognizer(longGesture)*/
                        
                        cell.labelMessageDate.text = Utility.main.getDateInLocalFormat(date: self.chatMessagesArray?[indexPath.row].dateTime ?? "")// date?.offset()
                        
                        return cell
                    }
                        
                    else{
                        guard let cell = tableView.dequeueReusableCell(withIdentifier: "TextToTextReplyReceived", for: indexPath) as? ReplyMessageReceivedCell else {
                            return UITableViewCell()
                        }
                        
                        cell.labelMessageText.text = self.chatMessagesArray?[indexPath.row].message ?? ""
                        cell.labelReplyTitle.text = self.chatMessagesArray?[indexPath.row].replyName ?? ""
                        cell.labelReplyMessage.text = self.chatMessagesArray?[indexPath.row].replyText ?? ""
                        
                        /*let longGesture = CustomizeLongPressGestureRecognizer(target: self, action: #selector(self.actionLongPressGesture(_:)))
                        let index = self.chatMessagesArray?.firstIndex{$0 === self.chatMessagesArray?[indexPath.row]}
                        longGesture.index = index//indexPath.row
                        cell.addGestureRecognizer(longGesture)*/
                        
                        cell.labelMessageDate.text = Utility.main.getDateInLocalFormat(date: self.chatMessagesArray?[indexPath.row].dateTime ?? "")// date?.offset()
                        
                        if self.chatMessagesArray?[indexPath.row].priority == "1"{
                            cell.labelMessageText.textColor = UIColor.red
                        }
                        else{
                            cell.labelMessageText.textColor = UIColor.black
                        }
                        return cell
                    }
                }
            }
            else{
                guard let cell = tableView.dequeueReusableCell(withIdentifier: (self.chatMessagesArray?[indexPath.row].sendUid == AppStateManager.shared.loggedInUser?.uid) ? "MessageSend" : "MessageReceived", for: indexPath) as? MessageCell else {
                    return UITableViewCell()
                }
                cell.labelMessageText.text = self.chatMessagesArray?[indexPath.row].message ?? ""
                
                /*let longGesture = CustomizeLongPressGestureRecognizer(target: self, action: #selector(self.actionLongPressGesture(_:)))
                let index = chatMessagesArray?.firstIndex{$0 === self.chatMessagesArray?[indexPath.row]}
                longGesture.index = index//indexPath.row
                cell.addGestureRecognizer(longGesture)*/
                
                cell.labelMessageDate.text = Utility.main.getDateInLocalFormat(date: self.chatMessagesArray?[indexPath.row].dateTime ?? "")// date?.offset()
                
                if self.chatMessagesArray?[indexPath.row].sendUid != AppStateManager.shared.loggedInUser?.uid {
                    if self.controllerType == .groupChat {
                        cell.labelSenderName.isHidden = false
                        cell.labelSenderName.text = self.chatMessagesArray?[indexPath.row].senderName
                    } else {
                        cell.labelSenderName.isHidden = true
                        cell.labelSenderName.text = ""//self.chatMessagesArray?[indexPath.row].senderName
                    }
                }
                if self.chatMessagesArray?[indexPath.row].priority == "1"{
                    cell.labelMessageText.textColor = UIColor.red
                }
                else{
                    cell.labelMessageText.textColor = UIColor.black
                }
                return cell
                // }
                
                
                
                
                
                
                //            let dateformatter = DateFormatter()
                //            dateformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                //            let date: Date? = dateformatter.date(from: self.chatMessagesArray?[indexPath.row].dateTime ?? "")
                //
                
            }
        } else {
            
            if !(self.chatMessagesArray?[indexPath.row].replyText ?? "").isEmpty{
                
                if self.chatMessagesArray?[indexPath.row].sendUid == AppStateManager.shared.loggedInUser?.uid{
                    
                    if self.chatMessagesArray?[indexPath.row].replyType == "1"{
                        
                        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ImageToImageReplyCell", for: indexPath) as? ImageToImageReplyCell else {
                            return UITableViewCell()
                        }
                        
                        /*let longGesture = CustomizeLongPressGestureRecognizer(target: self, action: #selector(self.actionLongPressGesture(_:)))
                        let index = chatMessagesArray?.firstIndex{$0 === self.chatMessagesArray?[indexPath.row]}
                        longGesture.index = index//indexPath.row
                        cell.addGestureRecognizer(longGesture)*/
                        
                        //cell.btnImageView.tag = indexPath.row
                        //cell.btnImageView.addTarget(self, action: #selector(self.actionImageView(_:)), for: .touchUpInside)
                        
                        if let url = URL(string: self.chatMessagesArray?[indexPath.row].replyText ?? "") {
                            cell.replyImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder-image"))
                        } else {
                            cell.replyImageView.image = UIImage(named: "placeholder-image")
                        }
                        
                        if let url = URL(string: self.chatMessagesArray?[indexPath.row].message ?? "") {
                            cell.imageViewMessage.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder-image"))
                        } else {
                            cell.imageViewMessage.image = UIImage(named: "placeholder-image")
                        }
                        
                        cell.labelReplyTitle.text = self.chatMessagesArray?[indexPath.row].replyName ?? ""
                        // cell.labelMessageText.text = self.chatMessagesArray?[indexPath.row].message ?? ""
                        /*let dateformatter = DateFormatter()
                         dateformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                         let date: Date? = dateformatter.date(from: self.chatMessagesArray?[indexPath.row].dateTime ?? "")
                         dateformatter.timeZone = TimeZone.current*/
                        cell.labelMessageDate.text = Utility.main.getDateInLocalFormat(date: self.chatMessagesArray?[indexPath.row].dateTime ?? "")//dateformatter.string(from: date ?? Date())//self.chatMessagesArray?[indexPath.row].dateTime ?? ""//date?.offset()
                        
                        if self.chatMessagesArray?[indexPath.row].sendUid != AppStateManager.shared.loggedInUser?.uid {
                            if self.controllerType == .groupChat {
                                cell.labelSenderName.isHidden = false
                                cell.labelSenderName.text = self.chatMessagesArray?[indexPath.row].senderName
                            } else {
                                //  cell.labelSenderName.isHidden = true
                                //  cell.labelSenderName.text = ""//self.chatMessagesArray?[indexPath.row].senderName
                            }
                        }
                        return cell
                    }
                        
                    else{
                        guard let cell = tableView.dequeueReusableCell(withIdentifier: "TextToImageReplyCell", for: indexPath) as? TextToImageReplyCell else {
                            return UITableViewCell()
                        }
                        
                        /*let longGesture = CustomizeLongPressGestureRecognizer(target: self, action: #selector(self.actionLongPressGesture(_:)))
                       let index = chatMessagesArray?.firstIndex{$0 === self.chatMessagesArray?[indexPath.row]}
                        longGesture.index = index//indexPath.row
                        cell.addGestureRecognizer(longGesture)*/
                        
                        //cell.btnImageView.tag = indexPath.row
                        //cell.btnImageView.addTarget(self, action: #selector(self.actionImageView(_:)), for: .touchUpInside)
                        
                        if let url = URL(string: self.chatMessagesArray?[indexPath.row].message ?? "") {
                            cell.imageViewMessage.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder-image"))
                        } else {
                            cell.imageViewMessage.image = UIImage(named: "placeholder-image")
                        }
                        cell.labelReplyTitle.text = self.chatMessagesArray?[indexPath.row].replyName ?? ""
                        cell.labelReplyMessage.text = self.chatMessagesArray?[indexPath.row].replyText ?? ""
                        /*let dateformatter = DateFormatter()
                         dateformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                         let date: Date? = dateformatter.date(from: self.chatMessagesArray?[indexPath.row].dateTime ?? "")
                         dateformatter.timeZone = TimeZone.current*/
                        cell.labelMessageDate.text = Utility.main.getDateInLocalFormat(date: self.chatMessagesArray?[indexPath.row].dateTime ?? "")//dateformatter.string(from: date ?? Date())//self.chatMessagesArray?[indexPath.row].dateTime ?? ""//date?.offset()
                        
                        if self.chatMessagesArray?[indexPath.row].sendUid != AppStateManager.shared.loggedInUser?.uid {
                            if self.controllerType == .groupChat {
                                cell.labelSenderName.isHidden = false
                                cell.labelSenderName.text = self.chatMessagesArray?[indexPath.row].senderName
                            } else {
                                //  cell.labelSenderName.isHidden = true
                                //  cell.labelSenderName.text = ""//self.chatMessagesArray?[indexPath.row].senderName
                            }
                        }
                        return cell
                    }
                }
                    
                else{
                    
                    
                    if self.chatMessagesArray?[indexPath.row].replyType == "1"{
                        
                        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ImageToImageReplyReceivedCell", for: indexPath) as? ImageToImageReplyReceivedCell else {
                            return UITableViewCell()
                        }
                        
                       /* let longGesture = CustomizeLongPressGestureRecognizer(target: self, action: #selector(self.actionLongPressGesture(_:)))
                        let index = chatMessagesArray?.firstIndex{$0 === self.chatMessagesArray?[indexPath.row]}
                        longGesture.index = index//indexPath.row
                        cell.addGestureRecognizer(longGesture)*/
                        
                        //cell.btnImageView.tag = indexPath.row
                        //cell.btnImageView.addTarget(self, action: #selector(self.actionImageView(_:)), for: .touchUpInside)
                        
                        if let url = URL(string: self.chatMessagesArray?[indexPath.row].replyText ?? "") {
                            cell.replyImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder-image"))
                        } else {
                            cell.replyImageView.image = UIImage(named: "placeholder-image")
                        }
                        
                        if let url = URL(string: self.chatMessagesArray?[indexPath.row].message ?? "") {
                            cell.imageViewMessage.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder-image"))
                        } else {
                            cell.imageViewMessage.image = UIImage(named: "placeholder-image")
                        }
                        
                        cell.labelReplyTitle.text = self.chatMessagesArray?[indexPath.row].replyName ?? ""
                        // cell.labelMessageText.text = self.chatMessagesArray?[indexPath.row].message ?? ""
                        /*let dateformatter = DateFormatter()
                         dateformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                         let date: Date? = dateformatter.date(from: self.chatMessagesArray?[indexPath.row].dateTime ?? "")
                         dateformatter.timeZone = TimeZone.current*/
                        cell.labelMessageDate.text = Utility.main.getDateInLocalFormat(date: self.chatMessagesArray?[indexPath.row].dateTime ?? "")//dateformatter.string(from: date ?? Date())//self.chatMessagesArray?[indexPath.row].dateTime ?? ""//date?.offset()
                        
                        if self.chatMessagesArray?[indexPath.row].sendUid != AppStateManager.shared.loggedInUser?.uid {
                            if self.controllerType == .groupChat {
                                cell.labelSenderName.isHidden = false
                                cell.labelSenderName.text = self.chatMessagesArray?[indexPath.row].senderName
                            } else {
                                //  cell.labelSenderName.isHidden = true
                                //  cell.labelSenderName.text = ""//self.chatMessagesArray?[indexPath.row].senderName
                            }
                        }
                        return cell
                    }
                        
                    else{
                        guard let cell = tableView.dequeueReusableCell(withIdentifier: "TextToImageReplyReceivedCell", for: indexPath) as? TextToImageReplyReceivedCell else {
                            return UITableViewCell()
                        }
                        
                        /*let longGesture = CustomizeLongPressGestureRecognizer(target: self, action: #selector(self.actionLongPressGesture(_:)))
                         let index = chatMessagesArray?.firstIndex{$0 === self.chatMessagesArray?[indexPath.row]}
                        longGesture.index = index//indexPath.row
                        cell.addGestureRecognizer(longGesture)*/
                        
                        //cell.btnImageView.tag = indexPath.row
                        //cell.btnImageView.addTarget(self, action: #selector(self.actionImageView(_:)), for: .touchUpInside)
                        
                        if let url = URL(string: self.chatMessagesArray?[indexPath.row].message ?? "") {
                            cell.imageViewMessage.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder-image"))
                        } else {
                            cell.imageViewMessage.image = UIImage(named: "placeholder-image")
                        }
                        cell.labelReplyTitle.text = self.chatMessagesArray?[indexPath.row].replyName ?? ""
                        cell.labelReplyMessage.text = self.chatMessagesArray?[indexPath.row].replyText ?? ""
                        /*let dateformatter = DateFormatter()
                         dateformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                         let date: Date? = dateformatter.date(from: self.chatMessagesArray?[indexPath.row].dateTime ?? "")
                         dateformatter.timeZone = TimeZone.current*/
                        cell.labelMessageDate.text = Utility.main.getDateInLocalFormat(date: self.chatMessagesArray?[indexPath.row].dateTime ?? "")//dateformatter.string(from: date ?? Date())//self.chatMessagesArray?[indexPath.row].dateTime ?? ""//date?.offset()
                        
                        if self.chatMessagesArray?[indexPath.row].sendUid != AppStateManager.shared.loggedInUser?.uid {
                            if self.controllerType == .groupChat {
                                cell.labelSenderName.isHidden = false
                                cell.labelSenderName.text = self.chatMessagesArray?[indexPath.row].senderName
                            } else {
                                //  cell.labelSenderName.isHidden = true
                                //  cell.labelSenderName.text = ""//self.chatMessagesArray?[indexPath.row].senderName
                            }
                        }
                        return cell
                    }
                }
            }
            else{
                
                guard let cell = tableView.dequeueReusableCell(withIdentifier: (self.chatMessagesArray?[indexPath.row].sendUid == AppStateManager.shared.loggedInUser?.uid) ? "ImageMessageSend" : "ImageMessageReceived", for: indexPath) as? ImageMessageCell else {
                    return UITableViewCell()
                }
                
                /*let longGesture = CustomizeLongPressGestureRecognizer(target: self, action: #selector(self.actionLongPressGesture(_:)))
                let index = chatMessagesArray?.firstIndex{$0 === self.chatMessagesArray?[indexPath.row]}
                longGesture.index = index//indexPath.row
                cell.addGestureRecognizer(longGesture)*/
                
                cell.btnImageView.tag = indexPath.row
                cell.btnImageView.addTarget(self, action: #selector(self.actionImageView(_:)), for: .touchUpInside)
                
                if let url = URL(string: self.chatMessagesArray?[indexPath.row].message ?? "") {
                    cell.imageViewMessage.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder-image"))
                } else {
                    cell.imageViewMessage.image = UIImage(named: "placeholder-image")
                }
                
                /*let dateformatter = DateFormatter()
                 dateformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                 let date: Date? = dateformatter.date(from: self.chatMessagesArray?[indexPath.row].dateTime ?? "")
                 dateformatter.timeZone = TimeZone.current*/
                cell.labelMessageDate.text = Utility.main.getDateInLocalFormat(date: self.chatMessagesArray?[indexPath.row].dateTime ?? "")//dateformatter.string(from: date ?? Date())//self.chatMessagesArray?[indexPath.row].dateTime ?? ""//date?.offset()
                
                if self.chatMessagesArray?[indexPath.row].sendUid != AppStateManager.shared.loggedInUser?.uid {
                    if self.controllerType == .groupChat {
                        cell.labelSenderName.isHidden = false
                        cell.labelSenderName.text = self.chatMessagesArray?[indexPath.row].senderName
                    } else {
                        cell.labelSenderName.isHidden = true
                        cell.labelSenderName.text = ""//self.chatMessagesArray?[indexPath.row].senderName
                    }
                }
                
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        //sinch?.call()
    }
}

extension ChatController: MFMessageComposeViewControllerDelegate {
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        //... handle sms screen actions
        self.dismiss(animated: true, completion: nil)
    }
    
}


class CustomizeLongPressGestureRecognizer: UILongPressGestureRecognizer {
    var index: Int? = 0
}

extension ChatController: SINCallClientDelegate, SINCallDelegate, SINClientDelegate{
    
    func client(_ client: SINCallClient!, didReceiveIncomingCall call: SINCall!) {
        print("received")
        performSegue(withIdentifier: "callView", sender: call)
        //call.answer()
    }
    
   
    
    func callDidProgress(_ call: SINCall!) {
        print("progress")
    }
    
    func callDidEnd(_ call: SINCall!) {
        print("end")
    }
    
    func callDidEstablish(_ call: SINCall!) {
        print("establish")
    }
   
    func clientDidStart(_ client: SINClient!) {
        print("started")
        print(client.audioController())
    }
    
    func clientDidFail(_ client: SINClient!, error: Error!) {
        
    }
    
    
}
