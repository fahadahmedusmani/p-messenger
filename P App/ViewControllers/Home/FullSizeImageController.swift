//
//  FullSizeImageController.swift
//  P App
//
//  Created by Ahmed Shahid on 15/09/2019.
//  Copyright © 2019 Ahmed Shahid. All rights reserved.
//

import UIKit
import SDWebImage

class FullSizeImageController: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var scrollview: UIScrollView!
    
    public var imageURLString : String? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
         // Do any additional setup after loading the view.
        self.scrollview.delegate = self
        self.scrollview.minimumZoomScale = 1
        self.scrollview.maximumZoomScale = 6
        let doubleTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.actionImageDoubleTap(_:)))
        doubleTapGesture.numberOfTapsRequired = 2
        self.scrollview.addGestureRecognizer(doubleTapGesture)
        
        if let url = URL(string: imageURLString ?? "") {
            self.imageView.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder-image"))
        } else {
            self.imageView.image = UIImage(named: "placeholder-image")
        }
        
       
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
    @IBAction func actionCrossBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func actionImageDoubleTap(_ sender: UITapGestureRecognizer) {
        if scrollview.zoomScale == 1 {
            scrollview.zoom(to: zoomRectForScale(scale: scrollview.maximumZoomScale, center: sender.location(in: sender.view)), animated: true)
        }
        else {
            scrollview.setZoomScale(1, animated: true)
        }
    }
    
    func zoomRectForScale(scale: CGFloat, center: CGPoint) -> CGRect {
        var zoomRect = CGRect.zero
        zoomRect.size.height = imageView.frame.size.height / scale
        zoomRect.size.width  = imageView.frame.size.width  / scale
        let newCenter = imageView.convert(center, from: self.scrollview)
        zoomRect.origin.x = newCenter.x - (zoomRect.size.width / 2.0)
        zoomRect.origin.y = newCenter.y - (zoomRect.size.height / 2.0)
        return zoomRect
    }
    
}
