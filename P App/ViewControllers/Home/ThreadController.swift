//
//  ChatController.swift
//  P App
//
//  Created by Ahmed Shahid on 18/05/2019.
//  Copyright © 2019 Ahmed Shahid. All rights reserved.
//

import UIKit
import Sinch

class ThreadController: UIViewController {

    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var label1NoData: UILabel!
    
    @IBOutlet weak var viewNoData: UIView!
    
    /*var client: SINClient {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.client!
    }*/
    
    var isSearching: Bool? = false
//    var selectedThread: String? = nil
    var threadArray: [Thread] = [Thread]()
    var searchArray: [Thread] = [Thread]()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.getRealTimeThreads()
        self.createComposeImageLabel()
        
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.searchBar.delegate = self;
        searchController.searchBar.backgroundColor = UIColor.white
        searchController.obscuresBackgroundDuringPresentation = false
        navigationItem.searchController = searchController
        definesPresentationContext = true
        
//        navigationItem.hidesSearchBarWhenScrolling = false

        
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        self.viewWillAppear(animated)
////        self.tabBarController?.tabBar.isHidden = false
//    }
//    
//    override func viewWillDisappear(_ animated: Bool) {
//        self.viewWillDisappear(animated)
////        self.tabBarController?.tabBar.isHidden = true
//    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let userId = AppStateManager.shared.loggedInUser?.uid
        NotificationCenter.default.post(name: NSNotification.Name("UserDidLoginNotification"), object: nil, userInfo: ["userId": userId])
    }
     
    func createComposeImageLabel() {
        //Create Attachment
        let imageAttachment =  NSTextAttachment()
        imageAttachment.image = UIImage(named:"composeIcon")
        //Set bound to reposition
        let imageOffsetY:CGFloat = -5.0;
        imageAttachment.bounds = CGRect(x: 0, y: imageOffsetY, width: imageAttachment.image!.size.width, height: imageAttachment.image!.size.height)
        //Create string with attachment
        let attachmentString = NSAttributedString(attachment: imageAttachment)
        //Initialize mutable string
        let completeText = NSMutableAttributedString(string: "Tap on ")
        //Add image to mutable string
        completeText.append(attachmentString)
        //Add your text to mutable string
        let  textAfterIcon = NSMutableAttributedString(string: " in the top right corner to start a new chat.")
        completeText.append(textAfterIcon)
        self.label1NoData.textAlignment = .left;
        self.label1NoData.attributedText = completeText;
    }

    @IBAction func actionEditBtnPressed(_ sender: Any) {
        if let controller = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "ContactsViewController") as? ContactsViewController {
            controller.controllerType = .ShowContactsAndCreateGroup
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    func pushChatController(with thread: Thread?) {
        
        if thread?.snederObject?.phoneNumber == AppStateManager.shared.loggedInUser?.phoneNumber{
            Utility.main.showAlert(message: "Chat with own account is not allowed", title: "Alert")
            return
        }
        if let controller = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "ChatController") as? ChatController {
            controller.threadArray = self.threadArray
            if (thread?.isGroupThread ?? "0") == "1" {
                controller.group = thread?.groupObject
                controller.groupID = thread?.groupID
                controller.controllerType = .groupChat
                controller.threadID = thread?.threadID
            } else {
                controller.threadID = thread?.threadID
                controller.recipientUser = thread?.snederObject
                controller.controllerType = .oneToOneChat
                controller.senderUID = thread?.senderUid
            }
//            self.selectedThread = thread?.threadID
            self.navigationController?.pushViewController(controller, animated: true)
//            ChatFireStoreManager.shared.editThreadDataIHaveSeenLstMsg(threadID: thread?.threadID ?? "", sucess: nil, failure: nil)
        }
    }
    @IBAction func btnPremier(_ sender: Any) {
        Utility.main.openWebURL(webURL: Constants.PREMIER_WEB_URL)
    }
    
    @IBAction func btnAdvanceDirectiveNow(_ sender: Any) {
        Utility.main.openWebURL(webURL: Constants.ADVANCE_DIRECT_NOW_WEB_URL)
    }
    
    @IBAction func btnMedSupply(_ sender: Any) {
        Utility.main.openWebURL(webURL: Constants.MED_SUPPLY_WEB_URL)
    }
    
    @IBAction func btnMHP(_ sender: Any) {
        Utility.main.openWebURL(webURL: Constants.MHP_WEB_URL_WEB_URL)
    }
    
    @IBAction func btnGoogle(_ sender: Any) {
        Utility.main.openWebURL(webURL: Constants.GOOGLE_WEB_URL)
    }
    
    @IBAction func btnAmazon(_ sender: Any) {
        Utility.main.openWebURL(webURL: Constants.AMAZON_WEB_URL)
    }
    
}

// MARK: - API CALLING
extension ThreadController {
    func getRealTimeThreads() {
        
        Global.db.collection(FireStoreCollection.AllThreads).document(AppStateManager.shared.loggedInUser?.uid ?? "").collection(FireStoreCollection.ThreadsCollection).order(by: Thread.CodingKeys.dateTime.stringValueThread(), descending: true).addSnapshotListener { documentSnapshot, error in
            guard let documents = documentSnapshot?.documents else {
                print("Error fetching document: \(error!)")
                return
            }
            
            self.threadArray.removeAll()
            for document in documents {
                self.threadArray.append(Thread(with: document.data() as NSDictionary, threadID: document.documentID))
            }
            
            if self.threadArray.count == 0 {
                self.viewNoData.isHidden = false
            } else {
                self.viewNoData.isHidden = true
            }
            (UIApplication.shared.delegate as! AppDelegate).threads = self.threadArray
            self.tableview.reloadData()
        }
    }
}

// MARK: - UISearchResultsUpdating
extension ThreadController: UISearchResultsUpdating, UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.isSearching = false
        self.tableview.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.isSearching = true
        self.searchArray = self.threadArray.filter { (thread) -> Bool in
            if thread.isGroupThread == "1" {
                return thread.groupObject?.groupName?.lowercased().contains(searchText.lowercased()) ?? false
            } else {
                return thread.snederObject?.name?.lowercased().contains(searchText.lowercased()) ?? false
            }
        }
        if self.searchArray.count == 0 && searchText.isEmpty {
            self.searchArray = self.threadArray
        }
        self.tableview.reloadData()
    }
    
    func updateSearchResults(for searchController: UISearchController) {
       
    }
    
    
}
// MARK: - UITableView DataSource & Delegate
extension ThreadController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isSearching! {
            return self.searchArray.count
        }
        return self.threadArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ThreadCell") as? ThreadCell else {
            return UITableViewCell()
        }
        if self.isSearching! {
            cell.bindData(thread: self.searchArray[indexPath.row])
        } else {
            cell.bindData(thread: self.threadArray[indexPath.row])
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if self.isSearching! {
            self.pushChatController(with: self.searchArray[indexPath.row])
        } else {
            self.pushChatController(with: self.threadArray[indexPath.row])
        }
    }
}
