//
//  ChatFireStoreManager.swift
//  P App
//
//  Created by Ahmed Shahid on 10/06/2019.
//  Copyright © 2019 Ahmed Shahid. All rights reserved.
//

import Foundation

class ChatFireStoreManager {
    
    static let shared = ChatFireStoreManager()
    private init() {}
    var index: Int = 0
    var returnSenderUidsArr = [String]()
    
    func sendMessage(with textMessage: String, senderUid: String, uidCombination: String, senderName: String, dateTime: String, messageType: String, priority: String, replyName: String, replyType: String, replyText: String, messengSent: @escaping (Bool) -> Void, failure: @escaping (Error) -> Void) {
        let param = [
            "message": textMessage,
            "sendUid": senderUid,
            "dateTime" : dateTime,
            "senderName" : senderName,
            "messageType" : messageType,
            "priority" : priority,
            "replyName" : replyName,
            "replyType" : replyType,
            "replyText" : replyText
        ]
        Global.db.collection(FireStoreCollection.AllMessagesCollection).document(uidCombination).collection(FireStoreCollection.MessagesCollection).addDocument(data: param) { (err) in
            if let err = err {
                failure(err)
                return
            } else {
                print("Document added")
                messengSent(true)
            }
        }
    }
    
    func sendGroupMessage(with textMessage: String, senderUid: String, groupID: String, senderName: String, priority: String, replyName: String, replyType: String, replyText: String, messageType: String, messengSent: @escaping (Bool) -> Void, failure: @escaping (Error) -> Void) {
        let param = [
            "message": textMessage,
            "sendUid": senderUid,
            "dateTime" : Utility.main.getDateString(),
            "senderName" : senderName,
            "messageType" : messageType,
            "priority" : priority,
            "replyName" : replyName,
            "replyType" : replyType,
            "replyText" : replyText
        ]
        Global.db.collection(FireStoreCollection.AllMessagesCollection).document(groupID).collection(FireStoreCollection.MessagesCollection).addDocument(data: param) { (err) in
             if let err = err {
                failure(err)
                return
            } else {
                print("Document added")
                messengSent(true)
            }
        }
    }
    
    func getMessages(with uidCombination: String , success: @escaping ([ChatMessage]) -> Void, failure: @escaping (Error) -> Void) {
        let docRef = Global.db.collection(FireStoreCollection.AllMessagesCollection).document(uidCombination).collection(FireStoreCollection.MessagesCollection)
        
        Utility.main.showLoader()
        docRef.getDocuments { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
                failure(err)
            } else {
                var returnChatArray = [ChatMessage]()
                for document in querySnapshot!.documents {
                    returnChatArray.append(ChatMessage(with: document.data() as NSDictionary, documentId: document.documentID))
                    print("\(document.documentID) => \(document.data())")
                }
                success(returnChatArray)
            }
            Utility.main.hideLoader()
        }
    }
    
    func getMyThreads(success: @escaping ([Thread]) -> Void, failure: @escaping (Error) -> Void) {
        let docRef = Global.db.collection(FireStoreCollection.AllThreads).document(AppStateManager.shared.loggedInUser?.uid ?? "").collection(FireStoreCollection.ThreadsCollection)
        
        docRef.getDocuments { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
                failure(err)
            } else {
                var returnChatArray = [Thread]()
                for document in querySnapshot!.documents {
                    returnChatArray.append(Thread(with: document.data() as NSDictionary, threadID: document.documentID))
                    print("\(document.documentID) => \(document.data())")
                }
                success(returnChatArray)
            }
        }
    }

    func editThreadDataIHaveSeenLstMsg(threadID: String,  sucess: ((Bool?) -> Void)?, failure: ((Error) -> Void)? ) {
        let batch = Global.db.batch()
        let ref = Global.db.collection(FireStoreCollection.AllThreads).document(AppStateManager.shared.loggedInUser?.uid ?? "").collection(FireStoreCollection.ThreadsCollection).document(threadID)
        batch.updateData([Thread.CodingKeys.isLastMsgSeen.stringValueThread() : true], forDocument: ref)
        batch.commit { (error) in
            if let err = error {
                print("Error writing batch \(err)")
                failure?(err)
                return
            }
            sucess?(true)
        }
    }
    
    func updatePriorityMessage(with priority: String, threadID: String, uidCombination: String, sucess: ((Bool?) -> Void)?, failure: ((Error) -> Void)? ) {
        Global.db.collection(FireStoreCollection.AllMessagesCollection).document(uidCombination).collection(FireStoreCollection.MessagesCollection).document(threadID).updateData([
            "priority": priority
        ]) { err in
            if let err = err {
                print("Error updating document: \(err)")
            } else {
                print("Document successfully updated")
                sucess?(true)
            }
        }
    }
    
    // use this method to edit my own group thread
    func editThreadDataForGroup(lastMessage: String, dateTime: String, threadID: String, groupID: String, senderUids: [String],  sucess: @escaping (Bool?) -> Void, failure: @escaping (Error) -> Void) {
        let batch = Global.db.batch()
        let ref = Global.db.collection(FireStoreCollection.AllThreads).document(AppStateManager.shared.loggedInUser?.uid ?? "").collection(FireStoreCollection.ThreadsCollection).document(threadID)
        batch.updateData([Thread.CodingKeys.dateTime.stringValueThread():dateTime, Thread.CodingKeys.lastMessage.stringValueThread(): lastMessage, Thread.CodingKeys.isLastMsgSeen.stringValueThread() : true], forDocument: ref)
        batch.commit { (error) in
            if let err = error {
                print("Error writing batch \(err)")
                failure(err)
                return
            }
            self.recursiveAEditThreadForGroup(lastMessage: lastMessage, dateTime: dateTime, groupID: groupID, senderUids: senderUids, sucess: sucess, failure: failure)
        }
    }
    
    
    
    private func recursiveAEditThreadForGroup(lastMessage: String, dateTime: String, groupID: String, senderUids: [String],  sucess: @escaping (Bool?) -> Void, failure: @escaping (Error) -> Void) {
         var senderUidsTemp = senderUids
        self.editOthersThreadForGroup(lastMessage: lastMessage, dateTime: dateTime, groupID: groupID, senderUid: senderUidsTemp[index], sucess: { (success) in
            if senderUids.count > 1 {
                senderUidsTemp.removeFirst()
                self.recursiveAEditThreadForGroup(lastMessage: lastMessage, dateTime: dateTime, groupID: groupID, senderUids: senderUidsTemp, sucess: {(success) in
                    sucess(success)
                }, failure: failure)
            } else {
                sucess(true)
            }
        }, failure: failure)
    }
    
    private func editOthersThreadForGroup(lastMessage: String, dateTime: String, groupID: String, senderUid: String,  sucess: @escaping (Bool?) -> Void, failure: @escaping (Error) -> Void) {
        Global.db.collection(FireStoreCollection.AllThreads).document(senderUid).collection(FireStoreCollection.ThreadsCollection).whereField(Thread.CodingKeys.groupID.stringValueThread(), isEqualTo: groupID).getDocuments { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
                failure(err)
            } else {
                let threadID = querySnapshot!.documents.last?.documentID
                let batch = Global.db.batch()
                let ref = Global.db.collection(FireStoreCollection.AllThreads).document(senderUid).collection(FireStoreCollection.ThreadsCollection).document(threadID ?? "")
                batch.updateData([Thread.CodingKeys.dateTime.stringValueThread():dateTime, Thread.CodingKeys.lastMessage.stringValueThread(): lastMessage, Thread.CodingKeys.isLastMsgSeen.stringValueThread() : false], forDocument: ref)
                batch.commit { (error) in
                    if let err = error {
                        print("Error writing batch \(err)")
                        failure(err)
                        return
                    }
                    sucess(true)
                }
            }
        }
    }
    
    // use this method to edit my own thread
    func editThreadData(lastMessage: String, dateTime: String, senderUid: String, threadID: String, combineID: String, sucess: @escaping (Bool?) -> Void, failure: @escaping (Error) -> Void) {
        let batch = Global.db.batch()
        let ref = Global.db.collection(FireStoreCollection.AllThreads).document(AppStateManager.shared.loggedInUser?.uid ?? "").collection(FireStoreCollection.ThreadsCollection).document(threadID)
        batch.updateData([Thread.CodingKeys.dateTime.stringValueThread():dateTime, Thread.CodingKeys.lastMessage.stringValueThread(): lastMessage, Thread.CodingKeys.isLastMsgSeen.stringValueThread() : true], forDocument: ref)
        batch.commit { (error) in
            if let err = error {
                print("Error writing batch \(err)")
                failure(err)
                return
            }
            self.editThreadDataOfAnother(lastMessage: lastMessage, dateTime: dateTime, senderUid: senderUid, combinedUid: combineID, sucess: sucess, failure: failure)
        }
    }
    
    // use  this method to edit kese aur ka thread
    private func editThreadDataOfAnother(lastMessage: String, dateTime: String, senderUid: String, combinedUid: String, sucess: @escaping (Bool?) -> Void, failure: @escaping (Error) -> Void) {
        
       Global.db.collection(FireStoreCollection.AllThreads).document(senderUid).collection(FireStoreCollection.ThreadsCollection).whereField(Thread.CodingKeys.combineUid.stringValueThread(), isEqualTo: combinedUid).getDocuments { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
                failure(err)
            } else {
                let threadID = querySnapshot!.documents.last?.documentID
                let batch = Global.db.batch()
                let ref = Global.db.collection(FireStoreCollection.AllThreads).document(senderUid).collection(FireStoreCollection.ThreadsCollection).document(threadID ?? "")
                batch.updateData([Thread.CodingKeys.dateTime.stringValueThread():dateTime, Thread.CodingKeys.lastMessage.stringValueThread(): lastMessage, Thread.CodingKeys.isLastMsgSeen.stringValueThread() : false], forDocument: ref)
                batch.commit { (error) in
                    if let err = error {
                        print("Error writing batch \(err)")
                        failure(err)
                        return
                    }
                    sucess(true)
                }
            }
        }
//        batch.updateData(["dateTime":dateTime], forDocument: ref)
//        batch.commit { (error) in
//            if let err = error {
//                print("Error writing batch \(err)")
//                failure(err)
//                return
//            }
//            sucess(true)
//        }
    }
    
    func createThread(senderUid: String, uidCombination: String, senderObject: NSDictionary, threadIDHasBeenCreated: @escaping (String) -> Void, threadCreated: @escaping (Bool) -> Void, failure: @escaping (Error) -> Void) {
        let param: [String : Any?] = [
            "combineUid":  uidCombination,
            "senderUid": senderUid,
            "dateTime" : Utility.main.getDateString(),
            "snederObject" : AppStateManager.shared.loggedInUser?.dictionaryRepresentation() ?? NSDictionary(),
            "isGroupThread" : "0",
            "groupID" : "",
            "groupObject" : nil,
            
            "lastMessage" : "",
            "isLastMsgSeen" : false
        ]

        let docRef = Global.db.collection(FireStoreCollection.AllThreads).document(senderUid).collection(FireStoreCollection.ThreadsCollection)
        
        Utility.main.showLoader()
        docRef.addDocument(data: param) { (err) in
            if let err = err {
                failure(err)
                return
            } else {
                print("Document added")
                threadIDHasBeenCreated(docRef.document().documentID)
                self.createThread(myUid:senderUid, uidCombination: uidCombination, senderObject: senderObject, threadCreated: threadCreated, failure: failure)
            }
        }
    }
    
    var tempFirst = false
    
    // this method creates thread for mine chat
    func createGroupThread(senderUid: String, groupID: String, groupObject: NSDictionary, otherMembers: [User], threadCreated: @escaping (Bool) -> Void, failure: @escaping (Error) -> Void) {
        let param: [String : Any] = [
            "combineUid": "",
            "senderUid": "",
            "dateTime" : Utility.main.getDateString(),
            "snederObject" : NSDictionary(),
            "isGroupThread" : "1",
            "groupID" : groupID,
            "groupObject" : groupObject,
            
            
            "lastMessage" : "",
            "isLastMsgSeen" : false
        ]
        
        let docRef = Global.db.collection(FireStoreCollection.AllThreads).document(senderUid).collection(FireStoreCollection.ThreadsCollection)
        
        Utility.main.showLoader()
        docRef.addDocument(data: param) { (err) in
            if let err = err {
                failure(err)
                return
            } else {
                print("Document added")
                if !self.tempFirst {
                    self.tempFirst = true
                    self.createGroupThreadForAllOtherMembers(groupID: groupID, groupObject: groupObject, otherMembers: otherMembers, threadCreated: threadCreated, failure: failure)
                } else {
                    threadCreated(true)
                }
                
//                self.createThread(myUid:senderUid, uidCombination: uidCombination, senderObject: senderObject, threadCreated: threadCreated, failure: failure)
            }
        }
    }
    
    // this method creates thread for all other group members
    private  func createGroupThreadForAllOtherMembers( groupID: String, groupObject: NSDictionary, otherMembers: [User], threadCreated: @escaping (Bool) -> Void, failure: @escaping (Error) -> Void) {
        var otherMembersTemp = otherMembers
        self.createGroupThread(senderUid: otherMembers.first?.uid ?? "", groupID: groupID, groupObject: groupObject, otherMembers: otherMembers, threadCreated: { (success) in
            if otherMembersTemp.count > 1 {
                otherMembersTemp.removeFirst()
                self.createGroupThreadForAllOtherMembers( groupID: groupID, groupObject: groupObject, otherMembers: otherMembersTemp, threadCreated: threadCreated, failure: failure)
            } else {
                self.tempFirst = false
                threadCreated(true)
            }
        }) { (err) in
            print("thread create others member fail with err \(err.localizedDescription)")
        }
    }
    
   private func createThread(myUid: String, uidCombination: String, senderObject: NSDictionary, threadCreated: @escaping (Bool) -> Void, failure: @escaping (Error) -> Void) {
    let param: [String : Any] = [
        "combineUid": uidCombination,
        "senderUid": myUid,
        "dateTime" : Utility.main.getDateString(),
        "snederObject" : senderObject
    ]
    
        let docRef = Global.db.collection(FireStoreCollection.AllThreads).document(AppStateManager.shared.loggedInUser?.uid ?? "").collection(FireStoreCollection.ThreadsCollection)
    
        Utility.main.showLoader()
        docRef.addDocument(data: param) { (err) in
            if let err = err {
                failure(err)
                return
            } else {
                print("Document added")
                threadCreated(true)
            }
        }
    }
    
    
}
