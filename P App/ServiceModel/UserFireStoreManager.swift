//
//  UserFireStoreManager.swift
//  P App
//
//  Created by Ahmed Shahid on 01/06/2019.
//  Copyright © 2019 Ahmed Shahid. All rights reserved.
//

import Foundation
import UIKit
import FirebaseStorage

class UserFireStoreManager {
    static let shared = UserFireStoreManager()
    
    private init() {}
    
    
    func registerdUser(with uid: String, param: [String : Any], isUserCreated: @escaping (Bool) -> Void, failure: @escaping (Error) -> Void) {
        Global.db.collection(FireStoreCollection.UserCollection).document(uid).setData(param) { err in
            if let err = err {
               failure(err)
                return
            } else {
                print("Document added")
                isUserCreated(true)
            }
        }
    }
    
    func getUserCollection(with uid: String, sucess: @escaping (User?) -> Void, failure: @escaping (Error) -> Void) {
        let docRef = Global.db.collection(FireStoreCollection.UserCollection).document(uid)
        Utility.main.showLoader()
        docRef.getDocument { (document, error) in
            if error != nil {
                failure(error!)
            }
            if let document = document, document.exists {
                let dataDescription = document.data()
                sucess(User(dictionary: dataDescription! as NSDictionary))
                print("Document data: \(String(describing: dataDescription))")
            } else {
                let errorMessage: String = "User does not exist";
                let userInfo = [NSLocalizedFailureReasonErrorKey: errorMessage]
                let error = NSError(domain: "Domain", code: 0, userInfo: userInfo);
                failure(error)
            }
            Utility.main.hideLoader()
        }
    }
    
    func getOrganizationCollection(with code: String, sucess: @escaping (Organization?) -> Void, failure: @escaping (Error) -> Void) {
        let docRef = Global.db.collection(FireStoreCollection.Organizations).document("OeQWxtJnUi4RcUjrB9eh")
        Utility.main.showLoader()
        docRef.getDocument { (document, error) in
            if error != nil {
                failure(error!)
            }
            if let document = document, document.exists {
                let dataDescription = document.data()
                
                sucess(Organization(with: dataDescription! as NSDictionary))
                print("Organization data: \(String(describing: dataDescription))")
            } else {
                let errorMessage: String = "Organization does not exist";
                let userInfo = [NSLocalizedFailureReasonErrorKey: errorMessage]
                let error = NSError(domain: "Domain", code: 0, userInfo: userInfo);
                failure(error)
            }
            Utility.main.hideLoader()
        }
    }
    
    
    func editData(key: String, value: Any, sucess: @escaping (Bool?) -> Void, failure: @escaping (Error) -> Void) {
        let batch = Global.db.batch()
        var valueToSend = value
        if value is Bool {
            valueToSend = value as! Bool
        } else if value is String {
            valueToSend = value as! String
        }
        let ref = Global.db.collection(FireStoreCollection.UserCollection).document(AppStateManager.shared.loggedInUser?.uid ?? "")
        batch.updateData([key:valueToSend], forDocument: ref)
        Utility.main.showLoader()
        batch.commit { (error) in
            if let err = error {
                print("Error writing batch \(err)")
               failure(err)
                return
            }
            sucess(true)
        }
    }
    
    func getUserToken(uid: String, sucess: @escaping (String?) -> Void, failure: @escaping (Error) -> Void) {
        let docRef = Global.db.collection(FireStoreCollection.UserCollection).document(uid)
        Utility.main.showLoader()
        docRef.getDocument { (document, error) in
            if error != nil {
                failure(error!)
            }
            if let document = document, document.exists {
                let dataDescription = document.data()
                let user = User(dictionary: dataDescription! as NSDictionary)
                sucess(user?.fcmDeviceToken)
                print("Document data: \(String(describing: dataDescription))")
            } else {
                let errorMessage: String = "User does not exist";
                let userInfo = [NSLocalizedFailureReasonErrorKey: errorMessage]
                let error = NSError(domain: "Domain", code: 0, userInfo: userInfo);
                failure(error)
            }
            Utility.main.hideLoader()
        }
    }
    
    func uploadImage(with uid: String, image: UIImage, onCompletion: @escaping(String) -> Void) {
        // Get a reference to the storage service using the default Firebase App
        //         storage = Storage.storage()
        let storage = Storage.storage(url: Constants.FIREBASE_IMAGE_BASEURL)
        // This is equivalent to creating the full reference
        
        let storagePath = "\(Constants.FIREBASE_IMAGE_STORAGE_PATH)\(uid).jpg"
        let storageRef = storage.reference(forURL: storagePath)
        
        // Data in memory
        guard let data = image.jpegData(compressionQuality: 0.7) else {
            onCompletion("")
            return
        }
        
        // Create a reference to the file you want to upload
        let dpRef = storageRef.child("images/\(uid).jpg")
        
        // Upload the file to the path "images/rivers.jpg"
        Utility.main.showLoader()
        let uploadTask = dpRef.putData(data, metadata: nil) { (metadata, error) in
            guard let metadata = metadata else {
                // Uh-oh, an error occurred!
                Utility.main.hideLoader()
                onCompletion("")
                return
            }
            // Metadata contains file metadata such as size, content-type.
            let size = metadata.size
            // You can also access to download URL after upload.
            dpRef.downloadURL { (url, error) in
                guard let downloadURL = url else {
                    // Uh-oh, an error occurred!
                    Utility.main.hideLoader()
                    onCompletion("")
                    return
                }
                Utility.main.hideLoader()
                onCompletion(downloadURL.absoluteString)
            }
        }
    }
    
    // image upload for messages
    func uploadImage(with uid: String, chatID: String, image: UIImage, onCompletion: @escaping(String) -> Void) {
        // Get a reference to the storage service using the default Firebase App
        //         storage = Storage.storage()
        let storage = Storage.storage(url: Constants.FIREBASE_IMAGE_BASEURL)
        // This is equivalent to creating the full reference
        
        let storagePath = "\(Constants.FIREBASE_IMAGE_STORAGE_PATH)\(uid)_\(chatID)_\(Utility.main.getDateString()).jpg"
        let storageRef = storage.reference(forURL: storagePath)
        
        // Data in memory
        guard let data = image.jpegData(compressionQuality: 0.7) else {
            onCompletion("")
            return
        }
        
        // Create a reference to the file you want to upload
        let dpRef = storageRef.child("images/\(uid).jpg")
        
        // Upload the file to the path "images/rivers.jpg"
        Utility.main.showLoader()
        let uploadTask = dpRef.putData(data, metadata: nil) { (metadata, error) in
            guard let metadata = metadata else {
                // Uh-oh, an error occurred!
                Utility.main.hideLoader()
                onCompletion("")
                return
            }
            // Metadata contains file metadata such as size, content-type.
            let size = metadata.size
            // You can also access to download URL after upload.
            dpRef.downloadURL { (url, error) in
                guard let downloadURL = url else {
                    // Uh-oh, an error occurred!
                    Utility.main.hideLoader()
                    onCompletion("")
                    return
                }
                Utility.main.hideLoader()
                onCompletion(downloadURL.absoluteString)
            }
        }
    }
    
    func uploadGroupImage(with uid: String, image: UIImage, onCompletion: @escaping(String) -> Void) {
        // Get a reference to the storage service using the default Firebase App
        //         storage = Storage.storage()
        let storage = Storage.storage(url: Constants.FIREBASE_IMAGE_BASEURL)
        // This is equivalent to creating the full reference
        
        let storagePath = "\(Constants.FIREBASE_IMAGE_STORAGE_PATH)\(uid)_\(Utility.main.getDateString()).jpg"
        let storageRef = storage.reference(forURL: storagePath)
        
        // Data in memory
        guard let data = image.jpegData(compressionQuality: 0.7) else {
            onCompletion("")
            return
        }
        
        // Create a reference to the file you want to upload
        let dpRef = storageRef.child("images/\(uid).jpg")
        
        // Upload the file to the path "images/rivers.jpg"
        Utility.main.showLoader()
        let uploadTask = dpRef.putData(data, metadata: nil) { (metadata, error) in
            guard let metadata = metadata else {
                // Uh-oh, an error occurred!
                Utility.main.hideLoader()
                onCompletion("")
                return
            }
            // Metadata contains file metadata such as size, content-type.
            let size = metadata.size
            // You can also access to download URL after upload.
            dpRef.downloadURL { (url, error) in
                guard let downloadURL = url else {
                    // Uh-oh, an error occurred!
                    Utility.main.hideLoader()
                    onCompletion("")
                    return
                }
                Utility.main.hideLoader()
                onCompletion(downloadURL.absoluteString)
            }
        }
    }
}
