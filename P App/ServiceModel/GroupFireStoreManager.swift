//
//  GroupFireStoreManager.swift
//  P App
//
//  Created by Ahmed Shahid on 07/07/2019.
//  Copyright © 2019 Ahmed Shahid. All rights reserved.
//

import Foundation
import FirebaseFirestore

class GroupFireStoreManager {
    static let shared = GroupFireStoreManager()
    private init() {}
    
    
    func createGroup(groupName: String, groupImage: String, groupMembers: [NSDictionary], groupCreated: @escaping (Bool, Group, String) -> Void, failure: @escaping (Error) -> Void) {
        let param: [String : Any] = [
            "groupName": groupName,
            "groupImage": groupImage,
            "dateTime" : Utility.main.getDateString(),
            "groupMembers" : groupMembers
        ]
        
        var docRef: DocumentReference? = nil
        docRef = Global.db.collection(FireStoreCollection.AllGroups).addDocument(data: param) { (err) in
            if let err = err {
                failure(err)
                return
            } else {
                print("Document added \(docRef!.documentID)")
                let group = Group(with: param as NSDictionary)
                groupCreated(true, group, docRef!.documentID)
            }
        }
    }
}
