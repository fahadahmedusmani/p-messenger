//
//  FCMPushServiceModel.swift
//  P App
//
//  Created by Ahmed Shahid on 18/07/2019.
//  Copyright © 2019 Ahmed Shahid. All rights reserved.
//

import Foundation


class FCMPushServiceModel {
    
    
    static let shared = FCMPushServiceModel()
    
    private init() {}
    
    public class func sendPushNotificationTo(uid: String, senderName: String, message: String) {
        UserFireStoreManager.shared.getUserToken(uid: uid, sucess: { (fcmToken) in
            let Url = String(format: Constants.FCM_BASE_URL)
            guard let serviceUrl = URL(string: Url) else { return }
            if fcmToken?.isEmpty ?? true { return }
            let paramNotification: [String : Any] = [
                "title": senderName,
                "body": message,
                "badge" : "1",
                "sound" : "1"
            ]
            
            let parameterDictionary: [String : Any] = [
                "to" : fcmToken!,
                "priority" : "high",
                "notification" : paramNotification
            ]
            var request = URLRequest(url: serviceUrl)
            request.httpMethod = "POST"
            request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("key=\(Constants.fcmServerKey)", forHTTPHeaderField: "Authorization")
            guard let httpBody = try? JSONSerialization.data(withJSONObject: parameterDictionary, options: []) else {
                return
            }
            request.httpBody = httpBody
            
            let session = URLSession.shared
            session.dataTask(with: request) { (data, response, error) in
                if let response = response {
                    print(response)
                }
                if let data = data {
                    do {
                        let json = try JSONSerialization.jsonObject(with: data, options: [])
                        print(json)
                    } catch {
                        print(error)
                    }
                }
                }.resume()
        }) { (err) in
            
        }

    }
    
    var fcmTokensGlobal = [String]()
    
    public func sendPushNotificationInGroup(fcmTokens: [String], senderName: String, message: String, groupName: String) {
        let Url = String(format: Constants.FCM_BASE_URL)
        var uids = fcmTokens
        UserFireStoreManager.shared.getUserToken(uid: uids.first!, sucess: { (fcmToken) in
            self.fcmTokensGlobal.append(fcmToken ?? "")
            if uids.count == 1 {
                guard let serviceUrl = URL(string: Url) else { return }
                
                
                let paramNotification: [String : Any] = [
                    "title": "\(senderName) @ \(groupName)",
                    "body": message,
                    "badge" : "1",
                    "sound" : "1"
                ]
                
                let parameterDictionary: [String : Any] = [
                    "registration_ids" : self.fcmTokensGlobal,
                    "priority" : "high",
                    "notification" : paramNotification
                ]
//                let parameterDictionary: [String : Any] = [
//                    "to" : fcmToken!,
//                    "priority" : "high",
//                    "notification" : paramNotification
//                ]
                var request = URLRequest(url: serviceUrl)
                request.httpMethod = "POST"
                request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
                request.setValue("key=\(Constants.fcmServerKey)", forHTTPHeaderField: "Authorization")
                guard let httpBody = try? JSONSerialization.data(withJSONObject: parameterDictionary, options: []) else {
                    return
                }
                request.httpBody = httpBody
                
                let session = URLSession.shared
                session.dataTask(with: request) { (data, response, error) in
                    if let response = response {
                        print(response)
                    }
                    if let data = data {
                        do {
                            let json = try JSONSerialization.jsonObject(with: data, options: [])
                            
                            print(json)
                        } catch {
                            print(error)
                        }
                    }
                    self.fcmTokensGlobal.removeAll()
                    }.resume()
            } else {
                uids.removeFirst()
                self.sendPushNotificationInGroup(fcmTokens: uids, senderName: senderName, message: message, groupName: groupName)
            }
        }) { (err) in
            
        }
        
       
    }
}
