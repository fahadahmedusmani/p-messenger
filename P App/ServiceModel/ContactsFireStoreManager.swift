//
//  ContactsFireStoreManager.swift
//  P App
//
//  Created by Ahmed Shahid on 04/06/2019.
//  Copyright © 2019 Ahmed Shahid. All rights reserved.
//

import Foundation

class ContactsFireStoreManager {
    
    static let shared = ContactsFireStoreManager()
    var index: Int = 0
    private init() {}
    var returnUsersArray = [User]()
    func getUser(by phoneNumber: String, sucess: @escaping (User?) -> Void, failure: @escaping (Error) -> Void) {
        
        Global.db.collection(FireStoreCollection.UserCollection).whereField(User.CodingKeys.phoneNumber.stringValue(), isEqualTo: phoneNumber)
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                    failure(err)
                } else {
                    var user: User?// = User(dictionary: document.data() as NSDictionary)
                    for document in querySnapshot!.documents {
                        print("\(document.documentID) => \(document.data())")
                       user = User(dictionary: document.data() as NSDictionary)
                        if user?.phoneNumber == phoneNumber {
                            sucess(user)
                            return
                        }
                    }
                    if user == nil {
                        let err = NSError(domain: "No User found", code: 404, userInfo: nil)
                        failure(err as Error)
                    }
                    
                }
        }
    }
    
    
    func getMultipleUsers(by phoneNumbers: [String], sucess: @escaping ([User]?) -> Void, failure: @escaping (Error) -> Void) {
        var phoneNumberTemp = phoneNumbers
        self.getUser(by: phoneNumbers[index], sucess: { (user) in
            self.returnUsersArray.append(user ?? User())
            if phoneNumbers.count > 1 {
                phoneNumberTemp.removeFirst()
                self.getMultipleUsers(by: phoneNumberTemp, sucess: sucess, failure: failure)
            } else {
                sucess(self.returnUsersArray)
                self.returnUsersArray.removeAll()
            }
        }, failure: failure)
    }
    
    func getUser(from last10DigitsOfNumber: String, sucess: @escaping (User?) -> Void, failure: @escaping (Error) -> Void) {
        
        Global.db.collection(FireStoreCollection.UserCollection).whereField(User.CodingKeys.lastTenDigit.stringValue(), isEqualTo: last10DigitsOfNumber)
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                    failure(err)
                } else {
                    var user: User?// = User(dictionary: document.data() as NSDictionary)
                    for document in querySnapshot!.documents {
                        print("\(document.documentID) => \(document.data())")
                        user = User(dictionary: document.data() as NSDictionary)
                        if user?.lastTenDigit == last10DigitsOfNumber {
                            sucess(user)
                            return
                        }
                    }
                    if user == nil {
                       /* var x : NSDictionary = ["uid":"sAFDSFDSFDS34135SDVDFGFDGFD",
                                                "name":"FAHADDDDD",
                                                "phoneNumber":"123123123",
                                                "dateTime":"7-11-2019",
                                                "image":"fahad",
                                                "isUserActive":"1",
                                                "isMedicalProfessional":false,
                                                "isPatient":false,
                                                "isPatientFamilyMemeber":false,
                                                "fcmDeviceToken":"3214234324312dfggfdgfsdg234321432432",
                                                "lastTenDigit":"3399008853"
                        ]
                        let user = User(dictionary: x)
                        sucess(user)
                        return*/
                        let err = NSError(domain: "No User found", code: 404, userInfo: nil)
                        failure(err as Error)
                    }
                    
                }
        }
    }
}
