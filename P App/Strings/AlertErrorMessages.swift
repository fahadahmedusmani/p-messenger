//
//  AlertErrorMessages.swift
//  P App
//
//  Created by Ahmed Shahid on 15/05/2019.
//  Copyright © 2019 Ahmed Shahid. All rights reserved.
//

import Foundation

enum AlertErrorMessages: String {
    
    case alertTitle = "Alert"
    
    case invalidPhoneNumber = "Please enter correct phone number"
    case invalidOrganizationCode = "Please enter valid organization code to proceed"
    case otpSessionExpire = "Session timeout."
    case unableToEditProfile = "Sorry, we are unable to edit your profile at this moment, please try again or later"
    case tellAFriendText = "Please download the P-Messenger app from playstore link : https://play.google.com/store/apps/details?id=com.app.pappchatlive appstore link :  https://itunes.apple.com/app/id1471015360"
    case logoutWarningMsg = "Are you sure you want to logout your account?"
    case NoContactFound = "This contact does not have P-App installed on their phone. Would you like to invite them to use P-App?"
    case NoInternetSendViaSMS = "It seems like you are not connected with internet. Would you like to send via SMS*. \n\n*Carrier charges may apply."
    func message() -> String {
        return self.rawValue
    }
}
