//
//  Group.swift
//  P App
//
//  Created by Ahmed Shahid on 07/07/2019.
//  Copyright © 2019 Ahmed Shahid. All rights reserved.
//

import Foundation

class Group {
    var groupName: String?
    var groupImage: String?
    var dateTime: String?
    var groupMembers: [User]?
    
    enum CodingKeys: String, CodingKey {
        
        case groupName = "groupName"
        case groupImage = "groupImage"
        case dateTime = "dateTime"
        case groupMembers = "groupMembers"
        
        func stringValue() -> String {
            return self.rawValue
        }
    }
    
    public init(with dictionary: NSDictionary?) {
        groupName = dictionary?[CodingKeys.groupName.stringValue()] as? String
        groupImage = dictionary?[CodingKeys.groupImage.stringValue()] as? String
        dateTime = dictionary?[CodingKeys.dateTime.stringValue()] as? String
        if let userArray = dictionary?[CodingKeys.groupMembers.stringValue()] as? NSArray {
           groupMembers = User.modelsFromDictionaryArray(array: userArray)
        }
        
    }
    
    public class func modelsFromDictionaryArray(array:NSArray) -> [Group] {
        var models:[Group] = []
        for item in array
        {
            models.append(Group(with: item as! NSDictionary))
        }
        return models
    }
    
    private func groupMembersArray() -> NSArray {
        var returnArray = NSMutableArray()
        for member in groupMembers! {
            returnArray.add(member.dictionaryRepresentation())
        }
        return returnArray
    }
    
    public func dictionaryRepresentation() -> NSDictionary {
        let param: [String : Any] = [
            CodingKeys.groupName.stringValue() : self.groupName ?? "",
            CodingKeys.groupImage.stringValue() : self.groupImage ?? "",
            CodingKeys.dateTime.stringValue() : self.dateTime ?? "",
            CodingKeys.groupMembers.stringValue() : self.groupMembersArray()
        ]
        let dictionary = NSMutableDictionary(dictionary: param)
        
        return dictionary
    }
}
