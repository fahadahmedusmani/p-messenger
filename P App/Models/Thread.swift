//
//  Thread.swift
//  P App
//
//  Created by Ahmed Shahid on 01/07/2019.
//  Copyright © 2019 Ahmed Shahid. All rights reserved.
//

import Foundation

class Thread {
    var combineUid: String?
    var senderUid: String?
    var dateTime: String?
    var snederObject: User?
    
    var isGroupThread: String?
    var groupID: String?
    var groupObject: Group?

    var lastMessage: String?
    var isLastMsgSeen: Bool?
    
    var threadID: String?
    
    enum CodingKeys: String, CodingKey {
        
        case combineUid = "combineUid"
        case senderUid = "senderUid"
        case dateTime = "dateTime"
        case snederObject = "snederObject"
        
        case isGroupThread = "isGroupThread"
        case groupID = "groupID"
        case groupObject = "groupObject"
        
        case lastMessage = "lastMessage"
        case isLastMsgSeen = "isLastMsgSeen"
        
        func stringValueThread() -> String {
            return self.rawValue
        }
    }
    
    public init(with dictionary: NSDictionary?, threadID: String) {
        combineUid = dictionary?[CodingKeys.combineUid.stringValueThread()] as? String
        senderUid = dictionary?[CodingKeys.senderUid.stringValueThread()] as? String
        let date = dictionary?[CodingKeys.dateTime.stringValueThread()] as? String
        dateTime = Utility.main.getDateInLocalFormat(date: date!)
        if let userDic = dictionary?[CodingKeys.snederObject.stringValueThread()] as? NSDictionary {
            snederObject = User(dictionary: userDic)
        }
        
        isGroupThread = dictionary?[CodingKeys.isGroupThread.stringValueThread()] as? String
        groupID = dictionary?[CodingKeys.groupID.stringValueThread()] as? String
        
        if let groupObjectDic = dictionary?[CodingKeys.groupObject.stringValueThread()] as? NSDictionary {
            groupObject = Group(with: groupObjectDic)
        }
        
        lastMessage = dictionary?[CodingKeys.lastMessage.stringValueThread()] as? String
        isLastMsgSeen = dictionary?[CodingKeys.isLastMsgSeen.stringValueThread()] as? Bool
        
        self.threadID = threadID
    }
    
//    public class func modelsFromDictionaryArray(array:NSArray) -> [Thread] {
//        var models:[Thread] = []
//        for item in array
//        {
//            models.append(Thread(with: item as! NSDictionary, threadID: ""))
//        }
//        return models
//    }
}
