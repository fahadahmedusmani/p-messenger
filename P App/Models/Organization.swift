//
//  Organization.swift
//  P App
//
//  Created by Avanza on 26/03/2020.
//  Copyright © 2020 Ahmed Shahid. All rights reserved.
//

import Foundation

class Organization {
    
    var limit: String?
    var pcode: String?
    
    enum CodingKeys: String, CodingKey {
        
        case limit = "limit"
        case pcode = "pcode"
        
        func stringValue() -> String {
            return self.rawValue
        }
    }
    
    public init(with dictionary: NSDictionary?) {
        limit = dictionary?[CodingKeys.limit.stringValue()] as? String
        pcode = dictionary?[CodingKeys.pcode.stringValue()] as? String
    }
    
    public class func modelsFromDictionaryArray(array:NSArray) -> [Organization] {
        var models:[Organization] = []
        for item in array
        {
            models.append(Organization(with: (item as! NSDictionary)))
        }
        return models
    }
}
