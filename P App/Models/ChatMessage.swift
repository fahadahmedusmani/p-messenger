//
//  ChatMessage.swift
//  P App
//
//  Created by Ahmed Shahid on 11/06/2019.
//  Copyright © 2019 Ahmed Shahid. All rights reserved.
//

import Foundation

enum MessageType: String {
    case textMessage = "0"
    case imageMessage = "1"
}

class ChatMessage {
    var message: String?
    var sendUid: String?
    var dateTime: String?
    var senderName: String?
    var messageType: String? // 0 for text message, 1 for image message
    //senderName
    var priority: String?
    var replyName: String?
    var replyType: String?
    var replyText: String?
    var documentId : String?
    
    enum CodingKeys: String, CodingKey {
        
        case message = "message"
        case sendUid = "sendUid"
        case dateTime = "dateTime"
        case senderName = "senderName"
        case messageType = "messageType"
        case priority = "priority"
        case replyName = "replyName"
        case replyType = "replyType"
        case replyText = "replyText"
        case documentId = "documentId"
        
        func stringValue() -> String {
            return self.rawValue
        }
    }
    
    public init(with dictionary: NSDictionary?, documentId: String) {
        self.documentId = documentId
        message = dictionary?[CodingKeys.message.stringValue()] as? String
        sendUid = dictionary?[CodingKeys.sendUid.stringValue()] as? String
        dateTime = dictionary?[CodingKeys.dateTime.stringValue()] as? String
        senderName = dictionary?[CodingKeys.senderName.stringValue()] as? String
        messageType = dictionary?[CodingKeys.messageType.stringValue()] as? String
        replyName = dictionary?[CodingKeys.replyName.stringValue()] as? String
        replyType = dictionary?[CodingKeys.replyType.stringValue()] as? String
        replyText = dictionary?[CodingKeys.replyText.stringValue()] as? String
        priority = dictionary?[CodingKeys.priority.stringValue()] as? String
    }
    
    public class func modelsFromDictionaryArray(array:NSArray, documentId: String) -> [ChatMessage] {
        var models:[ChatMessage] = []
        for item in array
        {
            models.append(ChatMessage(with: item as! NSDictionary, documentId: documentId))
        }
        return models
    }
}
