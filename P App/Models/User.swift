//
//  User.swift
//  P App
//
//  Created by Ahmed Shahid on 18/05/2019.
//  Copyright © 2019 Ahmed Shahid. All rights reserved.
//

import Foundation
import RMMapper

class User :  NSObject, NSCoding, Decodable {
    var uid : String?
    var name : String?
    var phoneNumber : String?
    var dateTime : String?
    var image : String?
    var isUserActive : String?
    
    var isMedicalProfessional: Bool?
    var isPatient : Bool?
    var isPatientFamilyMemeber : Bool?
    
    var fcmDeviceToken: String?
    var lastTenDigit: String?
    
    
    enum CodingKeys: String, CodingKey {
        
        case uid = "uid"
        case name = "name"
        case phoneNumber = "phoneNumber"
        case dateTime = "dateTime"
        case image = "image"
        case isUserActive = "isUserActive"
        
        case isMedicalProfessional = "isMedicalProfessional"
        case isPatient = "isPatient"
        case isPatientFamilyMemeber = "isPatientFamilyMemeber"
        
        case fcmDeviceToken = "fcmDeviceToken"
        case lastTenDigit = "lastTenDigit"
        
        func stringValue() -> String {
            return self.rawValue
        }
    }
    
    
    required public override init() {
        uid = ""
        name = ""
        phoneNumber = ""
        dateTime = ""
        image = ""
        isUserActive = ""
        
        isMedicalProfessional = false
        isPatient = false
        isPatientFamilyMemeber = false
        
        fcmDeviceToken = ""
        lastTenDigit = ""
    }
    
    public init?(with dictionary: NSDictionary) {
        uid = dictionary[CodingKeys.uid] as? String
        name = dictionary[CodingKeys.name] as? String
        phoneNumber = dictionary[CodingKeys.phoneNumber] as? String
        dateTime = dictionary[CodingKeys.dateTime] as? String
        image = dictionary[CodingKeys.image] as? String
        isUserActive = dictionary[CodingKeys.isUserActive] as? String
        
        isMedicalProfessional = dictionary[CodingKeys.isMedicalProfessional] as? Bool
        isPatient = dictionary[CodingKeys.isPatient] as? Bool
        isPatientFamilyMemeber = dictionary[CodingKeys.isPatientFamilyMemeber] as? Bool
        
        fcmDeviceToken = dictionary[CodingKeys.fcmDeviceToken] as? String
        lastTenDigit = dictionary[CodingKeys.lastTenDigit] as? String
    }
    
    public init?(dictionary: NSDictionary) {
        uid = dictionary["uid"] as? String
        name = dictionary["name"] as? String
        phoneNumber = dictionary["phoneNumber"] as? String
        dateTime = dictionary["dateTime"] as? String
        image = dictionary["image"] as? String
        isUserActive = dictionary["isUserActive"] as? String
        
        isMedicalProfessional = dictionary["isMedicalProfessional"] as? Bool
        isPatient = dictionary["isPatient"] as? Bool
        isPatientFamilyMemeber = dictionary["isPatientFamilyMemeber"] as? Bool
        
        fcmDeviceToken = dictionary["fcmDeviceToken"] as? String
        lastTenDigit = dictionary["lastTenDigit"] as? String
    }
    
    public class func modelsFromDictionaryArray(array:NSArray) -> [User]
    {
        var models:[User] = []
        for item in array
        {
            models.append(User(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        uid = try values.decodeIfPresent(String.self, forKey: .uid)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        phoneNumber = try values.decodeIfPresent(String.self, forKey: .phoneNumber)
        dateTime = try values.decodeIfPresent(String.self, forKey: .dateTime)
        image = try values.decodeIfPresent(String.self, forKey: .image)
        isUserActive = try values.decodeIfPresent(String.self, forKey: .isUserActive)
        
        isMedicalProfessional = try values.decodeIfPresent(Bool.self, forKey: .isMedicalProfessional)
        isPatient = try values.decodeIfPresent(Bool.self, forKey: .isPatient)
        isPatientFamilyMemeber = try values.decodeIfPresent(Bool.self, forKey: .isPatientFamilyMemeber)
        
        fcmDeviceToken = try values.decodeIfPresent(String.self, forKey: .fcmDeviceToken)
        lastTenDigit = try values.decodeIfPresent(String.self, forKey: .lastTenDigit)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init()
        uid = aDecoder.decodeObject(forKey:  CodingKeys.uid.rawValue) as? String
        name = aDecoder.decodeObject(forKey:  CodingKeys.name.rawValue) as? String
        phoneNumber = aDecoder.decodeObject(forKey:  CodingKeys.phoneNumber.rawValue) as? String
        dateTime = aDecoder.decodeObject(forKey:  CodingKeys.dateTime.rawValue) as? String
        image = aDecoder.decodeObject(forKey:  CodingKeys.image.rawValue) as? String
        isUserActive = aDecoder.decodeObject(forKey:  CodingKeys.isUserActive.rawValue) as? String
        
        isMedicalProfessional = aDecoder.decodeObject(forKey:  CodingKeys.isMedicalProfessional.rawValue) as? Bool
        isPatient = aDecoder.decodeObject(forKey:  CodingKeys.isPatient.rawValue) as? Bool
        isPatientFamilyMemeber = aDecoder.decodeObject(forKey:  CodingKeys.isPatientFamilyMemeber.rawValue) as? Bool
        
        fcmDeviceToken = aDecoder.decodeObject(forKey:  CodingKeys.fcmDeviceToken.rawValue) as? String
        lastTenDigit = aDecoder.decodeObject(forKey:  CodingKeys.lastTenDigit.rawValue) as? String
    }
    
    public func encode(with encoder: NSCoder) {
        //Encode properties, other class variables, etc
        encoder.encode(self.uid, forKey: CodingKeys.uid.rawValue)
        encoder.encode(self.name, forKey: CodingKeys.name.rawValue)
        encoder.encode(self.phoneNumber, forKey: CodingKeys.phoneNumber.rawValue)
        encoder.encode(self.dateTime, forKey: CodingKeys.dateTime.rawValue)
        encoder.encode(self.image, forKey: CodingKeys.image.rawValue)
        encoder.encode(self.isUserActive, forKey: CodingKeys.isUserActive.rawValue)
        
        encoder.encode(self.isMedicalProfessional, forKey: CodingKeys.isMedicalProfessional.rawValue)
        encoder.encode(self.isPatient, forKey: CodingKeys.isPatient.rawValue)
        encoder.encode(self.isPatientFamilyMemeber, forKey: CodingKeys.isPatientFamilyMemeber.rawValue)
        
        encoder.encode(self.fcmDeviceToken, forKey: CodingKeys.fcmDeviceToken.rawValue)
        encoder.encode(self.lastTenDigit, forKey: CodingKeys.lastTenDigit.rawValue)
    }
    
    public func dictionaryRepresentation() -> NSDictionary {
        let param: [String : Any] = [
            CodingKeys.uid.stringValue() : self.uid ?? "",
            CodingKeys.name.stringValue() : self.name ?? "",
            CodingKeys.phoneNumber.stringValue() : self.phoneNumber ?? "",
            CodingKeys.dateTime.stringValue() : self.dateTime ?? "",
            CodingKeys.image.stringValue() : self.image ?? "",
            CodingKeys.isUserActive.stringValue() : self.isUserActive ?? "",
            CodingKeys.isMedicalProfessional.stringValue() : self.isMedicalProfessional ?? false,
            CodingKeys.isPatient.stringValue() : self.isPatient ?? false,
            CodingKeys.isPatientFamilyMemeber.stringValue() : self.isPatientFamilyMemeber ?? false,
            CodingKeys.fcmDeviceToken.stringValue() : self.fcmDeviceToken ?? "",
            CodingKeys.lastTenDigit.stringValue() : self.lastTenDigit ?? ""
        ]
        let dictionary = NSMutableDictionary(dictionary: param)
        
        return dictionary
    }
    
}
