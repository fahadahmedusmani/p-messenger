//
//  TextToImageReplyReceivedCell.swift
//  P App
//
//  Created by Avanza on 20/04/2020.
//  Copyright © 2020 Ahmed Shahid. All rights reserved.
//

import UIKit

class TextToImageReplyReceivedCell: UITableViewCell {

    @IBOutlet weak var btnImageView: UIButton!
    @IBOutlet weak var labelSenderName: UILabel!
    @IBOutlet weak var imageViewMessage: UIImageView!
    @IBOutlet weak var labelMessageDate: UILabel!
    @IBOutlet weak var labelReplyTitle: UILabel!
    @IBOutlet weak var mainViewReply: UIView!
    @IBOutlet weak var layerView: UIView!
    @IBOutlet weak var labelReplyMessage: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        layerView.borderWidth = 1.0
        layerView.borderColor = UIColor.black
        layerView.layer.cornerRadius = 6.0
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}

