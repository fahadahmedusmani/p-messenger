//
//  ThreadCell.swift
//  P App
//
//  Created by Ahmed Shahid on 20/05/2019.
//  Copyright © 2019 Ahmed Shahid. All rights reserved.
//

import UIKit

class ThreadCell: UITableViewCell {

    @IBOutlet weak var imageviewThread: UIImageView!
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var labelDetail: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var viewSpotNewMessage: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func bindData(thread: Thread) {
        if (thread.isGroupThread ?? "0") == "1" {
            if let urlImage = URL(string: thread.groupObject?.groupImage ?? "") {
                self.imageviewThread.sd_setImage(with: urlImage, placeholderImage: UIImage(named: "profilePlaceholder"))
            } else {
                self.imageviewThread.image = UIImage(named: "profilePlaceholder")
            }
            self.labelName.text = thread.groupObject?.groupName ?? ""
        } else {
            if let urlImage = URL(string: thread.snederObject?.image ?? "") {
                self.imageviewThread.sd_setImage(with: urlImage, placeholderImage: UIImage(named: "profilePlaceholder"))
            } else {
                self.imageviewThread.image = UIImage(named: "profilePlaceholder")
            }
            self.labelName.text = thread.snederObject?.name ?? ""
        }
        
        
        //let dateformatter = DateFormatter()
        //dateformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        //let date: Date? = dateformatter.date(from: thread.dateTime ?? "")
        
        self.labelTime.text = thread.dateTime//date?.offset()
        
        self.labelDetail.text = thread.lastMessage ?? ""
        
        if !(thread.isLastMsgSeen ?? true) {
            self.viewSpotNewMessage.isHidden = false
            self.labelTime.textColor = ColorSheet.NEW_MSG_COLOR
            self.labelDetail.font = UIFont.boldSystemFont(ofSize: 17.0)
            self.labelName.font =  UIFont.boldSystemFont(ofSize: 13.0)
        } else {
            self.viewSpotNewMessage.isHidden = true
            self.labelTime.textColor = UIColor.lightGray
            self.labelDetail.font = UIFont.systemFont(ofSize: 17.0)
            self.labelName.font =  UIFont.systemFont(ofSize: 13.0)
        }
    }

}
