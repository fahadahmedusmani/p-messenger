//
//  MessageImageReplyCell.swift
//  P App
//
//  Created by Avanza on 20/04/2020.
//  Copyright © 2020 Ahmed Shahid. All rights reserved.
//

import UIKit

class MessageImageReplyCell: UITableViewCell {

    @IBOutlet weak var labelSenderName: UILabel!
    @IBOutlet weak var labelMessageText: UILabel!
    @IBOutlet weak var labelMessageDate: UILabel!
    @IBOutlet weak var labelReplyTitle: UILabel!
    @IBOutlet weak var labelReplyMessage: UILabel!
    @IBOutlet weak var mainViewReply: UIView!
    @IBOutlet weak var layerView: UIView!
    @IBOutlet weak var replyImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        layerView.layer.borderWidth = 1.0
        layerView.layer.borderColor = UIColor.black.cgColor
        layerView.layer.cornerRadius = 6.0
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}


