//
//  MessageCell.swift
//  P App
//
//  Created by Ahmed Shahid on 24/05/2019.
//  Copyright © 2019 Ahmed Shahid. All rights reserved.
//

import UIKit

class MessageCell: UITableViewCell {

    @IBOutlet weak var labelSenderName: UILabel!
    @IBOutlet weak var labelMessageText: UILabel!
    @IBOutlet weak var labelMessageDate: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
//    func bindData(message: MessageModel) {
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "E, d MMM yyyy HH:mm:ss"
//        self.labelMessageText.text = message.text
//        self.labelMessageDate.text = dateFormatter.string(from: message.date!)
//    }
}

class ImageMessageCell: UITableViewCell {
    
    @IBOutlet weak var btnImageView: UIButton!
    @IBOutlet weak var labelSenderName: UILabel!
    @IBOutlet weak var imageViewMessage: UIImageView!
    @IBOutlet weak var labelMessageDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
